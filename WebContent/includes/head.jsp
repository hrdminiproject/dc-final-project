<meta name="description" content="">
<meta name="keywords" content="">
<meta charset="utf-8">
<meta name="author" content="Phnom Penh Group 3">
<!--[if IE]><meta http-equiv='X-UA-Compatible' content='IE=edge,chrome=1'><![endif]-->
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0" />

<!-- Favorite icons -->
<link rel="shortcut icon" href="images/favicon.png">
<link rel="apple-touch-icon" href="images/apple-touch-icon.png">
<link rel="apple-touch-icon" sizes="72x72" href="images/apple-touch-icon-72x72.png">
<link rel="apple-touch-icon" sizes="114x114" href="images/apple-touch-icon-114x114.png">

<!-- CSS -->
<link rel="stylesheet" href="plugins/bootstrap-3.3.5/css/bootstrap.min.css">
<link rel="stylesheet" href="plugins/font-awesome-4.4.0/css/font-awesome.min.css">
<link rel="stylesheet" href="plugins/rhythm-tamplate/css/style.css">
<link rel="stylesheet" href="plugins/rhythm-tamplate/css/style-responsive.css">
<link rel="stylesheet" href="plugins/rhythm-tamplate/css/vertical-rhythm.min.css">
<link rel="stylesheet" href="plugins/animate/animate.css">
<link rel="stylesheet" href="plugins/owl.carousel/css/owl.carousel.css">
<link rel="stylesheet" href="plugins/magnific-popup/css/magnific-popup.css">
<link rel="stylesheet" href="plugins/themepunch/css/rev-slider.css">
<link rel="stylesheet" href="plugins/themepunch/css/settings.css" media="screen" />
<link rel="stylesheet" href="css/my-style.css">