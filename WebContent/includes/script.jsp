<!-- JS -->
<script type="text/javascript" src="plugins/jquery/jquery-2.1.3.min.js"></script>
<script type="text/javascript" src="plugins/jquery/jquery.easing.1.3.js"></script>
<script type="text/javascript" src="plugins/bootstrap-3.3.5/js/bootstrap.min.js"></script>
<script type="text/javascript" src="plugins/jquery/SmoothScroll.js"></script>
<script type="text/javascript" src="plugins/jquery/jquery.scrollTo.min.js"></script>
<script type="text/javascript" src="plugins/jquery/jquery.localScroll.min.js"></script>
<script type="text/javascript" src="plugins/jquery/jquery.viewport.mini.js"></script>
<script type="text/javascript" src="plugins/jquery/jquery.countTo.js"></script>
<script type="text/javascript" src="plugins/jquery/jquery.appear.js"></script>
<script type="text/javascript" src="plugins/jquery/jquery.sticky.js"></script>
<script type="text/javascript" src="plugins/jquery/jquery.parallax-1.1.3.js"></script>
<script type="text/javascript" src="plugins/jquery/jquery.fitvids.js"></script>
<script type="text/javascript" src="plugins/owl.carousel/js/owl.carousel.min.js"></script>
<script type="text/javascript" src="plugins/isotope/isotope.pkgd.min.js"></script>
<script type="text/javascript" src="plugins/imagesloaded/imagesloaded.pkgd.min.js"></script>
<script type="text/javascript" src="plugins/magnific-popup/js/jquery.magnific-popup.min.js"></script>
<script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false&amp;language=en"></script>
<script type="text/javascript" src="plugins/gmap3/gmap3.min.js"></script>
<script type="text/javascript" src="plugins/jquery/wow.min.js"></script>
<script type="text/javascript" src="plugins/masonry/masonry.pkgd.min.js"></script>
<script type="text/javascript" src="plugins/jquery/jquery.simple-text-rotator.min.js"></script>
<script type="text/javascript" src="plugins/rhythm-tamplate/js/all.js"></script>
<script type="text/javascript" src="plugins/jquery/jquery.ajaxchimp.min.js"></script>
<script type="text/javascript" src="plugins/themepunch/js/jquery.themepunch.tools.min.js"></script>
<script type="text/javascript" src="plugins/themepunch/js/jquery.themepunch.revolution.min.js"></script>
<script type="text/javascript" src="plugins/themepunch/js/rev-slider.js"></script>
<script type="text/javascript" src="js/contact-form.js"></script>
<!--[if lt IE 10]><script type="text/javascript" src="js/placeholder.js"></script><![endif]-->



