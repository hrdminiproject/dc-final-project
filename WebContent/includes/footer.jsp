<!-- Footer Section -->
<section class="small-section bg-dark" style="padding:35px;">
    <div class="container relative">

        <div class="align-center">
            <div class="container">
                <div class="row">
                    <div class="col-md-4">
                        <h3>About</h3> Korea Software HRD Center is an IT academy for training SW professionals established by Korea International Cooperation Agency (KOICA) and Webcash Co., Ltd in April, 2013. Our mission is to train SW experts, provide job opportunities for them, and improve SW technologies in Cambodia.
                    </div>
                    <div class="col-md-4">
                        <h3>Contact Us</h3> Address: #12, St 323, Sangkat Boeung Kak II, KhanToul Kork, Phnom Penh, Cambodia.
                        <br> Tel: (855)23 99 13 14
                        <br> (855)77 77 12 36 (Khmer, English)
                        <br> (855)15 4 5555 2 (Khmer, English)
                        <br> (855)17 52 81 69(Korean, English)
                        <br>
                    </div>
                    <div class="col-md-4">
	                    <h3>Social</h3>
	                    <div class="row">
	                    	<div class="col-sm-2">
		                    	<i class="fa fa-envelope"></i>
		                    </div>
		                    <div class="col-sm-2">
		                    	<a href="kpsohe@hrd.com">kpsohe@hrd.com</a>
		                    </div>
		                    <div class="col-sm-2">
		                    	
		                    </div>
		                    <div class="col-sm-2">
		                    	<a href="kpsohe@kshrd.com">kpsohe@hrd.com</a>
		                    </div>
	                    </div>
	                    <div class="row">
	                    	<div class="col-sm-2">
		                    	<i class="fa fa-facebook"></i>
		                    </div>
		                    <div class="col-sm-2">
		                    	<a href="kpsohe@facebook.com">kpsohe@hrd.com</a>
		                    </div>
		                    <div class="col-sm-2">
		                    	
		                    </div>
		                    <div class="col-sm-2">
		                    	<a href="kpsohe@facebook.com">kpsohe@hrd.com</a>
		                    </div>
	                    </div>
	                    <div class="row">
	                    	<div class="col-sm-2">
		                    	<i class="fa fa-twitter"></i>
		                    </div>
		                    <div class="col-sm-2">
		                    	<a href="kpsohe@hrd.com">kpsohe@twitter.com</a>
		                    </div>
	                    </div>
		                                
	                    <!-- End Email --> 
                    </div>
                 </div>
            </div>
    	</div>
	</div>
</section>
<!-- End Footer Section -->