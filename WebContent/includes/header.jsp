<script>
	window.fbAsyncInit = function() {
		FB.init({

			appId : 157712197964057, // KhmerAcademy
			version : 'v2.6', // KhmerAcademy

			//			appId: 169900766745299,  /// TEST
			//			version: 'v2.6',

			status : true, // check login status
			cookie : true, // enable cookies to allow the server to access the session
			xfbml : true
		// parse XFBML

		});

		/*FB.getLoginStatus(function(response) {
			if (response.status === 'connected') {
				alert("Connected to Facebook");
			} else if (response.status === 'not_authorized') {
				alert("the user is logged in to Facebook - but has not authenticated your app");
			 }else{
				alert("the user isn't logged in to Facebook.");
			}
		});*/

		FB.Event.subscribe('auth.authResponseChange', function(response) {
			if (response.status === 'connected') {

				//	 		alert("Connected to Facebook");
				//document.getElementById("message").innerHTML +=  "<br>Connected to Facebook";
				//SUCCESS
			} else if (response.status === 'not_authorized') {
				alert("Failed to Connect");
				//document.getElementById("message").innerHTML +=  "<br>Failed to Connect";	
				//FAILED
			} else {
				alert("Logged Out");
				//document.getElementById("message").innerHTML +=  "<br>Logged Out";
				//UNKNOWN ERROR
			}
		});

	};

	function Login() {

		FB
				.login(
						function(response) {
							if (response.authResponse) {

								var fbname = "";
								var fbemail = "";
								var fbprofileimage = "";
								FB
										.api(
												'/me?fields=id,email,name,gender,birthday',
												function(response) {

													//swal({   title: "Connecting with your facebook Account!",   text: "This alert will close in 5 seconds.",   timer: 5000,   showConfirmButton: false });

													fbname = response.name;
													fbemail = response.email;
													fbprofileimage = response.id;
													fbId = response.id;
													fbGender = response.gender;
													fbDateofbirth = new Date(
															response.birthday);
													//alert(response.birthday);
													//alert(response.birthday + " | " +  response.gender + " | " + response.age_range);

													//			   alert(fbemail + " | " + fbprofileimage );

													//			   if(fbemail == null){
													//				   KA.destroyProgressBar();
													//				   alert("Email is required! You didn't provide your email from facebook. Plase try to sign up again!");
													//				   location.href = path+"/register";
													//				   return;
													//			   }

													//			   alert(fbname + " | " + fbemail + " | " + fbprofileimage+ " | " +  fbGender +" | " + fbId); 

													frmData = {
														email : fbemail,
														username : fbname,
														scID : fbId,
														scType : 2,
														imageUrl : "http://graph.facebook.com/"
																+ fbId
																+ "/picture?type=large",
														gender : fbGender,
														dateofbirth : fbDateofbirth,
														type : 0
													};
													console.log(frmData);
													$
															.ajax({
																url : path
																		+ "/rest/login_with_fb",
																method : "POST",
																datatype : "JSON",
																beforeSend : function(
																		xhr) {
																	xhr
																			.setRequestHeader(
																					"Accept",
																					"application/json");
																	xhr
																			.setRequestHeader(
																					"Content-Type",
																					"application/json");
																},
																//		            data: JSON.stringify({ email : fbemail, scID : fbId, scType : 2 }),
																data : JSON
																		.stringify(frmData),
																success : function(
																		data) {
																	console
																			.log(data);
																	if (data.STATUS == "NOTCONFIRMED") {
																		$(
																				"#message-re,#message")
																				.replaceWith(
																						'<div id="message-re" class="alert alert-danger alert-bold-border square fade in alert-dismissable"> '
																								+ '<button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button>'
																								+ '<strong class="alert-link">This email is already registered with KhmerAcademy, but not yet active. Please go to your email to active your account <a href="https://'+email+'" target="_blank" class="btn btn-primary btn-xs">Open your mail</a> or  <a href="#resend" id="btFrmSendMailToConf" class="btn btn-primary btn-xs">Resend confirmed code to your email</a></strong></strong>'
																								+ '</div>');
																		KA
																				.destroyProgressBar();
																	} else if (data.USER != null) {
																		userLogin(data);
																	} else {
																		KA
																				.destroyProgressBar();
																		$(
																				"#btFacebook")
																				.prop(
																						"disabled",
																						false);
																		$(
																				"#fb-loading")
																				.hide();
																		alert("OOP! "
																				+ data.MESSAGE
																				+ ". Sorry we cannot access your email from facebook. Please register with your email!");
																		//setTimeout(function(){
																		location.href = path
																				+ "/register";
																		//}, 200 );
																		/*if(data.USER != null){
																			userLogin(data);
																		}else{
																			console.log(data);
																			 frmData = {  email : fbemail,
																					 	  username : fbname,
																					  	  scID : fbId,
																					  	  scType : 2,
																					  	  imageUrl : "http://graph.facebook.com/"+fbId+"/picture?type=large",
																					  	  gender : fbGender
																			 }; 
																			$.ajax({
																				url : path+"/rest/add_user_sc",
																				method: "POST",
																				datatype : "JSON",
																				beforeSend: function(xhr) {
																		                xhr.setRequestHeader("Accept", "application/json");
																		                xhr.setRequestHeader("Content-Type", "application/json");
																		        },
																		        data: JSON.stringify(frmData),
																				success: function(data){ 								
																					console.log(data);
																					if(data.STATUS == true){
																						userLogin(data);
																					}else{
																						alert("OOP! Please try to register again!");
																					}
																				}
																			});  
																		}*/
																	}
																}
															});

												});

							} else {
								console
										.log('User cancelled login or did not fully authorize.');
							}
						}, {
							scope : 'email,public_profile,user_birthday',
							return_scopes : true
						});

	}

	function getUserInfo() {
		FB
				.api(
						'/me',
						function(response) {
							var str = "<b>Name</b> : " + response.name + "<br>";
							str += "<b>Link: </b>" + response.link + "<br>";
							str += "<b>Username:</b> " + response.username
									+ "<br>";
							str += "<b>id: </b>" + response.id + "<br>";
							str += "<b>Email:</b> " + response.email + "<br>";
							str += "<input type='button' value='Get Photo' onclick='getPhoto();'/>";
							str += "<input type='button' value='Logout' onclick='Logout();'/>";
							document.getElementById("status").innerHTML = str;
						});
	}

	function getPhoto() {
		FB.api('/me/picture?type=normal', function(response) {
			var str = "<br/><b>Pic</b> : <img src='"+response.data.url+"'/>";
			document.getElementById("status").innerHTML += str;

		});
	}

	function Logout() {
		FB.logout(function() {
			document.location.reload();
		});
	}

	function userLogin(data) {
		//  console.log(data.USER.email + " | " + data.USER.password);
		frmData = {
			ka_username : data.USER.email,
			ka_password : data.USER.password
		};
		$
				.ajax({
					url : path + "/login",
					type : "POST",
					datatype : "JSON",
					data : frmData,
					success : function(data) {
						if (data == "Bad credentials") {
							$("#message")
									.replaceWith(
											'<div id="message" class="alert alert-danger alert-bold-border square fade in alert-dismissable"> '
													+ '<button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button>'
													+ '<strong class="alert-link">Invalid username or password! please try again!</strong>'
													+ '</div>');
						} else if (data == 'false') {
							$("#message")
									.replaceWith(
											'<div id="message" class="alert alert-danger alert-bold-border square fade in alert-dismissable"> '
													+ '<button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button>'
													+ '<strong class="alert-link">Your email is not yet verify. Please go to your email to verify!</strong>'
													+ '<br/><a href="#" id="btFrmSendMailToConf">Not yet receive mail click here!</a>'
													+ '</div>');
						} else {
							$("#message")
									.replaceWith(
											'<div id="message" class="alert alert-success alert-bold-border square fade in alert-dismissable"> '
													+ '<button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button>'
													+ '<strong class="alert-link">Login successfully!</strong>'
													+ '</div>');
							setTimeout(function() {
								location.href = data;
							}, 200);

						}
					},
					error : function(data) {
						console.log(data);
					}
				});
	}

	/*  setTimeout(function(){ 
	 Login();
	 }, 3000);*/
</script>

<!-- Navigation panel -->
<nav class="main-nav dark transparent stick-fixed">
	<div class="full-wrapper relative clearfix">
		<!-- Logo ( * your text or image into link tag *) -->
		<div class="nav-logo-wrap local-scroll">
			<a href="index.jsp" class="logo"> <img
				src="images/logo-white.png" alt="" />
			</a>
		</div>
		<div class="mobile-nav">
			<i class="fa fa-bars"></i>
		</div>

		<!-- Main Menu -->
		<div class="inner-nav desktop-nav">
			<ul class="clearlist">

				<!-- Home -->
				<li><a href="index.jsp"><i class="fa fa-home"></i> Home</a></li>
				<!-- End Home -->
				<li><a href="category.jsp"><i class="fa fa-codepen"></i>
						Category</a></li>

				<%
					if (pageContext.getAttribute("user", PageContext.SESSION_SCOPE) != null) {
				%>
				<li><a href="#" class="mn-has-sub"><i class="fa fa-user"></i>
						<%
							out.print(pageContext.getAttribute("lname", PageContext.SESSION_SCOPE) + " "
										+ pageContext.getAttribute("fname", PageContext.SESSION_SCOPE));
						%> </a>
					<ul class="mn-sub" style="display: none;">
						<%
							if (pageContext.getAttribute("role", PageContext.SESSION_SCOPE).toString().contains("0")) {
						%>
						<li><a href="admin/index.jsp" class="mn-has-sub"><i
								class="fa fa-male"></i> Go to Admin Panel</a></li>
						<%
							} else if (pageContext.getAttribute("role", PageContext.SESSION_SCOPE).toString().contains("1")) {
						%>
						<li><a href="admin/profile.jsp" class="mn-has-sub"><i
								class="fa fa-male"></i> Change Info</a></li>
						<%
							} else {
						%>
						<li><a href="admin/becomeDeveloper.hrd" class="mn-has-sub"><i
								class="fa fa-github-alt"></i> Become Developer</a></li>
						<li><a href="#myModal" data-toggle="modal" class="mn-has-sub"><i
								class="fa fa-female"></i> Change Info</a></li>
						<li><a href="#myPass" data-toggle="modal" class="mn-has-sub"><i
								class="fa  fa-wrench"></i> Change Password</a></li>
						<%
							}
						%>
						<li><a href="frontUserOut.hrd" class="mn-has-sub"><i
								class="fa fa-sign-out"></i> Log out</a></li>
					</ul></li>
				<%
					} else {
				%>
				<li><a href="admin/registration.jsp" class="mn-has-sub"><i
						class="fa fa-user"></i> Sign Up</a></li>
				<li><a href="#myModal123" data-toggle="modal"
					class="mn-has-sub"><i class="fa fa-key"></i>LOG IN</a></li>

				<%
					}
				%>

			</ul>
		</div>
		<!-- End Main Menu -->


	</div>
</nav>
<!-- End Navigation panel -->

<div aria-hidden="true" aria-labelledby="myModalLabel2" role="dialog"
	tabindex="-1" id="myModal123" class="modal fade" style="display: none;">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header" style="background: #50A253;">
				<button aria-hidden="true" data-dismiss="modal" class="close"
					type="button">
					<span class="fa fa-times"></span>
				</button>
				<h4 class="modal-title">Login</h4>
			</div>
			<div class="modal-body">
				<div style="display: none" class="alert alert-danger" role="alert">
					<b>Username and password is incorrect!</b>
				</div>
				<form role="form" id="formLogin">
					<div class="form-group">
						<label for="username">Username:</label> <input type="text"
							class="form-control" id="username" name="username">
					</div>
					<div class="form-group">
						<label for="password">Password:</label> <input type="password"
							class="form-control" id="password" name="password">
					</div>
					<button class="btn btn-success btn-md btn-block" type="submit">Login</button>

				</form>
			</div>
		</div>
	</div>
</div>

<!-- Password Modal -->
<div aria-hidden="true" aria-labelledby="myModalLabel2" role="dialog"
	tabindex="-1" id="myPass" class="modal fade" style="display: none;">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button aria-hidden="true" data-dismiss="modal" class="close"
					type="button">
					<span class="fa fa-times"></span>
				</button>
				<h4 class="modal-title">Change Password</h4>
			</div>
			<div class="modal-body">




				<form role="form" action="changePassword.hrd" method="post">
					<div class="form-group">
						<label for="exampleInputEmail1">Old Password</label> <input
							type="password" class="form-control" id="oldpwd" name="oldpwd">
					</div>
					<div class="form-group">
						<label for="exampleInputPassword1">New Password</label> <input
							type="password" class="form-control" id="newpwd" name="newpwd">
					</div>
					<div class="form-group">
						<label for="exampleInput">Confirm New Password</label> <input
							type="password" class="form-control" id="confirmnewpwd"
							name="confirmnewpwd">
					</div>
					<button class="btn btn-success btn-md btn-block" type="submit">Save</button>
				</form>
			</div>
		</div>
	</div>
</div>


<!-- Change Info -->
<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog"
	tabindex="-1" id="myModal" class="modal fade" style="display: none;">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button aria-hidden="true" data-dismiss="modal" class="close"
					type="button">
					<span class="fa fa-times"></span>
				</button>
				<h4 class="modal-title">Change Information</h4>
			</div>
			<div class="modal-body">

				<form role="form" action="tochangeinfo.hrd" method="post">

					<div class="form-group">
						<label for="exampleInputPassword1">Last Name</label> <input
							type="text" class="form-control" id="lname" name="lname">
					</div>
					<div class="form-group">
						<label for="exampleInputEmail1">First Name</label> <input
							type="text" class="form-control" id="fname" name="fname">
					</div>
					<div class="form-group">
						<label for="exampleInput">Email</label> <input type="email"
							class="form-control" id="email" name="email">
					</div>
					<button class="btn btn-success btn-md btn-block" type="submit">Save</button>
				</form>
			</div>
		</div>
	</div>
</div>


