<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="ThemeBucket">
    <link rel="shortcut icon" href="#" type="image/png">

    <title>Login</title>

    <link href="admin/css/style.css" rel="stylesheet">
    <link href="admin/css/style-responsive.css" rel="stylesheet">
	<style>
		input.error{
			border-color: red !important;
		}
		label.error{
			color: red !important;
		}
	</style>

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="js/html5shiv.js"></script>
    <script src="js/respond.min.js"></script>
    <![endif]-->
</head>

<body class="login-body">

	<div class="container">
	    <form class="form-signin" id="frmSignIn">
	        <div class="form-signin-heading text-center">
	            <img src="admin/images/signin.png" alt=""/>
	        </div>
	        <div class="login-wrap">
	        	<div class="row">
	                <div class="col-lg-12">
	                 <div class="form-group">   
							<div class="col-lg-12">
								 <div style="display:none" class="alert alert-danger" role="alert">
									<b>Username and password is incorrect!</b>
								</div>
							</div>
                        </div>
	                
	                
                        <div class="form-group">    
                            <div class="col-lg-12">
                                <input class="form-control" type="text" id="username" name="username" placeholder="User Name" />
                            </div>
                        </div>
                        
						<div class="form-group">
                            <div class="col-lg-12">
							<p></p>
                            </div>
                        </div>
                        
                        <div class="form-group">    
                            <div class="col-lg-12">
                                <input class="form-control" type="password" id="password" name="password" placeholder="Password" />
                            </div>
                        </div>
                        <div class="form-group">
							<div class="col-lg-12">
								<button class="btn btn-lg btn-login btn-block" type="submit">
					                <i class="fa fa-check"></i>
					            </button>
							</div>
                        </div>
                        <div class="form-group">   
							<div class="col-lg-12">
								<div class="registration">
					                Not a member yet?
					                <a class="" href="admin/registration.jsp">
					                    Register
					                </a>
					            </div>
							</div>
							<div class="col-lg-12">
								<div class="registration">
					                <a class="" href="/DC/">
					                    Back to Home
					                </a>
					          </div>
							</div>
                        </div>
                        
                           
                        
                     
	                </div>
	            </div>
	        </div>
	    </form>
	</div>


<!-- Placed js at the end of the document so the pages load faster -->

<!-- Placed js at the end of the document so the pages load faster -->
<script src="admin/js/jquery-1.10.2.min.js"></script>
<script src="admin/js/bootstrap.min.js"></script>
<script src="admin/js/modernizr.min.js"></script>

<!-- Validation -->
<script type="text/javascript" src="admin/js/jquery.validate.min.js"></script>
<script>

// this is the id of the form
$("#frmSignIn").submit(function() {
	$.ajax({
		type : "POST",
		url : 'userIn.hrd',
		data : $("#frmSignIn").serialize(), // serializes all the form's elements.
		success : function(data) {
			if (data != "fail") {
				if (data.uri == "") {
					if (data.role == "0")
						//location.replace("admin/index.jsp"); can't back
						location.href = "admin";
					else
						location.href = "/DC";
				} else {
					location.href = data.uri;
				}
			}

			else
				$('#frmSignIn .alert').show();
		}
	});

	return false; // avoid to execute the actual submit of the form.
});

var Script = function() {
	$().ready(function() {
		// validate education adding form on keyup and submit
		$("#frmSignIn").validate({
			rules : {
				username : {
					required : true
				},
				password : {
					required : true
				}
			}
		});
	});
}();
</script>

</body>
</html>
