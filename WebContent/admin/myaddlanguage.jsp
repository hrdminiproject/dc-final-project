<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta name="viewport"
	content="width=device-width, initial-scale=1.0, maximum-scale=1.0">
<meta name="description" content="">
<meta name="author" content="ThemeBucket">

<title>Language</title>

<!--common styles -->
<jsp:include page="includes/common_styles.jsp"></jsp:include>
<link rel="stylesheet" href="css/sub-style.css">

<!--dynamic table-->
  <link href="js/advanced-datatable/css/demo_page.css" rel="stylesheet" />
  <link href="js/advanced-datatable/css/demo_table.css" rel="stylesheet" />
  <link rel="stylesheet" href="js/data-tables/DT_bootstrap.css" />

<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!--[if lt IE 9]>
  <script src="js/html5shiv.js"></script>
  <script src="js/respond.min.js"></script>
  <![endif]-->
</head>

<body class="sticky-header">

	<section>
		<!-- left side start -->
		<jsp:include page="includes/left_side.jsp"></jsp:include>
		<!-- left side end -->

		<!-- main content start -->
		<div class="main-content">

			<!-- header section start-->
			<jsp:include page="includes/top_side.jsp"></jsp:include>
			<!-- header section end-->

			<!--body wrapper start-->
			<div class="wrapper">
	            <div class="panel panel-success">
	                <div class="panel-heading">
	                    <h3 class="panel-title">Language Information</h3>
	                </div>
	                <div class="panel-body table-responsive">
	                  	<a href="addlanguage.jsp" class="btn btn-md btn-success">
	                      	Add New	Language
	                    </a>
	                    <div class="adv-table">
                           <table  class="display table table-bordered table-striped" id="dynamic-table">
                               <thead>
                                   <tr>
                                   	<th class="text-center">#</th>
                                       <th class="text-center">Language</th>
                                       <th class="text-center">Action</th>
                                   </tr>
                               </thead>
                               <tbody id="allLang">
					
                               </tbody>
                           </table>
                       </div>
	                </div>
	        	</div>
	        </div>
			
			<!-- <div class="wrapper">
				Pannel for Categories
				<div class="row">
					<div class="col-md-12">

						pannel skill categories
						<div class="panel panel-success">
							<div class="panel-heading">
								<h3 class="panel-title">Language Information</h3>
							</div>

						</div>

						Categories table information
							<section class="panel">
							<header class="panel-heading panel-body">
							
							
								button Add-new-category skill
								<a href="addlanguage.jsp"><button class="btn btn-info" type="submit">Add New	Language</button></a>	
									
							</header>
							
							table skill category showing
							<div class="panel-body" style="display: block;">
								<table class="table table-hover general-table">
			                        <thead>
			                            <tr>
			                                <th> #</th>
			                                <th>Language</th>
			                                <th>Action</th>
			                            </tr>
			                        </thead>
						
			                        <tbody ng-app="myApp" ng-controller="tableCtrl">
			                            <tr ng-repeat="item in listLanguages">
			                            	<td>{{item.id}}</td>
			                                <td>{{item.language}}</td>
			                                <td>
			                                    <a href="addlanguage.jsp?id={{item.id}}"><i class="fa fa-gear fa-lg"></i></a>&nbsp;
												<a href="#" ng-click="mydelete(item.id)"><i class="fa fa-trash-o fa-lg"></i></a>
			                                </td>
			                            </tr>
			                        </tbody>
			                    </table>
							</div>
							
							
						</section>

					</div>
				</div>
			</div> -->
			<!--body wrapper end-->

			<!--footer section start-->
			<jsp:include page="includes/footer.jsp"></jsp:include>
			<!--footer section end-->
		</div>
		<!-- main content end-->
	</section>


	<!-- common scripts -->
	<jsp:include page="includes/common_scripts.jsp"></jsp:include>
	
	<script src="js/angular.min.js"></script>
	<script src="js/language-script.js"></script>
	
	<!--dynamic table-->
<script type="text/javascript" src="js/advanced-datatable/js/jquery.dataTables.js"></script>
<script type="text/javascript" src="js/data-tables/DT_bootstrap.js"></script>

<script>
	loadData();
	function loadData(){
		$.ajax({
			method: 'post',
			url: 'listAdminLang.hrd',
			success: function(response){
				var tr = "", index = 1;
				for (var x in response){
					tr += '<tr class="gradeX">' +
				            '<td class="text-center">' + index + '</td>' +
				            '<td class="center">' + response[x].language + '</td>' +
				            '<td class="center">' +
				            	'<a href="addlanguage.jsp?id=' + response[x].id + '">' +
				            		'<i class="fa fa-gear fa-lg"></i>' +
				            	'</a>&nbsp;' + 
				            	
				            	'<a href="#" onclick="deleteLang(' + response[x].id + ')">' +
				            		'<i class="fa fa-trash-o fa-lg"></i>' +
				            	'</a>' + 
				            '</td>' +
				          '</tr>';
				     index++;
				}
				$('#allLang').html(tr);
				$('#dynamic-table').dataTable();
			}
		 });
	}
	
	function deleteLang (langId){		
		new $.flavr({
			title		: 'Are your sure ?',
		    dialog      : 'confirm',
		    onConfirm   : function( $container ){
				$.ajax({
			        method: 'POST',
			        url: 'deleteLang.hrd',
			        data: {
			            id: langId
			        },
			        success:function(result){
			        	loadData();
			        }
			    });
		        return true;
		    }
		});
	};
</script>

</body>
</html>
