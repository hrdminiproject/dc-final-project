<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="ThemeBucket">
    <link rel="shortcut icon" href="#" type="image/png">

    <title>Profile Information</title>

    <jsp:include page="includes/common_styles.jsp"></jsp:include>
    <!--pickers css-->
    <link rel="stylesheet" type="text/css" href="js/bootstrap-datepicker/css/datepicker-custom.css" />
    <link rel="stylesheet" type="text/css" href="js/bootstrap-timepicker/css/timepicker.css" />
    <link rel="stylesheet" type="text/css" href="js/bootstrap-colorpicker/css/colorpicker.css" />
    <link rel="stylesheet" type="text/css" href="js/bootstrap-daterangepicker/daterangepicker-bs3.css" />
    <link rel="stylesheet" type="text/css" href="js/bootstrap-datetimepicker/css/datetimepicker-custom.css" />

    <!--file upload-->
    <link rel="stylesheet" type="text/css" href="css/bootstrap-fileupload.min.css" />

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
  <script src="js/html5shiv.js"></script>
  <script src="js/respond.min.js"></script>
  <![endif]-->
</head>

<body class="sticky-header">

    <section>
        <!-- left side start-->
        <jsp:include page="includes/left_side.jsp"></jsp:include>
        <!-- left side end-->

        <!-- main content start-->
        <div class="main-content">

            <!-- header section start-->
            <jsp:include page="includes/top_side.jsp"></jsp:include>
            <!-- header section end-->
            <!--body wrapper start-->
            <div class="wrapper">
	            <div class="row">
			        <div class="col-lg-12">
			            <section class="panel panel-success">
			                <header class="panel-heading">
			                    Personal Information
			                </header>
			                <div class="panel-body">
			                    <div class="form" ng-app="myApp">
			                      <form class="cmxform form-horizontal adminex-form" id="frmPersonal" action="upsertDev.hrd" method="post" enctype="multipart/form-data">
			                      <input type="hidden" id="avatar" name="avatar">
			                            <div class="form-group ">
			                                <label for="firstname" class="control-label col-lg-2">Full Name *</label>
			                                <div class="col-lg-4 has-success">
			                                    <input class=" form-control" id="fName" name="fName" type="text" placeholder="First Name" />
			                                </div>
			                                <div class="col-lg-4 has-success">
			                                    <input class=" form-control" id="lName" name="lName" type="text" placeholder="Last Name" />
			                                </div>
			                                <div class="col-lg-2 has-success">
			                                    <select class="form-control m-bot15" id="cbo_gender" name="cbo_gender">
		                                            <!-- <option value="">--Gender--</option> -->
		                                            <option value="Male">Male</option>
		                                            <option value="Female">Female</option>
		                                        </select>
			                                </div>
			                            </div>
			                            <div class="form-group ">
			                                <label for="lastname" class="control-label col-lg-2">Nationality *</label>
			                                <div class="col-lg-10 has-success">
			                                    <input class=" form-control" id="nationality" name="nationality" type="text" />
			                                </div>
			                            </div>
			                            <div class="form-group ">
			                                <label for="lastname" class="control-label col-lg-2">Date of Birth *</label>
			                                <div class="col-lg-10 has-success">
			                                    <input class="form-control form-control-inline input-medium default-date-picker" size="16" type="text" id="dob" name="dob">
			                                </div>
			                            </div>
			                            <div class="form-group ">
			                                <label for="lastname" class="control-label col-lg-2">Place of Birth *</label>
			                                <div class="col-lg-10 has-success">
			                                    <input class=" form-control" id="pob" name="pob" type="text" />
			                                </div>
			                            </div>
			                            <div class="form-group ">
			                                <label for="lastname" class="control-label col-lg-2">Marital Status *</label>
			                                <div class="col-lg-5 has-success">
			                                    <select class="form-control m-bot15" id="cbo_marital" name="cbo_marital">
	                                            <!-- <option value="">--Select--</option> -->
	                                            <option value="Single">Single</option>
	                                            <option value="Married">Married</option>
                                        </select>
			                                </div>
			                            </div>
			                            <div class="form-group ">
			                                <label for="lastname" class="control-label col-lg-2">Current City/States *</label>
			                                <div class="col-lg-5 has-success">
			                                    <select class="form-control m-bot15" id="cbo_cur_citystate" name="cbo_cur_citystate">
	                                            <!-- <option value="">--Select--</option> -->
	                                            <option value="Battambong">Battambong</option>
	                                            <option value="Bonteay Meanchey">Bonteay Meanchey</option>
	                                            <option value="Kandal">Kandal</option>
	                                            <option value="Kep">Kep</option>
	                                            <option value="Koh Kong">Koh Kong</option>
	                                            <option value="Kompong Cham">Kompong Cham</option>
	                                            <option value="Kompong Chhnang">Kompong Chhnang</option>
	                                            <option value="Kompong Speu">Kompong Speu</option>
	                                            <option value="Kompong Thom">Kompong Thom</option>
	                                            <option value="Kompot">Kompot</option>
	                                            <option value="Kratie">Kratie</option>	
	                                            <option value="Mondulkiri">Mondulkiri</option>
	                                            <option value="Oddor Meanchey">Oddor Meanchey</option>                             
	                                            <option value="Pailin">Pailin</option>
	                                            <option value="Phnom Penh">Phnom Penh</option>
	                                            <option value="Preah Vihear">Preah Vihear</option>
	                                            <option value="Preah Sikhaknouk">Preah Sihaknouk</option>
	                                            <option value="Prey Veng">Prey Veng</option>
	                                            <option value="Pursat">Pursat</option>		
	                                            <option value="Ratanakiri">Ratanakiri</option>
	                                            <option value="Siem Reap">Siem Reap</option>
	                                            <option value="Steung Treng">Steung Treng</option>
	                                            <option value="Svay Reang">Svay Reang</option>
	                                            <option value="Takeo">Takeo</option>	
	                                            <option value="Thboung Khhmom">Thboung Khhmom</option>
                                        </select>
			                                </div>
			                            </div>
			                            <div class="form-group ">
			                                <label for="lastname" class="control-label col-lg-2">Phone *</label>
			                                <div class="col-lg-10 has-success">
			                                    <input class=" form-control" id="phone" name="phone" type="text" />
			                                </div>
			                            </div>
			                            <div class="form-group ">
			                                <label for="email" class="control-label col-lg-2">Email *</label>
			                                <div class="col-lg-10 has-success">
			                                    <input class=" form-control" id="email" name="email" type="text" />
			                                </div>
			                            </div>
			                            <div class="form-group ">
	                                        <label for="lastname" class="control-label col-lg-2">Address *</label>
	                                        <div class="col-lg-10 has-success">
	                                            <textarea rows="5" cols="60" class="form-control" id="address" name="address"></textarea>
	                                        </div>
	                                    </div>
	                                    
	                                     <div class="form-group ">
			                                <label for="specialty_skill" class="control-label col-lg-2">Specialty Skill *</label>
			                                <div class="col-lg-5 has-success">
			                                    <select class="form-control m-bot15" id="specialty_skill" name="specialty_skill"  ng-controller="selectCtrl">
			                                     <option value="N/A">No</option>
			                                     <option value="{{skill.subjects}}" ng-repeat="skill in specialtySkill">{{skill.subjects}}</option>
                                        		</select>
			                                </div>
			                            </div>
			                           <!--   -->
			                            			                            
			                             <div class="form-group ">
			                                <label for="wanted_position" class="control-label col-lg-2">Wanted Position* </label>
			                                <div class="col-lg-3 has-success">
			                                    <input class=" form-control" id="wanted_position" name="wanted_position" type="text" />
			                                </div>
			                                <label class="control-label col-lg-2">Salary* ($)</label>
			                                <div class="col-lg-2 has-success">
			                                    <input class=" form-control" id="salary" name="salary" type="text" />
			                                </div>
			                            </div>
			                            
			                            <div class="form-group ">
	                                        <label for="motivation" class="control-label col-lg-2">Motivation *</label>
	                                        <div class="col-lg-10 has-success">
	                                            <textarea rows="5" cols="60" class="form-control" id="motivation" name="motivation"></textarea>
	                                        </div>
	                                    </div>
			                            
             			                <div class="form-group">
							              	<label class="col-sm-2 control-label col-lg-2" for="inputSuccess">Profile *</label> 
							              	<div class="col-sm-10">
						                       <div class="fileupload fileupload-new" id="upload" data-provides="fileupload"><input type="hidden" value="" name="">
						                           <div class="fileupload-new thumbnail" style="width: 200px; height: 150px;">
						                               <img src="http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image" id="evidence" alt="">
						                           </div>
						                           <div class="fileupload-preview fileupload-exists thumbnail" style="max-width: 200px; max-height: 150px; line-height: 10px;"></div>
						                           <div>
						                                  <span class="btn btn-default btn-file">
						                                  <span class="fileupload-new"><i class="fa fa-paper-clip"></i> Select image</span>
						                                  <span class="fileupload-exists"><i class="fa fa-undo"></i> Change</span>
						               	                	<input type="file" class="default" name="evidence" accept="image/*">
						                                  </span>
						                               	  <a href="#" class="btn btn-danger fileupload-exists" data-dismiss="fileupload"><i class="fa fa-trash"></i> Remove</a>
						                           </div>
						                       </div>
							                </div>
						                </div>
						                <div class="form-group">
						                   <div class="col-md-offset-2 col-lg-2">
						                   	<button type="submit" class="btn btn-success" id="btnSubmit">Save</button>
						                      	<button type="button" class="btn btn-warning">Cancel</button>
						                    </div>
						                </div>
			                    </form>
			                </div>
			            </div>
			        </section>
			    </div>
			</div>
     	</div>
            <!--body wrapper end-->

            <!--footer section start-->
            <jsp:include page="includes/footer.jsp"></jsp:include>
            <!--footer section end-->
        </div>
        <!-- main content end-->
    </section>

    <!--common scripts for all pages-->
    <jsp:include page="includes/common_scripts.jsp"></jsp:include>
	<script type="text/javascript" src="js/bootstrap-fileupload.min.js"></script>
    <!--pickers plugins-->
    <script type="text/javascript" src="js/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
    <script type="text/javascript" src="js/bootstrap-datetimepicker/js/bootstrap-datetimepicker.js"></script>
	
	<!-- Validation -->	
	<script type="text/javascript" src="js/jquery.validate.min.js"></script>
	<script type="text/javascript" src="js/personal_validate.js"></script>
		
    <!--pickers initialization-->
    <script src="js/pickers-init.js"></script>
    <script src="js/angular.min.js"></script>



    <script>

    var app = angular.module('myApp', []);
    app.controller('selectCtrl', function($scope, $http) {
    	$scope.loadSpecialtySkill = function(){
    		$http({
    		    method: 'POST',
    		    url: 'listSub.hrd',
    		    headers: {'Content-Type': 'application/x-www-form-urlencoded'}
    		}).success(function(response) {
    	    	$scope.specialtySkill = response;
    	    });
    	}
    	$scope.loadSpecialtySkill();
    }); 
    
    
        $(function () {
        	$.ajax({
                url: "getDev.hrd",
                success: function (data) {
                	/*for (var x in data[0]){
                		if (data[0][x]=="null")
                			$('#'+x).val('');
                		else
                			$('#'+x).val(data[0][x]);
                	}*/
                	$('#fName').val(data[0].fname== "null" ? "":data[0].fname);
                	$('#lName').val(data[0].lname== "null" ? "":data[0].lname);
                	$('#cbo_gender').val(data[0].gender== "null" ? "":data[0].gender);
                	$('#phone').val(data[0].phone== "null" ? "":data[0].phone);
                	$('#email').val(data[0].email== "null" ? "":data[0].email);
                	$('#nationality').val(data[0].nationality== "null" ? "":data[0].nationality);
                	$('#pob').val(data[0].pob== "null" ? "":data[0].pob);
                	$('#wanted_position').val(data[0].wanted_position== "null" ? "":data[0].wanted_position);
                	$('#address').val(data[0].address== "null" ? "":data[0].address);
                	$('#salary').val(data[0].salary== "null" ? "":data[0].salary);
                	$('#cbo_marital').val(data[0].marital== "null" ? "":data[0].marital);
                	$('#specialty_skill').val(data[0].specialty_skill == "null" ? "N/A" : data[0].specialty_skill);
                	$('#dob').val(data[0].dob== "null" ? "":data[0].dob);
                	$('#motivation').val(data[0].motivation == "null" ? "":data[0].motivation);
                	$('#avatar').val(data[0].avatar== "null" ? "":data[0].avatar);
                	
                	if(data[0].avatar != null || data[0].avatar != ""){
						$('#upload').removeClass('fileupload-new').addClass('fileupload-exists');
						if(data[0].avatar == ""){
							data[0].avatar = "../default.png";
						}
						$('div.fileupload-preview').append('<img src="../images/uploads/developer/' + data[0].avatar + '" style="max-height: 150px;">');
					}
                }
            });
        });
    </script>
</body>

</html>