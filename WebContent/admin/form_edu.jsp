<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">
  <meta name="description" content="">
  <meta name="author" content="ThemeBucket">

  <title>Education</title>
  
  <!--common styles -->
  <jsp:include page="includes/common_styles.jsp"></jsp:include>
  <link rel="stylesheet" type="text/css" href="css/bootstrap-fileupload.min.css">
  
  <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!--[if lt IE 9]>
  <script src="js/html5shiv.js"></script>
  <script src="js/respond.min.js"></script>
  <![endif]-->
</head>

<body class="sticky-header">

<section>
    <!-- left side start -->
    <jsp:include page="includes/left_side.jsp"></jsp:include>
    <!-- left side end -->
    
    <!-- main content start -->
    <div class="main-content" >

        <!-- header section start-->
        <jsp:include page="includes/top_side.jsp"></jsp:include>
        <!-- header section end-->

        <!--body wrapper start-->
        <div class="wrapper">
	<!--Education Information -->
			<div class="row">
		        <div class="col-lg-12">
		            <section class="panel panel-success">
		                <header class="panel-heading">
		                    Education Information
		                </header>
		                <div class="panel-body">
		                    <div class="form">
		                        <form class="cmxform form-horizontal adminex-form" id="frmEducation" method="post" action="addEdu.hrd" enctype="multipart/form-data">
		                        	<input type="hidden" id="image_url" name="image_url">
		                            <div class="form-group ">
		                                <label for="firstname" class="control-label col-lg-2">School/Center *</label>
		                                <div class="col-lg-10 has-success">
		                                    <input class=" form-control" id="school" name="school" type="text" />
		                                </div>
		                            </div>
		                            		                            
		                            <div class="form-group ">
			                             <label for="lastname" class="control-label col-lg-2">Degree </label>
			                             <div class="col-lg-10 has-success">
			                                 <select class="form-control m-bot15" id="degree" name="degree">
	                                            <!-- <option value="">--Select--</option> -->
	                                            <option value="Associate">Associate</option>
	                                            <option value="Bachelor">Bachelor</option>
	                                            <option value="Master">Master</option>
	                                            <option value="Ph.D">Ph.D</option>
	                                            <option value="Doctorate">Doctorate</option>
	                                            <option value="Other">Other</option>
	                                         </select>
			                              </div>
			                         </div>
		                            
		                            <div class="form-group">
						                 <label class="col-sm-2 control-label col-lg-2" for="inputSuccess">Major *</label>
						                 <div class="col-lg-3 has-success">
						                     <div class="radio">
						                         <label>
						                             <input type="radio" id="it" name="major" value="IT" checked="checked">
						                             	Information of Technology
						                         </label>
						                     </div>
						                 </div>
					                	 <div class="col-lg-1">
					                     	<div class="radio">
					                         	<label>
					                            	<input type="radio" name="major" id="ot" value="Other">
					                             	 Other...
					                         	</label>
					                     	</div>
				                 		</div>
						                 <div class="col-lg-3 has-success">
						                     <input type="text" class="form-control" id="other" name="other" placeholder="eg.Environment" disabled>
						                 </div>
					             	</div>
					             
					              	<div class="form-group ">
			                                <label for="location" class="control-label col-lg-2">School location *</label>
			                              <div class="col-lg-5 has-success">
			                                  <select class="form-control m-bot15" id="location" name="location">
	                                            <!-- <option value="">--Select--</option> -->
	                                            <option value="Battambong">Battambong</option>
	                                            <option value="Bonteay Meanchey">Bonteay Meanchey</option>
	                                            <option value="Kandal">Kandal</option>
	                                            <option value="Kep">Kep</option>
	                                            <option value="Koh Kong">Koh Kong</option>
	                                            <option value="Kompong Cham">Kompong Cham</option>
	                                            <option value="Kompong Chhnang">Kompong Chhnang</option>
	                                            <option value="Kompong Speu">Kompong Speu</option>
	                                            <option value="Kompong Thom">Kompong Thom</option>
	                                            <option value="Kompot">Kompot</option>
	                                            <option value="Kratie">Kratie</option>	
	                                            <option value="Mondulkiri">Mondulkiri</option>
	                                            <option value="Oddor Meanchey">Oddor Meanchey</option>                             
	                                            <option value="Pailin">Pailin</option>
	                                            <option value="Phnom Penh">Phnom Penh</option>
	                                            <option value="Preah Vihear">Preah Vihear</option>
	                                            <option value="Preah Sikhaknouk">Preah Sihaknouk</option>
	                                            <option value="Prey Veng">Prey Veng</option>
	                                            <option value="Pursat">Pursat</option>		
	                                            <option value="Ratanakiri">Ratanakiri</option>
	                                            <option value="Siem Reap">Siem Reap</option>
	                                            <option value="Steung Treng">Steung Treng</option>
	                                            <option value="Svay Reang">Svay Reang</option>
	                                            <option value="Takeo">Takeo</option>	
	                                            <option value="Thboung Khhmom">Thboung Khhmom</option>
                                        	 </select>
			                             </div>
			                      	 </div>
			                            
					                 <div class="form-group ">
					                     <label for="start_year" class="control-label col-lg-2">Start Year *</label>
					                     <div class="col-lg-10 has-success">
					                         <input class="form-control" id="start_year" name="start_year" type="text" />
					                     </div>
					                 </div>
					                 
					                 <div class="form-group ">
					                     <label for="end_year" class="control-label col-lg-2">End Year *</label>
					                     <div class="col-lg-10 has-success">
					                         <input class="form-control" id="end_year" name="end_year" type="text" />
					                     </div>
					                 </div>
				                 
				                     <div class="form-group ">
			                              <label for="current_year" class="control-label col-lg-2">Current Year</label>
			                              <div class="col-lg-5 has-success">
			                                <select class="form-control m-bot15" id="current_year" name="current_year">
	                                            <!-- <option value="">--Select--</option> -->
	                                            <option value="Year 1">Year 1</option>
	                                            <option value="Year 2">Year 2</option>
	                                            <option value="Year 3">Year 3</option>
	                                            <option value="Year 4">Year 4</option>
	                                            <option value="Graduated">Graduated</option>
	                                            <option value="Other">Other</option>
                                        	</select>
			                              </div>
			                         </div>
				                 
				                     <div class="form-group ">
					                     <label for="description" class="control-label col-lg-2">Description</label>
					                     <div class="col-lg-10 has-success">
					                          <textarea rows="5" cols="60" class="form-control" id="description" name="description"></textarea>
					                     </div>
				                     </div>
					                            
					                 <div class="form-group">
						              	 <label class="col-sm-2 control-label col-lg-2" for="inputSuccess">Reference *</label> 
						              	 <div class="col-sm-10">
					                        <div class="fileupload fileupload-new" id="upload" data-provides="fileupload"><input type="hidden" value="" name="">
					                            <div class="fileupload-new thumbnail" style="width: 200px; height: 150px;">
					                                <img src="http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image" alt="">
					                            </div>
					                            <div class="fileupload-preview fileupload-exists thumbnail" style="max-width: 200px; max-height: 150px; line-height: 10px;"></div>
					                            <div>
					                                   <span class="btn btn-default btn-file">
					                                   <span class="fileupload-new"><i class="fa fa-paper-clip"></i> Select image</span>
					                                   <span class="fileupload-exists"><i class="fa fa-undo"></i> Change</span>
					                                   	<input type="file" class="default" name="evidence" accept="image/*">
					                                   </span>
					                                	  <a href="#" class="btn btn-danger fileupload-exists" data-dismiss="fileupload"><i class="fa fa-trash"></i> Remove</a>
					                            </div>
					                        </div>
						                 </div>
					                </div>
					                <div class="form-group">
					                   <div class="col-md-offset-2 col-lg-2">
					                   		<button type="submit" class="btn btn-success" id="btnSubmit">Save</button>
					                      	<a href="education.jsp"><button type="button" class="btn btn-warning">Cancel</button></a>
					                    </div>
					                </div>
		                    </form>
		                </div>
		            </div>
		        </section>
		    </div>
		</div>
	<!-- End of Education Information -->
        </div>
        <!--body wrapper end-->
        <!--footer section start-->
        <jsp:include page="includes/footer.jsp"></jsp:include>
        <!--footer section end-->

    </div>
    <!-- main content end-->
</section>

<!-- common scripts -->
<jsp:include page="includes/common_scripts.jsp"></jsp:include>
<script type="text/javascript" src="js/bootstrap-fileupload.min.js"></script>

<script type="text/javascript" src="js/jquery.validate.min.js"></script>
<script src="js/edu_validate.js"></script>

<script>
// Radio button.......
$(function(){
	$('#ot').click(function(){
		//$('#other').removeAttr('disabled');
		$('#other').prop('disabled',false);
	});

	$('#it').click(function(){
		$('#other').prop('disabled',true);
	});
	
	var x = getParameterByName("id");

	if (x != "") {
		$("#btnSubmit").text("Update");
		$('#frmEducation').attr('action','updateEdu.hrd' + x);
		$.ajax({
			url : "getEdu.hrd",
			data : {
				'id' : x
			},
			success : function(data) {
                $('#frmEducation').attr('action','updateEdu.hrd?id=' + x);
				$('#image-url').val(data[0].image_url);
                $('#id').val(x);
                $('#school').val(data[0].school);
                $('#degree').val(data[0].degree);
                $('#location').val(data[0].location);
                $('#start_year').val(data[0].start_year);
                $('#end_year').val(data[0].end_year);
                $('#current_year').val(data[0].current_year);
                $('#description').val(data[0].description);
               
                if (data[0].major == "IT"){
                	$('#it').prop('checked',true);
                }
                else{
                	$('#ot').prop('checked',true);
                	$('#other').prop('disabled',false);
                	$('#other').val(data[0].major);
                }
                
                $('#it').click(function(){
            		$('#other').val('');
            		$('#other').attr('placeholder','eg.Environment');
            	});

                if(data[0].image_url != null || data[0].image_url != ""){
					$('#upload').removeClass('fileupload-new').addClass('fileupload-exists');
					if(data[0].image_url == ""){
						data[0].image_url = "../default.png";
					}
					$('div.fileupload-preview').append('<img src="../images/uploads/education/' + data[0].image_url + '" style="max-height: 150px;">');
				}
			}
		});
	}
});
</script>
</body>
</html>
