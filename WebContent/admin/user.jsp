<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">
  <meta name="description" content="">
  <meta name="author" content="ThemeBucket">

  <title>User</title>
  
  <!--common styles -->
  <jsp:include page="includes/common_styles.jsp"></jsp:include>
  
  <link rel="stylesheet" type="text/css" href="css/bootstrap-fileupload.min.css">
  
  <!--dynamic table-->
  <link href="js/advanced-datatable/css/demo_page.css" rel="stylesheet" />
  <link href="js/advanced-datatable/css/demo_table.css" rel="stylesheet" />
  <link rel="stylesheet" href="js/data-tables/DT_bootstrap.css" />

  <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!--[if lt IE 9]>
  <script src="js/html5shiv.js"></script>
  <script src="js/respond.min.js"></script>
  <![endif]-->

<body class="sticky-header"  ng-app="myApp" ng-controller="tableCtrl">

<section>
    <!-- left side start -->
    <jsp:include page="includes/left_side.jsp"></jsp:include>
    <!-- left side end -->
    
    <!-- main content start -->
    <div class="main-content" >

        <!-- header section start-->
        <jsp:include page="includes/top_side.jsp"></jsp:include>
        <!-- header section end-->

        <!--body wrapper start-->
        <div class="wrapper">
            <div class="panel panel-success">
            
            
                <div class="panel-heading">
                    <h3 class="panel-title">User Informations</h3>
                </div>
                
                
                <div class="panel-body table-responsive">
                  	<a href="#myModal" data-toggle="modal" class="btn btn-md btn-success">
                      	Add New User
                    </a>
                    <div class="adv-table">
                        <table  class="display table table-bordered table-striped" id="dynamic-table">
                            <thead>
                                <tr>
                                	<th class="text-center">#</th>
                                	<th class="text-center">First Name</th>
                                    <th class="text-center">Last Name</th>
                                    <th class="text-center">Gender</th>
                                    <th class="text-center">User Name</th>
                                    <th class="text-center">E-mail</th>
                                    <!-- <th class="text-center">Password</th> -->
                                    <th class="text-center">Actions</th>
                                </tr>
                            </thead>
                            <tbody id="allUser">
		
                            </tbody>
                        </table>
                    </div>
                </div>
                
                
                <div class="panel-body table-responsive">
                    <section class="panel">
                        <div class="panel-body">
                            <div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="myModal" class="modal fade" style="display: none;">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button aria-hidden="true" data-dismiss="modal" class="close" type="button"><span class="fa fa-times"></span></button>
                                            <h4 class="modal-title">Update Admin Info</h4>
                                        </div>
                                        <div class="modal-body">

                                            <form role="form" id="frmRegister" action="updateUserInfo.hrd" method="post">
                                            	 <input type="text" id="id" name="id" hidden />
                                                <div class="form-group">
                                                    <label for="exampleInputEmail1">First Name</label>
                                                    <input type="text" class="form-control" id="fname" name="fname" placeholder="First Name">
                                                </div>
                                                <div class="form-group">
                                                    <label for="exampleInputPassword1">Last Name</label>
                                                    <input type="text" class="form-control" id="lname" name="lname" placeholder="Last Name">
                                                </div>
                                                <div class="form-group">
                                                    <label for="exampleInputPassword1">E-mail</label>
                                                    <input type="text" class="form-control" id="email" name="email" placeholder="E-mail">
                                                </div>
                                                <div class="form-group">   
						                            <div class="col-lg-12">
						                                <div class="radios">
						                                    <label for="radio-01" class="label_radio col-lg-6 col-sm-6">
						                                        <input type="radio" checked="checked" value="Male" id="male" name="gender"> Male
						                                    </label>
						                                    <label for="radio-02" class="label_radio col-lg-6 col-sm-6">
						                                        <input type="radio" value="Female" id="female" name="gender"> Female
						                                    </label>
						                                </div>
						                            </div>
						                        </div>
                                                
                                                <button class="btn btn-success btn-md btn-block" type="submit">Update</button>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                    <!-- <table class="table table-hover general-table">
                        <thead>
                            <tr>
                                <th> #</th>
                                <th>User Name</th>
                                <th>Password</th>
                                <th>First Name</th>
                                <th>Last Name</th>
                                <th>E-mail</th>
                                <th>Actions</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr ng-repeat="user in users">
                                <td>{{$index + 1}}</td>
                                <td>{{user.username}}</td>
                                <td>{{user.password}}</td>
                                <td>{{user.fname}}</td>
                                <td>{{user.lname}}</td>
                                <td>{{user.email}}</td>
                                <td>
                                    <span class="label label-info label-mini">Edit</span>
                                    <span class="label label-warning label-mini delete-user" ng-click="deleteUser( user.id )">Delete</span>
                                </td>
                            </tr>
                        </tbody>
                    </table> -->
                </div>
            </div>
        </div>
        <!--body wrapper end-->
        <!--footer section start-->
        <jsp:include page="includes/footer.jsp"></jsp:include>
        <!--footer section end-->

    </div>
    <!-- main content end-->
</section>


<!-- common scripts -->
<jsp:include page="includes/common_scripts.jsp"></jsp:include>
<script type="text/javascript" src="js/bootstrap-fileupload.min.js"></script>
	
<!-- Validation -->
<script type="text/javascript" src="js/jquery.validate.min.js"></script>
<script>
var Script = function () {
    $().ready(function() {
        // validate education adding form on keyup and submit
        $("#frmRegister").validate({
            rules: {
            	firstname: {
            		required: true
            	},
            	lastname: {
            		required: true
            	},
                username: {
                    required: true
                },
                email: {
                	required: true,
                	email: true
                },
                password: {
                    required: true
                },
                confirm_password: {
                    equalTo: "#password",
                    required: true
                }
            }
        });
    });
}();
</script>


<!--dynamic table-->
<script type="text/javascript" src="js/advanced-datatable/js/jquery.dataTables.js"></script>
<script type="text/javascript" src="js/data-tables/DT_bootstrap.js"></script>

<script>
loadData();
function loadData(){
	$.ajax({
		method: 'get',
		url: 'listUser.hrd',
		success: function(response){
			var tr = "", index = 1;
			for (var x in response){
				tr += '<tr class="gradeX">' +
			            '<td class="text-center">' + index + '</td>' +
			            '<td class="center">' + response[x].fname + '</td>' +
			            '<td class="center">' + response[x].lname + '</td>' +
			            '<td class="center">' + response[x].gender + '</td>' +
			            '<td class="center">' + response[x].username + '</td>' +
			            '<td class="center">' + response[x].email + '</td>' +
			            '<td class="center">' +
			            	'<a href="#myModal" data-toggle="modal" onclick="fillInfo('+ response[x].id +')" class="mn-has-sub"><span class="label label-info label-mini" >Edit</span></a>' + 
			            	'<a href="#" onclick="'+ (response[x].status=="t"?"deleteUser":"reactiveUser") +'('+ response[x].id +')"><span class="label '+ (response[x].status=="t"?"label-warning":"label-success")+' label-mini delete-user">'+ (response[x].status=="t"?"Disable":"Reactive") +'</span>' +
			            '</td>' +
			          '</tr>';reactiveUser.hrd
			    index++;
			}
			$('#allUser').html(tr);
			$('#dynamic-table').dataTable();
			
			$('#myModal').on('hidden.bs.modal', function() {
				$('#id').val("");
				$('#fname').val("");
				$('#lname').val("");
				$('input:radio[name="gender"][value="Male"]').prop('checked', true);
				$('#email').val("");
			});
		}
	});
}

	function fillInfo(dev_id){
		$.ajax({
			method: 'POST',
			url: 'getUser.hrd',
			data:{
				id: dev_id
			}, 
			success: function(response){
				$('#id').val(response.id);
				$('#fname').val(response.fname);
				$('#lname').val(response.lname);
				$('input:radio[name="gender"][value="' + response.gender + '"]').prop('checked', true);
				$('#email').val(response.email);
			}
		});
	}
	
	function deleteUser(dev_id){
		$.ajax({
			method: 'POST',
			url: 'deleteUser.hrd',
			data:{
				id: dev_id
			}, 
			success: function(response){
				loadData();
			}
		});
	}
	
	function reactiveUser(dev_id){
		$.ajax({
			method: 'POST',
			url: 'reactiveUser.hrd',
			data:{
				id: dev_id
			}, 
			success: function(response){
				loadData();
			}
		});
	}
	
</script>



</body>
</html>
