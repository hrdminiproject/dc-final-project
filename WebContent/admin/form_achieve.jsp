<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">
  <meta name="description" content="">
  <meta name="author" content="ThemeBucket">

  <title>Achievement</title>
  
  <!--common styles -->
  <jsp:include page="includes/common_styles.jsp"></jsp:include>
  <link rel="stylesheet" type="text/css" href="css/bootstrap-fileupload.min.css">
   
  <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!--[if lt IE 9]>
  <script src="js/html5shiv.js"></script>
  <script src="js/respond.min.js"></script>
  <![endif]-->
</head>

<body class="sticky-header">

<section>
    <!-- left side start -->
    <jsp:include page="includes/left_side.jsp"></jsp:include>
    <!-- left side end -->
    
    <!-- main content start -->
    <div class="main-content">

        <!-- header section start-->
        <jsp:include page="includes/top_side.jsp"></jsp:include>
        <!-- header section end-->

        <!--body wrapper start-->
        <div class="wrapper">
            <!--Achievement Information -->
            <div class="row">
		        <div class="col-lg-12">
		            <section class="panel panel-success">
		                <header class="panel-heading">
		                    Achievement Information
		                </header>
		                <div class="panel-body">
		                    <div class="form">
		                        <form class="cmxform form-horizontal adminex-form" id="frmAchieve" method="post" action="addAchieve.hrd" enctype="multipart/form-data">
		                        	<input type="hidden" id="image_url" name="image_url">
		                            <div class="form-group ">
		                                <label for="firstname" class="control-label col-lg-2">Title *</label>
		                                <div class="col-lg-10 has-success">
		                                    <input class=" form-control" id="title" name="title" type="text" />
		                                </div>
		                            </div>
		                            <div class="form-group ">
		                                <label for="lastname" class="control-label col-lg-2">Year *</label>
		                                <div class="col-lg-10 has-success">
		                                    <input class=" form-control" id="year" name="year" type="text" />
		                                </div>
		                            </div>
		                    
					                <div class="form-group ">
					                    <label for="username" class="control-label col-lg-2">Location *</label>
					                    <div class="col-lg-10 has-success">
					                         <input class="form-control" id="location" name="location" type="text" />
					                    </div>
					                </div>
					             
					                <div class="form-group ">
					                    <label for="confirm_password" class="control-label col-lg-2">Description *</label>
					                    <div class="col-lg-10 has-success">
					                        <textarea rows="5" cols="60" class="form-control" id="description" name="description" placeholder="Some text here..."></textarea>
					                    </div>
					                </div>
						                            
					                <div class="form-group">
						              	<label class="col-sm-2 control-label col-lg-2" for="inputSuccess">Reference *</label> 
						              	<div class="col-sm-10">
					                       <div class="fileupload fileupload-new" id="upload" data-provides="fileupload"><input type="hidden" value="" name="">
					                           <div class="fileupload-new thumbnail" style="width: 200px; height: 150px;">
					                               <img src="http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image" alt="">
					                           </div>
					                           <div class="fileupload-preview fileupload-exists thumbnail" style="max-width: 200px; max-height: 150px; line-height: 10px;"></div>
					                           <div>
					                               <span class="btn btn-default btn-file">
					                               <span class="fileupload-new"><i class="fa fa-paper-clip"></i> Select image</span>
					                               <span class="fileupload-exists"><i class="fa fa-undo"></i> Change</span>
					                               	   <input type="file" class="default" name="evidence" accept="image/*">
					                               </span>
					                                   <a href="#" class="btn btn-danger fileupload-exists" data-dismiss="fileupload"><i class="fa fa-trash"></i> Remove</a>
					                            </div>
					                        </div>
						                 </div>
					                </div>
					                <div class="form-group">
					                   <div class="col-md-offset-2 col-lg-2">
					                   	<button type="submit" class="btn btn-success" id="btnSubmit">Save</button>
					                      	<button type="button" class="btn btn-warning" id="btnCancel">Cancel</button>
					                    </div>
					                </div>
		                    	</form>
		               		</div>
		            	</div>
		        	</section>
		    	</div>
			</div>     	
        <!-- End of Achievement Information -->
        </div>
        <!--body wrapper end-->
        <!--footer section start-->
        <jsp:include page="includes/footer.jsp"></jsp:include>
        <!--footer section end-->

    </div>
    <!-- main content end-->
</section>

<!-- common scripts -->
<jsp:include page="includes/common_scripts.jsp"></jsp:include>
<script type="text/javascript" src="js/bootstrap-fileupload.min.js"></script>

<!-- Validation -->
<script type="text/javascript" src="js/jquery.validate.min.js"></script>
<script type="text/javascript" src="js/achieve_validate.js"></script>

<script>
$(function(){

	var x = getParameterByName("id");

		if (x != "") {
			$("#btnSubmit").text("Update");
			$.ajax({
				url : "getAchieve.hrd",
				data : {
					'id' : x
				},
				success : function(data) {
                    $('#frmAchieve').attr('action','updateAchieve.hrd?id=' + x);
					$('#image-url').val(data[0].image_url);
                    $('#id').val(x);
                    $('#title').val(data[0].title);
                    $('#year').val(data[0].year);
                    $('#location').val(data[0].location);
                    $('#description').val(data[0].description);
                    
					if(data[0].image_url != null || data[0].image_url != ""){
						$('#upload').removeClass('fileupload-new').addClass('fileupload-exists');
						if(data[0].image_url == ""){
							data[0].image_url = "../default.png";
						}
						$('div.fileupload-preview').append('<img src="images/uploads/achievement/' + data[0].image_url + '" style="max-height: 150px;">');
					}
				}
			});
		}
	});
</script>

</body>
</html>
