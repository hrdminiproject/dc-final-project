<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">
  <meta name="description" content="">
  <meta name="author" content="ThemeBucket">

  <title>Blank</title>
   
  <!--common styles -->
  <jsp:include page="includes/common_styles.jsp"></jsp:include>
  
  <!--dynamic table-->
  <link href="js/advanced-datatable/css/demo_page.css" rel="stylesheet" />
  <link href="js/advanced-datatable/css/demo_table.css" rel="stylesheet" />
  <link rel="stylesheet" href="js/data-tables/DT_bootstrap.css" />
  
  <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!--[if lt IE 9]>
  <script src="js/html5shiv.js"></script>
  <script src="js/respond.min.js"></script>
  <![endif]-->
</head>

<body class="sticky-header">

<section>
    <!-- left side start -->
    <jsp:include page="includes/left_side.jsp"></jsp:include>
    <!-- left side end -->
    
    <!-- main content start -->
    <div class="main-content" >

        <!-- header section start-->
        <jsp:include page="includes/top_side.jsp"></jsp:include>
        <!-- header section end-->

        <!--body wrapper start-->
        <div class="wrapper">
           <div class="row">
                <div class="col-sm-12">
                    <section class="panel panel-success">
                        <header class="panel-heading">
                            Developer Background
                        </header>
                        <div class="panel-body table-responsive">
                            <div class="adv-table">
                                <table  class="display table table-bordered table-striped" id="dynamic-table">
                                    <thead>
                                        <tr>
                                        	<th>#</th>
                                            <th class="text-center">Name</th>
                                            <th class="text-center">Gender</th>
                                            <th class="text-center">Contact</th>
                                            <th class="text-center">Skill</th>
                                            <th class="text-center">Nationality</th>
                                            <th class="text-center">Action</th>
                                        </tr>
                                    </thead>
                                    <tbody id="allDev">
										
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </section>
                </div>
            </div>
        </div>
        <!--body wrapper end-->
        <!--footer section start-->
        <jsp:include page="includes/footer.jsp"></jsp:include>
        <!--footer section end-->

    </div>
    <!-- main content end-->
</section>
<!-- common scripts -->
<jsp:include page="includes/common_scripts.jsp"></jsp:include>

<!--dynamic table-->
<script type="text/javascript" src="js/advanced-datatable/js/jquery.dataTables.js"></script>
<script type="text/javascript" src="js/data-tables/DT_bootstrap.js"></script>

<script>
	$.ajax({
		method: 'get',
		url: 'listAllDev.hrd',
		success: function(response){
			var tr = "", index = 1;
			for (var x in response){
				tr += '<tr class="gradeX">' +
			            '<td>' + index + '</td>' +
			            '<td class="center">' + response[x].fname + '</td>' +
			            '<td class="center">' + response[x].gender + '</td>' +
			            '<td class="center">' + response[x].email + '</td>' +
			            '<td class="center">' + response[x].specialty_skill + '</td>' +
			            '<td class="center">' + response[x].nationality + '</td>' +
			            '<td class="center">' +
				            	'<a href="profile.jsp?dev_id=' + response[x].id + '">' +
				            		'<span class="label label-success label-mini">View</span>' +
				            	'</a>' + 
			            '</td>' +
			          '</tr>';
			    index++;
			}
			$('#allDev').html(tr);
			$('#dynamic-table').dataTable();
		}
	});


 	/* var app = angular.module('myApp', []);
 	app.controller('tableCtrl', function($scope, $http) {
 		$scope.loadDev = function(){
 			$http({
 			    method: 'get',
 			    url: 'listAllDev',
 			}).success(function(response) {
 		    	$scope.devs = response;
 		    });
 		};
 		$scope.loadDev();
		$scope.deleteAch = function (achId){		
			new $.flavr({
				title		: 'Are your sure ?',
			    dialog      : 'confirm',
			    onConfirm   : function( $container ){
					$.ajax({
				        method: 'POST',
				        url: 'deleteAch',
				        data: {
				            id: achId
				        },
				        success:function(result){
				        	$scope.loadAch();
				        }
				    });
			        return true;
			    }
			});
		}; */
</script>


</body>
</html>
