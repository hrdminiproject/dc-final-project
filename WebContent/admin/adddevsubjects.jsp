<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">
  <meta name="description" content="">
  <meta name="author" content="ThemeBucket">

  <title>Add DevSubject</title>
  
  <!--common styles -->
  <jsp:include page="includes/common_styles.jsp"></jsp:include>
  <link rel="stylesheet" href="css/sub-style.css">
  <link rel="stylesheet" href="css/register_detail.css">
  <link rel="stylesheet" href="css/rate-bar.css">
  
  <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!--[if lt IE 9]>
  <script src="js/html5shiv.js"></script>
  <script src="js/respond.min.js"></script>
  <![endif]-->
</head>

<body class="sticky-header">

<section>
    <!-- left side start -->
    <jsp:include page="includes/left_side.jsp"></jsp:include>
    <!-- left side end -->
    
    <!-- main content start -->
    <div class="main-content" >

        <!-- header section start-->
        <jsp:include page="includes/top_side.jsp"></jsp:include>
        <!-- header section end-->

        <!--body wrapper start-->
        <div class="wrapper">
        	<!-- Pannel for Categories -->
            <div class="row">                
                <div class="col-md-12"> 
                	 <form action="addDevSub.hrd" method="post">
	                   <!-- pannel subjects -->
			            <div class="panel panel-success">
			                 <div class="panel-heading">
			                     <h3 class="panel-title">Developer Subjects Form</h3>
			                 </div>
			                 
			                 <!-- Subject contents -->
			                 <div class="panel-body">
			                 
			                     <div class="form-group">
			                         <!-- developer subject rating  -->
			                         <div class="panel">
						                <header class="panel-heading"> Rating Skills</header>
					                    <div class="panel-body">
					                    	<div class="col-sm-3">
									            <!-- skill type combobox -->
							                    <div class="form-group">
							                    	<div class="col-sm-3"><br/>
								                    	<label class="control-label">Subject:</label>
								                    </div>
								                    <div class="col-sm-9">
								                    <br/>
									                    <select class="col-sm-10 form-control m-bot15" id="sid" name="sid">
							                             
							                            </select>
							                        </div>
						                        </div>
							             	</div>
							            
								               <!--   Rating Skills Pannel -->
						            		<div class="panel">
								              <div class="panel-body">
										   	  	<!-- rate bar for developer subject -->
											    <div class="col-sm-12">
			                                         <br/>
											         <section class="container-rate-bar">
											
											             <input type="radio" class="radio" name="progress" value="0" id="reset" checked>
											             <label for="reset" class="label">Reset</label>
											
											             <input type="radio" class="radio" name="progress" value="1" id="ten">
											             <label for="ten" class="label">10%</label>
											
											             <input type="radio" class="radio" name="progress" value="2" id="twenty">
											             <label for="twenty" class="label">20%</label>
											
											             <input type="radio" class="radio" name="progress" value="3" id="thirty">
											             <label for="thirty" class="label">30%</label>
											
											             <input type="radio" class="radio" name="progress" value="4" id="forty">
											             <label for="forty" class="label">40%</label>
											
											             <input type="radio" class="radio" name="progress" value="5" id="fifty">
											             <label for="fifty" class="label">50%</label>
											
											             <input type="radio" class="radio" name="progress" value="6" id="sixty">
											             <label for="sixty" class="label">60%</label>
											
											             <input type="radio" class="radio" name="progress" value="7" id="seventy">
											             <label for="seventy" class="label">70%</label>
											
											             <input type="radio" class="radio" name="progress" value="8" id="eighty">
											             <label for="eighty" class="label">80%</label>
											
											             <input type="radio" class="radio" name="progress" value="9" id="ninety">
											             <label for="ninety" class="label">90%</label>
											
											             <input type="radio" class="radio" name="progress" value="10" id="onehundred">
											             <label for="onehundred" class="label">100%</label>
											
											             <div class="progress" style="width:490px">
											                  <div class="progress-bar"></div>
											             </div>
											        </section>
											    </div>
								              </div>
								           </div> 
								         </div>
            							</div>
			                     </div>			                     
			                     
			                     <!-- developer subject description -->
			                     <div class="form-group">
			                        <label class="col-sm-1 control-label">Description:</label>
			                        <div class="col-sm-11">
			                            <textarea rows="6" name="description" id="description" class="form-control"></textarea>
			                        </div>
			                     </div>
			                 </div>
			                   
					        <!-- button Subject submit -->
		                	<div class="panel-body">
			                	<div class="pull-right">
				                	<a href="myadddevsubject.jsp"><button class="btn btn-info" type="button">Cancel</button></a>
				                    <button class="btn btn-success" id="btn-add" type="submit">Save</button>
								</div>			                
		             		</div>
			            </div> 
			         </form>
                </div>
            </div>
        </div>  
        <!--body wrapper end--> 

        <!--footer section start-->
        <jsp:include page="includes/footer.jsp"></jsp:include>
        <!--footer section end-->
        
    </div>
    <!-- main content end-->
</section>


<!-- common scripts -->
<jsp:include page="includes/common_scripts.jsp"></jsp:include>
<script src="js/angular.min.js"></script>
<script src="js/devsubject-script.js"></script>

<script>
/* function mysave(){
	var check_value = $('.radio:checked').val();
	alert(check_value);
} */
$(function(){
	var x = getParameterByName("subid");
		if (x != "") {
			/* change button Save to Update */
			$("#btn-add").text("Update");
			
			/* change url of form from subject_add to subject_edit */
			$('form').attr("action","updateDevSub.hrd?sid="+x);
			$.ajax({
				url : "getDevSub.hrd",
				data : {
					'id' : x
				},
				success : function(data) {
					/* load data to textbox */
					
					 //$('#dev_id').val(data[0].devid); 
					$('#sid').val(data[0].sid);			
				
					$('#description').val(data[0].description);
					
					switch(data[0].rate){
					case "0":
						$('#reset').attr('checked', 'checked');	
						break;
					case "1":
						$('#ten').attr('checked', 'checked');	
						break;
					case "2":
						$('#twenty').attr('checked', 'checked');	
						break;
					case "3":
						$('#thirty').attr('checked', 'checked');	
						break;
					case "4":
						$('#forty').attr('checked', 'checked');	
						break;
					case "5":
						$('#fifty').attr('checked', 'checked');	
						break;
					case "6":
						$('#sixty').attr('checked', 'checked');	
						break;
					case "7":
						$('#seventy').attr('checked', 'checked');	
						break;
					case "8":
						$('#eighty').attr('checked', 'checked');	
						break;
					case "9":
						$('#ninety').attr('checked', 'checked');	
						break;
					case "10":
						$('#onehundred').attr('checked', 'checked');	
						break;
					}
			}
		});
	}
});
</script>

</body>
</html>
