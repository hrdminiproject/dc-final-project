<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">
  <meta name="description" content="">
  <meta name="author" content="ThemeBucket">

  <title>Subjects</title>
  
  <!--common styles -->
  <jsp:include page="includes/common_styles.jsp"></jsp:include>
  <link rel="stylesheet" href="css/sub-style.css">
  
  <!--dynamic table-->
  <link href="js/advanced-datatable/css/demo_page.css" rel="stylesheet" />
  <link href="js/advanced-datatable/css/demo_table.css" rel="stylesheet" />
  <link rel="stylesheet" href="js/data-tables/DT_bootstrap.css" />
  
  <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!--[if lt IE 9]>
  <script src="js/html5shiv.js"></script>
  <script src="js/respond.min.js"></script>
  <![endif]-->
</head>

<body class="sticky-header" ng-app="myApp">

<section>
    <!-- left side start -->
    <jsp:include page="includes/left_side.jsp"></jsp:include>
    <!-- left side end -->
    
    <!-- main content start -->
    <div class="main-content" >

        <!-- header section start-->
        <jsp:include page="includes/top_side.jsp"></jsp:include>
        <!-- header section end-->

        <!--body wrapper start-->
        <div class="wrapper" ng-controller="tableCtrl">
            <div class="panel panel-success">
                <div class="panel-heading">
                    <h3 class="panel-title">Subject Information</h3>
                </div>
                <div class="panel-body table-responsive">
                  	<a href="addsubjects.jsp" class="btn btn-md btn-info">
                      	Add New Subject
                    </a>
                    <div class="adv-table">
                        <table  class="display table table-bordered table-striped" id="dynamic-table">
                            <thead>
                                <tr>
	                                <th class="text-center">#</th>
	                                <th class="text-center">Subject</th>
	                                <th class="text-center">Category</th>
	                                <th class="text-center">Action</th>
                                </tr>
                            </thead>
                            <tbody id="allSub">
								<!-- Load from jquery -->
                            </tbody>
                        </table>
                    </div>
                </div>
        	</div>
        </div>

        <!--footer section start-->
        <jsp:include page="includes/footer.jsp"></jsp:include>
        <!--footer section end-->
    </div>
    <!-- main content end-->
</section>


<!-- common scripts -->
<jsp:include page="includes/common_scripts.jsp"></jsp:include>

<!--dynamic table-->
<script type="text/javascript" src="js/advanced-datatable/js/jquery.dataTables.js"></script>
<script type="text/javascript" src="js/data-tables/DT_bootstrap.js"></script>

<script src="js/angular.min.js"></script>
<script src="js/subject-script.js"></script>

</body>
</html>