<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">
  <meta name="description" content="">
  <meta name="author" content="ThemeBucket">

  <title>Experience</title>
  
  <!--common styles -->
  <jsp:include page="includes/common_styles.jsp"></jsp:include>
  
  <link rel="stylesheet" type="text/css" href="css/bootstrap-fileupload.min.css">

  <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!--[if lt IE 9]>
  <script src="js/html5shiv.js"></script>
  <script src="js/respond.min.js"></script>
  <![endif]-->
    <style>
  
  	a#browseFileButton:link { color:#FFF; text-decoration:none; font-weight:normal; }
	a#browseFileButton:visited { color: #FFF; text-decoration:none; font-weight:normal; }
	a#browseFileButton:hover { color: #FFF; text-decoration:none; font-weight:normal; }
	a#browseFileButton:active { color: #FFF; text-decoration:none; font-weight:normal; }
  </style>
</head>

<body class="sticky-header" ng-app="myApp">

<section>
    <!-- left side start -->
    <jsp:include page="includes/left_side.jsp"></jsp:include>
    <!-- left side end -->
    
    <!-- main content start -->
    <div class="main-content" >

        <!-- header section start-->
        <jsp:include page="includes/top_side.jsp"></jsp:include>
        <!-- header section end-->

        <!--body wrapper start-->
        <div class="wrapper">
            <div class="panel panel-success">
                <div class="panel-heading">
                    <h3 class="panel-title">Experiences</h3>
                </div>
                <div class="panel-body table-responsive">
                    <table class="table table-hover general-table">
                     <form method="POST" id="save_form" enctype="multipart/form-data"></form>
                        <thead>
                            <tr>
                                <th> #</th>
                                <th>Place</th>
                                <th>Position</th>
                                <th>Duration</th>
                                <th>Type</th>
                                <th>Skill</th>
                                <th>Action</th>
                            </tr>
                            <tr>
							    <td>
							        <input type="hidden" name="description" value="No Description" form="save_form" />
							        <a href="#" id="browseFileButton" onclick="document.getElementById('fileID').click(); return false;" class="btn btn-info" />Browse Document</a>
							        <input type="file" id="fileID" style="visibility: hidden; color:white; width:10px; " name="evidence" accept="image/*" form="save_form" />
							    </td>
							    <td>
							        <input class="form-control" id="place" name="place" type="text" form="save_form" />
							    </td>
							     <td>
							        <input class="form-control" id="position" name="position" type="text" form="save_form" />
							    </td>
							    <td>
							        <input class="form-control" id="duration" name="duration" type="number" form="save_form" />
							    </td>
							    <td>
							        <select class="form-control" id="type" name="type" form="save_form">
							             <option>Full Time</option>
			                             <option>Part Time</option>
			                             <option>Volunteer</option>
			                             <option>Internship</option>
			                             <option>Free Lance</option>
							        </select>
							    </td>
							     <td>
							        <select class="form-control" id="skill" name="skill" form="save_form">
											<option>Andriod</option
											<option>Angularjs</option>
											<option>ASP.NET</option>
											<option>Bootstrap</option>
											<option>C</option>
											<option>C#</option>
											<option>C++</option>
											<option>CakePHP</option>
											<option>CodeIgniter</option>
											<option>CoffeeScript</option>
											<option>CSS</option>
											<option>Drupal</option>
											<option>Hibernate</option>
											<option>Html</option>
											<option>Java</option>
											<option>Javascript</option>
											<option>Joomla</option>
											<option>JQuery</option>
											<option>Json</option>
											<option>JSP</option>
											<option>Linux</option>
											<option>Ms. Access</option>
											<option>MySQL</option>
											<option>NodeJS</option>
											<option>Objective C (IOS)</option>
											<option>OpenCart</option>
											<option>Oracle</option>
											<option>Perl</option>
											<option>PHP</option>
											<option>PostgreSQL</option>
											<option>Python</option>
											<option>Ruby</option>
											<option>Servlet</option>
											<option>Spring</option>
											<option>SQL Server</option>
											<option>SQLite</option>
											<option>Swift</option>
											<option>Typo3</option>
											<option>VB Script</option>
											<option>VB.net</option>
											<option>Wordpress</option>
											<option>Yii</option>
											<option>Zend</option>
											
											
											
											
									</select>
									</td>
							    <td>
							        <button type="submit" class="btn btn-success" id="btnSave" form="save_form">Save</button>
							    </td>
							</tr>
                        </thead>
                        <tbody id="experienceTbody" ng-controller="tableCtrl">
                            <tr ng-repeat="exp in experiences | orderBy : '-id'">
                            	<td>{{ $index + 1 }}</td>
                                <td>{{exp.place}}</td>
                                <td>{{exp.position}}</td>
                                <td>{{exp.duration}}</td>
                                <td>{{exp.type}}</td>
                                <td>{{exp.skill}}</td>
                                <td>
                                	<a href="form_exp.jsp?id={{exp.id}}"><i class="fa fa-gear fa-lg"></i></a>&nbsp;
									<a href="#" ng-click="deleteExp(exp.id)"><i class="fa fa-trash-o fa-lg"></i></a>                        
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <!--body wrapper end-->
        
        <!--footer section start-->
        <jsp:include page="includes/footer.jsp"></jsp:include>
        <!--footer section end-->

    </div>
    <!-- main content end-->
</section>

<!-- common scripts -->
<jsp:include page="includes/common_scripts.jsp"></jsp:include>
<script type="text/javascript" src="js/bootstrap-fileupload.min.js"></script>

<script>
	var app = angular.module('myApp', []);
	app.controller('tableCtrl', function($scope, $http) {
		$scope.loadExp = function(){
			$http({
			    method: 'post',
			    url: 'listExp.hrd',
			    headers: {'Content-Type': 'application/x-www-form-urlencoded'}
			}).success(function(response) {
		    	$scope.experiences = response;
		    });
		}
		$scope.loadExp();
		$scope.deleteExp = function (expId){		
			new $.flavr({
				title		: 'Are your sure ?',
			    dialog      : 'confirm',
			    onConfirm   : function( $container ){
					$.ajax({
				        method: 'POST',
				        url: 'deleteExp.hrd',
				        data: {
				            id: expId
				        },
				        success:function(result){
				        	$scope.loadExp();
				        }
				    });
			        return true;
			    }
			});
		};
	});
</script>

<script>
$("#fileID").change(function(){
	$("#browseFileButton").html("File Selected")
}).click(function(){
    $(this).val("")
});


//Program a custom submit function for the form
$("form#save_form").submit(function(event){

  //disable the default form submission
  event.preventDefault();
 
  //grab all form data  
  var formData = new FormData($(this)[0]);
  
  $.ajax({
    url: 'addExp.hrd',
    type: 'POST',
    data: formData,
    async: false,
    cache: false,
    contentType: false,
    processData: false,
    success: function (returndata) {
        if(returndata == ""){
            alert("Invalid input");
            return;
        }
        
      angular.element('#experienceTbody').scope().loadExp();
      $('form#save_form')[0].reset();     
    }
  });
 
  return false;
});

</script>


</body>
</html>
