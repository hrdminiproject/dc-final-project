<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">
  <meta name="description" content="">
  <meta name="author" content="ThemeBucket">

  <title>Add Subject</title>
  
  <!--common styles -->
  <jsp:include page="includes/common_styles.jsp"></jsp:include>
  <link rel="stylesheet" href="css/sub-style.css">
  <link rel="stylesheet" type="text/css" href="css/bootstrap-fileupload.min.css">
  
  <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!--[if lt IE 9]>
  <script src="js/html5shiv.js"></script>
  <script src="js/respond.min.js"></script>
  <![endif]-->
</head>

<body class="sticky-header">

<section>
    <!-- left side start -->
    <jsp:include page="includes/left_side.jsp"></jsp:include>
    <!-- left side end -->
    
    <!-- main content start -->
    <div class="main-content" >

        <!-- header section start-->
        <jsp:include page="includes/top_side.jsp"></jsp:include>
        <!-- header section end-->

        <!--body wrapper start-->
        <div class="wrapper">
        	<!-- Pannel for Categories -->
            <div class="row">                
                <div class="col-md-12"> 
                	 <form action="addSub.hrd" method="post" enctype="multipart/form-data">
	                   <!-- pannel subjects -->
			            <div class="panel panel-success">
			                 <div class="panel-heading">
			                     <h3 class="panel-title">Form Subjects</h3>
			                 </div>
			                 
			                 <!-- Subject contents -->
			                 <div class="panel-body">
			                     <div class="form-group">
			                     
			                         <!-- subject name  -->
			                         <label class="col-md-1 col-sm-1 control-label">Subject:</label>
			                         <div class="col-md-11 col-sm-11">
			                             <input class="form-control" id="name" name="name" type="text" placeholder="Enter subject name">
			                         </div>
			                     </div><br/><br/>
			                     
			                     <!-- subject description -->
			                     <div class="form-group">
			                        <label class="col-sm-1 control-label">Description:</label>
			                        <div class="col-sm-11">
			                            <textarea rows="6" name="description" id="description" class="form-control"></textarea>
			                        </div>
			                     </div>
			             		
			             		<!-- skill type combobox -->
			                    <div class="form-group">
			                    	<div class="col-sm-1"><br/>
				                    	<label class="control-label">Skill:</label>
				                    </div><br/><br/>
				                    <div class="col-sm-11">
				                    <br/>
					                    <select class="col-sm-10 form-control m-bot15" id="catid" name="catid">
			                             
			                            </select>
			                        </div>
		                        </div>
		                        
		                        <!-- subject image uploading -->
			                     <div class="form-group margin">
					                <label class="col-sm-1 control-label col-lg-1"><br/>Reference:</label> 
					                    <div class="col-sm-11"><br/>
				                            <div class="fileupload fileupload-new has-success" id="upload" data-provides="fileupload"><input type="hidden" value="" name="">
				                                <div class="fileupload-new thumbnail" style="width: 200px; height: 150px;">
				                                     <img src="http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image" id="evidence" alt="">
				                                </div>
				                                <div class="fileupload-preview fileupload-exists thumbnail" style="max-width: 200px; max-height: 150px; line-height: 10px;"></div>
				                                <div> <br/>
				                                    <span class="btn btn-default btn-file">
				                                    <span class="fileupload-new"><i class="fa fa-paper-clip"></i> Select image</span>
				                                    <span class="fileupload-exists"><i class="fa fa-undo"></i> Change</span>
				                                    <input type="file" class="default" name="evidence">
				                                    </span>
				                                     <a href="#" class="btn btn-danger fileupload-exists" data-dismiss="fileupload"><i class="fa fa-trash"></i> Remove</a>
				                                </div>
				                           </div>
				                      </div>
					             </div> 
			                 </div>
			                   
					        <!-- button Subject submit -->
		                	<div class="panel-body">
			                	<div class="pull-right">
				                	<a href="myaddsubject.jsp"><button class="btn btn-info" type="button">Cancel</button></a>
				                    <button class="btn btn-success" id="btn-add" type="submit">Save</button>
								</div>			                
		             		</div>
			            </div> 
			         </form>
                </div>
            </div>
        </div>  
        <!--body wrapper end--> 

        <!--footer section start-->
        <jsp:include page="includes/footer.jsp"></jsp:include>
        <!--footer section end-->
        
    </div>
    <!-- main content end-->
</section>


<!-- common scripts -->
<jsp:include page="includes/common_scripts.jsp"></jsp:include>
<script src="js/subject-script.js"></script>
<script type="text/javascript" src="js/bootstrap-fileupload.min.js"></script>

<script>
$(function(){	
	var x = getParameterByName("id");
		if (x != "") {
			/* change button Save to Update */
			$("#btn-add").text("Update");
			
			/* change url of form from subject_add to subject_edit */
			$('form').attr("action","updateSub.hrd?id="+x);
			$.ajax({
				url : "getSub.hrd",
				data : {
					'id' : x
				},
				success : function(data) {
					//alert(data);
					/* load data to textbox */
					$('#name').val(data.name);
					$('#description').val(data.description);
					$('#catid').val(data.catid);
						
					if(data.image_url != null || data.image_url != ""){
						$('#upload').removeClass('fileupload-new').addClass('fileupload-exists');
						if(data.image_url == ""){
							data.image_url = "../default.png";
						}
						$('div.fileupload-preview').append('<img src="../images/uploads/subject/' + data.image_url + '" style="max-height: 150px;">');
					}
			}
		});
	}
	
	
});
</script>

</body>
</html>