<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="ThemeBucket">
    <link rel="shortcut icon" href="#" type="image/png">

    <title>Registration</title>

  <!--common styles -->
  <jsp:include page="includes/common_styles.jsp"></jsp:include>

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="js/html5shiv.js"></script>
    <script src="js/respond.min.js"></script>
    <![endif]-->
    <style>
    	.form-signin{
    		margin: 30px auto;
    	}
    </style>
</head>

<body class="login-body">

<div class="container">

    <form class="form-signin" id="frmRegister" action="addUser.hrd" method="post">
        <div class="form-signin-heading text-center">
            <img src="images/regist.png" alt=""/>
        </div>
        
        <div class="login-wrap">
        	<div class="row">
                <div class="col-lg-12">
              			<div class="form-group ">
                            <div class="col-lg-12">
                                <p >Enter your personal details below</p>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-lg-12">
                                <input class="form-control" type="text" id="firstname" name="firstname" placeholder="First Name" />
                            </div>
                        </div>
						<div class="form-group">
                            <div class="col-lg-12">
							<p></p>
                            </div>
                        </div>
                        
                        <div class="form-group">   
                            <div class="col-lg-12">
                                <input class="form-control" type="text" id="lastname" name="lastname" placeholder="Last Name" />
                            </div>
                        </div>
						<div class="form-group">
                            <div class="col-lg-12">
							<p></p>
                            </div>
                        </div>
                        
                        <div class="form-group"> 
                              
                            <div class="col-lg-12">
                                <input class="form-control" type="text" id="email" name="email" placeholder="Email" />
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-lg-12">
							<p></p>
                            </div>
                        </div>
                        <div class="form-group">   
                            <div class="col-lg-12">
                                <div class="radios">
                                    <label for="radio-01" class="label_radio col-lg-6 col-sm-6">
                                        <input type="radio" checked="checked" value="Male" id="male" name="gender"> Male
                                    </label>
                                    <label for="radio-02" class="label_radio col-lg-6 col-sm-6">
                                        <input type="radio" value="Female" id="female" name="gender"> Female
                                    </label>
                                </div>
                            </div>
                        </div>
						<div class="form-group">
                            <div class="col-lg-12">
							<p></p>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-lg-12">
                                <p> Enter your account details below</p>
                            </div>
                        </div>
                         
                        <div class="form-group">    
                            <div class="col-lg-12">
                                <input class="form-control" type="text" id="username" name="username" placeholder="User Name" />
                            </div>
                        </div>
						<div class="form-group">
                            <div class="col-lg-12">
							<p></p>
                            </div>
                        </div>
                        
                        <div class="form-group">    
                            <div class="col-lg-12">
                                <input class="form-control" type="password" id="password" name="password" placeholder="Password" />
                            </div>
                        </div>
						<div class="form-group">
                            <div class="col-lg-12">
							<p></p>
                            </div>
                        </div>
                        
                        <div class="form-group">    
                            <div class="col-lg-12">
                                <input class="form-control" type="password" id="confirmpassword" name="confirmpassword" placeholder="Confirm Password" />
                            </div>
                        </div>
						<div class="form-group">
                            <div class="col-lg-12">
							 <div class="radios">
                                    <label for="simpleUser" class="label_radio col-lg-6 col-sm-6">
                                        <input type="radio" checked="checked" value="2" id="simpleUser" name="userType"> Simple User
                                    </label>
                                    <label for="Developer" class="label_radio col-lg-6 col-sm-6">
                                        <input type="radio" value="1" id="Developer" name="userType"> Developer
                                    </label>
                                </div>
                            </div>
                        </div>
                          
                        <div class="form-group">
							<div class="col-lg-12">
								<button type="submit" class="btn btn-lg btn-login btn-block">
									<i class="fa fa-check"></i>
								</button>
							</div>
                        </div>
                        <div class="form-group">   
							<div class="col-lg-12">
								<div class="registration">
									Already Registered.
									<a href="../login.jsp" class="">
										Login
									</a>
								</div>
							</div>
							
							<div class="col-lg-12">
								<div class="registration">
					                <a class="" href="/DC/">
					                    Back to Home
					                </a>
					          </div>
							</div>
                        </div>
                </div>
            </div>
        </div>
    </form>
</div>

<!-- Placed js at the end of the document so the pages load faster -->
<script src="js/jquery-1.10.2.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/modernizr.min.js"></script>

<script type="text/javascript" src="js/jquery.validate.min.js"></script>
<script type="text/javascript" src="js/registers_validate.js"></script>
<!--common scripts for all pages-->
<script src="js/scripts.js"></script>
</body>
</html>
