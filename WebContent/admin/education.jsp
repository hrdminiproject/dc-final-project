<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">
  <meta name="description" content="">
  <meta name="author" content="ThemeBucket">

  <title>Education</title>
  
  <!--common styles -->
  <jsp:include page="includes/common_styles.jsp"></jsp:include>
  
  <link rel="stylesheet" type="text/css" href="css/bootstrap-fileupload.min.css">

  <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!--[if lt IE 9]>
  <script src="js/html5shiv.js"></script>
  <script src="js/respond.min.js"></script>
  <![endif]-->
  
  <style>
  
  	a#browseFileButton:link { color:#FFF; text-decoration:none; font-weight:normal; }
	a#browseFileButton:visited { color: #FFF; text-decoration:none; font-weight:normal; }
	a#browseFileButton:hover { color: #FFF; text-decoration:none; font-weight:normal; }
	a#browseFileButton:active { color: #FFF; text-decoration:none; font-weight:normal; }
  </style>
</head>

<body class="sticky-header" ng-app="myApp">

<section>
    <!-- left side start -->
    <jsp:include page="includes/left_side.jsp"></jsp:include>
    <!-- left side end -->
    
    <!-- main content start -->
    <div class="main-content" >

        <!-- header section start-->
        <jsp:include page="includes/top_side.jsp"></jsp:include>
        <!-- header section end-->

        <!--body wrapper start-->
        <div class="wrapper">
            <div class="panel panel-success">
                <div class="panel-heading">
                    <h3 class="panel-title">Education</h3>
                </div>
                <div class="panel-body table-responsive">
                    <table class="table table-hover general-table">
                     <form method="POST" id="save_form" enctype="multipart/form-data"></form>
                        <thead>
                            <tr>
                                <th> #</th>
                                <th>School</th>
                                <th>Degree</th>
                                <th>Major</th>
                                <th>Start Year</th>
                                <th>End Year</th>
                                <th>Current Year</th>
                                <th>Action</th>
                            </tr>
							<tr>
							    <td>
							        <input type="hidden" name="location" value="Battambong" form="save_form" />
							        <input type="hidden" name="description" value="No Description" form="save_form" />
							        <a href="#" id="browseFileButton" onclick="document.getElementById('fileID').click(); return false;" class="btn btn-info" />Browse Document</a>
							        <input type="file" id="fileID" style="visibility: hidden; color:white; width:10px; " name="evidence" accept="image/*" form="save_form" />
							    </td>
							    <td>
							        <input class="form-control" id="school" name="school" type="text" form="save_form" />
							    </td>
							    <td>
							        <select class="form-control" id="degree" name="degree" form="save_form">
							            <option value="Associate">Associate</option>
							            <option value="Bachelor">Bachelor</option>
							            <option value="Master">Master</option>
							            <option value="Ph.D">Ph.D</option>
							            <option value="Doctorate">Doctorate</option>
							            <option value="Other">Other</option>
							        </select>
							    </td>
							    <td>
							        <input class="form-control" id="major" name="major" type="text" form="save_form" />
							    </td>
							    <td>
							        <select class="form-control" id="start_year" name="start_year" form="save_form"></select>
							    </td>
							    <td>
							        <select class="form-control" id="end_year" name="end_year" form="save_form"></select>
							    </td>
							    <td>
							        <select class="form-control m-bot15" id="current_year" name="current_year" form="save_form">
							            <option value="Year 1">Year 1</option>
							            <option value="Year 2">Year 2</option>
							            <option value="Year 3">Year 3</option>
							            <option value="Year 4">Year 4</option>
							            <option value="Graduated">Graduated</option>
							            <option value="Other">Other</option>
							        </select>
							    </td>
							    <td>
							        <button type="submit" class="btn btn-success" id="btnSave" form="save_form">Save</button>
							    </td>
							</tr>
                        </thead>
                       
                        <tbody id="educationTbody" ng-controller="tableCtrl">
                            <tr ng-repeat="edu in educations | orderBy : '-id'">
                            	<td>{{ $index + 1 }}</td>
                                <td>{{edu.school}}</td>
                                <td>{{edu.degree}}</td>
                                <td>{{edu.major}}</td>
                                <td>{{edu.start_year}}</td>
                                <td>{{edu.end_year}}</td>
                                <td>{{edu.current_year}}</td>
                                <td>
                                    <a href="form_edu.jsp?id={{edu.id}}"><i class="fa fa-gear fa-lg"></i></a>&nbsp;
									<a href="#" ng-click="deleteEdu(edu.id)"><i class="fa fa-trash-o fa-lg"></i></a>
                                </td>
                            </tr>
                        </tbody>
                        <tfoot  >
					      
					    </tfoot>
                    </table>
                </div>
            </div>
        </div>
        <!--body wrapper end-->
        <!--footer section start-->
        <jsp:include page="includes/footer.jsp"></jsp:include>
        <!--footer section end-->

    </div>
    <!-- main content end-->
</section>

<!-- common scripts -->
<jsp:include page="includes/common_scripts.jsp"></jsp:include>
<script type="text/javascript" src="js/bootstrap-fileupload.min.js"></script>
<script>
var app = angular.module('myApp', []);
app.controller('tableCtrl', function($scope, $http) {
	$scope.loadEdu = function(){
		$http({
		    method: 'POST',
		    url: 'listEdu.hrd',
		    headers: {'Content-Type': 'application/x-www-form-urlencoded'}
		}).success(function(response) {
	    	$scope.educations = response;
	    	console.log(response);
	    });
	}
	$scope.loadEdu();
	$scope.deleteEdu = function (eduId){
		new $.flavr({
			title		: 'Are your sure ?',
		    dialog      : 'confirm',
		    onConfirm   : function( $container ){
		    	$.ajax({
			        method: 'POST',
			        url: 'deleteEdu.hrd',
			        data: {
			            id: eduId
			        },
			        success:function(result){
			        	$scope.loadEdu();
			        }
			    });
		        return true;
		    }
		});
	};
});
</script>

<script>
$("#fileID").change(function(){
	$("#browseFileButton").html("File Selected")
}).click(function(){
    $(this).val("")
});


//Return today's date and time
var currentTime = new Date()

// returns the year (four digits)
var year = currentTime.getFullYear()
for(i=1950; i<=year;i++){
	$('#end_year').append($('<option>', {
	    value: i,
	    text: i
	}));
	$('#start_year').append($('<option>', {
	    value: i,
	    text: i
	}));
	
	$('#end_year option[value='+year+']').attr('selected','selected');
	$('#start_year option[value='+year+']').attr('selected','selected');
}

//Program a custom submit function for the form
$("form#save_form").submit(function(event){

  //disable the default form submission
  event.preventDefault();
 
  //grab all form data  
  var formData = new FormData($(this)[0]);
  
  $.ajax({
    url: 'addEdu.hrd',
    type: 'POST',
    data: formData,
    async: false,
    cache: false,
    contentType: false,
    processData: false,
    success: function (returndata) {
        if(returndata == ""){
            alert("Invalid input");
            return;
        }
        
      angular.element('#educationTbody').scope().loadEdu();
      $('form#save_form')[0].reset();     
    }
  });
 
  return false;
});

</script>

</body>
</html>
