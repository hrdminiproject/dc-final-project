<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">
  <meta name="description" content="">
  <meta name="author" content="ThemeBucket">

  <title>Profile</title>
  
  <!--common styles -->
  <jsp:include page="includes/common_styles.jsp"></jsp:include>

  <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!--[if lt IE 9]>
  <script src="js/html5shiv.js"></script>
  <script src="js/respond.min.js"></script>
  <![endif]-->
</head>

<body class="sticky-header" ng-app="myApp" ng-controller="allInfo">

<section>
    <!-- left side start -->
    <jsp:include page="includes/left_side.jsp"></jsp:include>
    <!-- left side end -->
    
    <!-- main content start -->
    <div class="main-content" >

        <!-- header section start-->
        <jsp:include page="includes/top_side.jsp"></jsp:include>
        <!-- header section end-->

        <!--body wrapper start-->
        <div class="wrapper">
            <div class="row">
                <div class="col-md-4">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="panel">
                                <div class="panel-body">
                                    <div class="profile-pic text-center">
                                        <img alt="{{user.avatar}}" src="../images/uploads/developer/{{user.avatar}}">
                                    </div>
                                </div>
                                <% if (session.getAttribute("role").toString().equals("1")) { %>
                                <center>
                             		<button class="btn btn-success" type="button" ng-click="pending()">Submit to Admin</button>
	                                <button class="btn btn-danger" type="button" ng-click="reject()">Cancel Pending</button>
                             	</center>
                             	<% } %>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="panel">
                                <div class="panel-body">
                                    <ul class="p-info">
                                        <li>
                                            <div class="title">Full Name</div>
                                            <div class="desk">{{user.fname + " " + user.lname}}</div>
                                        </li>
                                        <li>
                                            <div class="title">Gender</div>
                                            <div class="desk">{{user.gender}}</div>
                                        </li>
                                        <li>
                                            <div class="title">Nationality</div>
                                            <div class="desk">{{user.nationality}}</div>
                                        </li>
                                        <li>
                                            <div class="title">Date of Birth</div>
                                            <div class="desk">{{user.dob}}</div>
                                        </li>
                                        <li>
                                            <div class="title">Place of Birth</div>
                                            <div class="desk">{{user.pob}}</div>
                                        </li>
                                        <li>
                                            <div class="title">Marital Status</div>
                                            <div class="desk">{{user.marital}}</div>
                                        </li>
                                        <li>
                                            <div class="title">Print</div>
                                            <div class="desk"><a href="../report.pdf?id={{user.id}}" target="_blank" rel="external">Print CV</a></div>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                   
                </div>
                <div class="col-md-8">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="panel">
                                <div class="panel-body">
                                    <div class="profile-desk">
                                        <div class="wrapper">
                                            <section class="panel">
                                                <header class="panel-heading custom-tab turquoise-tab">
                                                    <ul class="nav nav-tabs">                      
                                                        <li class="active">
                                                            <a data-toggle="tab" href="#education-info">
                                                                <i class="fa fa-user"> Education</i>
                                                            </a>
                                                        </li>
                                                        
                                                        <li>
                                                            <a data-toggle="tab" href="#exp-info">
                                                                <i class="fa fa-envelope-o"> Work Experience</i>
                                                            </a>
                                                        </li>  
                                                        
                                                        <li>
                                                            <a data-toggle="tab" href="#ach-info">
                                                                <i class="fa fa-envelope-o"> Achievement</i>
                                                            </a>
                                                        </li> 
                                                        
                                                        <li>
                                                            <a data-toggle="tab" href="#skill-info">
                                                                <i class="fa fa-envelope-o"> Skill</i>
                                                            </a>
                                                        </li> 
                                                        
                                                        <li>
                                                            <a data-toggle="tab" href="#language-info">
                                                                <i class="fa fa-envelope-o"> Language</i>
                                                            </a>
                                                        </li> 
                                                                                
                                                        <li>
                                                            <a data-toggle="tab" href="#contact-info">
                                                                <i class="fa fa-envelope-o"> Contact</i>
                                                            </a>
                                                        </li>
                                                       
                                                    </ul>
                                                </header>
                                            </section>
                                        </div>
                                        <div class="panel-body" style="height:315px;">
                                            <div class="tab-content">
                                        <!--Education Information -->
                                                <div id="education-info" class="tab-pane active">
                                                    <div class="panel-body">
                                                        <table class="table  table-hover general-table">
                                                            <thead>
                                                                <tr>
                                                                    <th>School</th>
                                                                    <th>Major</th>
                                                                    <th>Degree</th>
                                                                    <th>Start Year</th>
                                                                    <th>End Year</th>
                                                                    <th>Current Year</th>
                                                                </tr>
                                                            </thead>
                                                            <tbody>
                                                                <tr ng-repeat="edu in educations">
                                                                    <td> {{edu.school}}</td>
                                                                    <td>{{edu.major}}</td>
                                                                    <td>{{edu.degree}}</td>
                                                                    <td>{{edu.start_year}}</td>
                                                                    <td>{{edu.end_year}}</td>
                                                                    <td>{{edu.current_year}}</td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>

                                         <!--Experience Information -->
                                                <div id="exp-info" class="tab-pane ">
                                                    <div class="panel-body">
                                                        <table class="table  table-hover general-table">
                                                            <thead>
                                                            <tr>
                                                                <th>Company</th>
                                                                <th>Work Duration</th>
                                                                <th>Type</th>
                                                                <th>Skill</th>
                                                                <th>Position</th>
                                                                <th>Description</th>
                                                            </tr>
                                                            </thead>
                                                            <tbody>
                                                            <tr ng-repeat="exp in experiences">
                                                                <td>{{exp.place}}</td>
                                                                <td>{{exp.duration}} Years</td>
                                                                <td>{{exp.type}}</td>
                                                                <td>{{exp.skill}}</td>
                                                                <td>{{exp.position}}</td>
                                                                <td>{{exp.description}}</td>
                                                            </tr>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                                
                                            <!--Achievement Information -->
                                                <div id="ach-info" class="tab-pane ">
                                                    <div class="panel-body">
                                                        <table class="table  table-hover general-table">
                                                            <thead>
                                                                <tr>
                                                                    <th>Title</th>
                                                                    <th>Year</th>
                                                                    <th>Location</th>
                                                                    <th>Description</th>
                                                                </tr>
                                                            </thead>
                                                            <tbody>
                                                                <tr ng-repeat="ach in achievements">
                                                                    <td>{{ach.title}}</td>
                                                                    <td>{{ach.year}}</td>
                                                                    <td>{{ach.location}}</td>
                                                                    <td>{{ach.description}}</td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                                
                                          <!--Skills Information -->
                                                <div id="skill-info" class="tab-pane ">
                                                    <div class="panel-body">
                                                        <table class="table  table-hover general-table">
                                                            <thead>
                                                            <tr>
                                                                <th>Name</th>
                                                                <th>Rate</th>
                                                            </tr>
                                                            </thead>
                                                            <tbody>
                                                            <tr ng-repeat="skill in skills">
                                                                <td>{{skill.name}}</td>
                                                                <td>{{skill.rate}}</td>
                                                            </tr>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>	
                                                
                                           <!--Languages Information -->
                                                <div id="language-info" class="tab-pane ">
                                                    <div class="panel-body">
                                                        <table class="table  table-hover general-table">
                                                            <thead>
                                                            <tr>
                                                                <th>Language</th>
                                                                <th>Understanding Level</th>
                                                                <th>Speaking Level</th>
                                                                <th>Writing Level</th>
                                                            </tr>
                                                            </thead>
                                                            <tbody>
                                                            <tr ng-repeat="lang in languages">
                                                                <td>{{lang.language}}</td>
                                                                <td>{{lang.understandingLevel}}</td>
                                                                <td>{{lang.speakingLevel}}</td>
                                                                <td>{{lang.writingLevel}}</td>
                                                            </tr>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                                
                                          <!--Contatact Information -->  
                                                <div id="contact-info" class="tab-pane ">
                                                    <form class="form-horizontal adminex-form" method="post">
                                                        <div class="form-group">
                                                            <label class="col-sm-2 control-label">Phone *</label>
                                                            <div class="col-sm-10">
                                                                <input type="text" name="phone" class="form-control" placeholder="phone..." ng-model="user.phone">
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label class="col-sm-2 col-sm-2 control-label">Email *</label>
                                                            <div class="col-sm-10">
                                                                <input type="text" name="email" class="form-control" placeholder="Email..." ng-model="user.email">
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label class="col-sm-2 control-label">Address</label>
                                                            <div class="col-sm-10">
                                                                <textarea rows="6" name="address" class="form-control" ng-model="user.address"></textarea>
                                                            </div>
                                                        </div>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            
            <!-- Reference -->
            <div class="row">
                <div class="col-sm-12">
                    <section class="panel">
                        <header class="panel-heading">
                            Reference Documents
                            <span class="tools pull-right">
                                <a href="javascript:;" class="fa fa-chevron-down"></a>
                             </span>
                        </header>
                        <div class="panel-body">

                            <ul id="filters" class="media-filter">
                                <li><a href="#" data-filter="*"> All</a></li>
                                <li><a href="#" data-filter=".education">Education</a></li>
                                <li><a href="#" data-filter=".experience">Work Experience</a></li>
                                <li><a href="#" data-filter=".achievement">Achievement</a></li>
                            </ul>
                            <div id="gallery" class="media-gal">
                                <!-- Insert from JQuery -->
                            </div>

                            <!-- Modal -->
                            
							  <div id="myModal" class="modal fade pop-up-2" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel-2" aria-hidden="true">
							    <div class="modal-dialog modal-lg">
							      <div class="modal-content">
							
							        <div class="modal-header">
							          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">�</button>
							          <h4 class="modal-title" id="myLargeModalLabel-2">Lion</h4>
							        </div>
							        <div class="modal-body">
							        <img src="" class="img-responsive img-rounded center-block" alt="" id="popup">
							        </div>
							      </div>
							    </div>
							  </div>
                            <!-- modal -->

                        </div>
                    </section>
                </div>
            </div>
            
            <% if (session.getAttribute("role").toString().equals("0")) { %>
            <!-- buttons to reject and approve -->
      		<div class="panel-body">
               	<button class="btn btn-danger btn-lg" type="button" ng-click="reject()">Reject</button>
               	<button class="btn btn-success btn-lg" type="button" ng-click="approve()">Approve</button>
            </div>
            <% } %>
        </div>
        <!--body wrapper end-->
        
        <!--footer section start-->
        <jsp:include page="includes/footer.jsp"></jsp:include>
        <!--footer section end-->

    </div>
    <!-- main content end-->
</section>

<!-- common scripts -->
<jsp:include page="includes/common_scripts.jsp"></jsp:include>
<!-- Gallery -->
<script src="js/jquery.isotope.js"></script>
<script type="text/javascript">
    $(function() {
        var $container = $('#gallery');
        $container.isotope({
            itemSelector: '.item',
            animationOptions: {
                duration: 750,
                easing: 'linear',
                queue: false
            }
        });

        // filter items when filter link is clicked
        $('#filters a').click(function() {
            var selector = $(this).attr('data-filter');
            $container.isotope({filter: selector});
            return false;
        });
    });
</script>

<script>
	var x = getParameterByName("dev_id");
	var app = angular.module('myApp', []);
	app.controller('allInfo', function($scope, $http) {
		$http({
		    method: 'post',
		    url: 'listDevInfo.hrd',
		    data: $.param({dev_id: x}),
		    headers: {'Content-Type': 'application/x-www-form-urlencoded'}
		}).success(function(response) {
	    	$scope.user = response.info;
	    	$scope.educations = response.educations;
	    	$scope.experiences = response.experiences;
	    	$scope.achievements = response.achievements;
	    	$scope.skills = response.skills;
	    	$scope.languages = response.languages;
	    	
	    	for (var x in response.educations){
	    		//var $newItems = $('<div class=" education item " ><a href="#myModal" data-toggle="modal"><img src="images/gallery/image2.jpg" alt="" /></a><p>img09.jpg </p></div>');
	    		addItem (response.educations[x].image_url, 'education');
	    	}
	    	
	    	for (var x in response.experiences){
	    		addItem (response.experiences[x].image_url, 'experience');
	    	}
	    	
	    	for (var x in response.achievements){
	    		addItem (response.achievements[x].image_url, 'achievement');
	    	}
	    	
	    	function addItem (url, type){
	    		if (url == null || url == "") return;
	    		var $newItems = $('<div class="' + type +' item" ><a href="#myModal" data-toggle="modal" onclick="test(this)"><img src="../images/uploads/' + type + '/' + url + '" alt="" /></a><p>' + url + '</p></div>');
	        	$('#gallery').isotope( 'insert', $newItems );
	    	}
	    });
		
		$scope.approve = function(){
			$http({
			    method: 'post',
			    url: 'updatePendingDev.hrd',
			    data: $.param({dev_id: x, status: 'approved'}),
			    headers: {'Content-Type': 'application/x-www-form-urlencoded'}
			}).success(function(response) {
				if(response=="success")
					new $.flavr('Approved Successful ! !');
					location.reload();
			});
		};
		
		$scope.reject = function(){
			$http({
			    method: 'post',
			    url: 'updatePendingDev.hrd',
			    data: $.param({dev_id: x, status: 'reject'}),
			    headers: {'Content-Type': 'application/x-www-form-urlencoded'}
			}).success(function(response) {
				if(response=="success")
					if (x==""){
						new $.flavr('Pending Cancel Successful !');
					}else{
						new $.flavr('Reject Successful !');
					}	
			});
		};
		
		$scope.pending = function(){
			$http({
			    method: 'post',
			    url: 'updatePendingDev.hrd',
			    data: $.param({dev_id: x, status: 'pending'}),
			    headers: {'Content-Type': 'application/x-www-form-urlencoded'}
			}).success(function(response) {
				if(response=="success")
					new $.flavr('Your request will be review by Admin !');
			});
		};
	});
	
	function test(element){
		var src = $(element).find('img').attr('src');
		$('#popup').attr('src', src);
		$('#myLargeModalLabel-2').text(src.substring(src.lastIndexOf("/")+1));
	}
	

</script>


</body>
</html>
