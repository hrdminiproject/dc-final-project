<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">
  <meta name="description" content="">
  <meta name="author" content="ThemeBucket">

  <title>Add DevLanguage</title>
  
  <!--common styles -->
  <jsp:include page="includes/common_styles.jsp"></jsp:include>
  <link rel="stylesheet" href="css/sub-style.css">
  <link rel="stylesheet" href="css/register_detail.css">
  <link rel="stylesheet" href="css/rate-bar.css">
  
  <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!--[if lt IE 9]>
  <script src="js/html5shiv.js"></script>
  <script src="js/respond.min.js"></script>
  <![endif]-->
</head>

<body class="sticky-header">

<section>
    <!-- left side start -->
    <jsp:include page="includes/left_side.jsp"></jsp:include>
    <!-- left side end -->
    
    <!-- main content start -->
    <div class="main-content" >

        <!-- header section start-->
        <jsp:include page="includes/top_side.jsp"></jsp:include>
        <!-- header section end-->

        <!--body wrapper start-->
        <div class="wrapper">
        	<!-- Pannel for Categories -->
            <div class="row">                
                <div class="col-md-12"> 
                	 <form action="addDevLang.hrd" method="post">
	                   <!-- pannel subjects -->
			            <div class="panel panel-success">
			                 <div class="panel-heading">
			                     <h3 class="panel-title">Developer Language Form</h3>
			                 </div>
			                 
			                 <!-- Subject contents -->
			                 <div class="panel-body">			                 
			                     <div class="form-group">
			                         <!-- developer subject rating  -->
					                    <div class="panel-body">
									        <!-- Language type combobox -->
							                <div class="form-group">
								                <label class="col-sm-2 control-label">Language:</label>
								                    <div class="col-sm-10 ">
									                        <select class="form-control m-bot15" id="laid" name="laid">
								                             
								                            </select>
							                        </div>
						                    </div>	
						                    <!-- End Language type combobox -->	
						                    
						                    <!-- Understanding and Speaking Level -->	                     
						                    <div class="form-group">
						                    		<!-- Understanding Level -->
							               		    <label class="col-sm-2 control-label">Understanding Level: </label>
							                    		<div class="col-sm-4">
															
			                                    				<select class="form-control m-bot15" id="understandingLevel" name="understandingLevel">
						                                            <!-- <option value="">--Select--</option> -->
						                                            <option value="1">1</option>
						                                            <option value="2">2</option>
						                                            <option value="3">3</option>
						                                            <option value="4">4</option>
						                                            <option value="5">5</option>
	                                            				</select>
			                                				
							                    		</div>
							                    	<!-- End Understanding Level -->
							                  		
							                  		<!-- Speaking Level -->
							               			<label class="col-sm-2 control-label">Speaking Level: </label>
							                    		<div class="col-sm-4">
															
			                                    				<select class="form-control m-bot15" id="speakingLevel" name="speakingLevel">
						                                            <!-- <option value="">--Select--</option> -->
						                                            <option value="1">1</option>
						                                            <option value="2">2</option>
						                                            <option value="3">3</option>
						                                            <option value="4">4</option>
						                                            <option value="5">5</option>
	                                            				</select>
							                    		</div>
							                    	<!-- End Speaking Level -->
							                 </div>
						                     <!-- End Understanding and Speaking Level -->		
						                    						           
				                     	     <div class="form-group">
								               <!-- Writing Level -->
								               <label class="col-sm-2 control-label">Writing Level: </label>
							                    		<div class="col-sm-10">
															
			                                    				<select class="form-control m-bot15" id="writingLevel" name="writingLevel">
						                                            <!-- <option value="">--Select--</option> -->
						                                            <option value="1">1</option>
						                                            <option value="2">2</option>
						                                            <option value="3">3</option>
						                                            <option value="4">4</option>
						                                            <option value="5">5</option>
	                                            				</select>
							                    		</div>	
							                    <!-- End Writing Level -->							            
				                     	      </div>                     
			                     
						                     <!-- developer language description -->
						                     <div class="form-group">
							                        <label class="col-sm-2 control-label">Description:</label>
							                        <div class="col-sm-10">
							                            <textarea rows="6" name="description" id="description" class="form-control"></textarea>
							                        </div>
						                     </div>
			                 			</div>
			                   </div>
						        <!-- button Language submit -->
			                	<div class="panel-body">
				                	<div class="pull-right">
					                	<a href="myadddevlanguage.jsp"><button class="btn btn-info" type="button">Cancel</button></a>
					                    <button class="btn btn-success" id="btn-add" type="submit">Save</button>
									</div>			                
			             		</div>
			            	</div> 
			            </div>
			         </form>
                </div>
            </div>
        </div>  
        <!--body wrapper end--> 

        <!--footer section start-->
        <jsp:include page="includes/footer.jsp"></jsp:include>
        <!--footer section end-->
        
    </div>
    <!-- main content end-->
</section>


<!-- common scripts -->
<jsp:include page="includes/common_scripts.jsp"></jsp:include>
<script src="js/devlanguage-script.js"></script>

<script>

$(function(){	
	var x = getParameterByName("lanid");
		if (x != "") {
			/* change button Save to Update */
			$("#btn-add").text("Update");
			
			/* change url of form from subject_add to subject_edit */
			$('form').attr("action","updateDevLang.hrd?lanid="+x);
			$.ajax({
				url : "getDevLang.hrd",
				data : {
					'id' : x
				},
				success : function(data) {
					/* load data to combobox and textbox */
					$('#laid').val(data[0].lid);
					$('#understandingLevel').val(data[0].understanding_level);
					$('#speakingLevel').val(data[0].speaking_level);
					$('#writingLevel').val(data[0].writing_level);
					$('#description').val(data[0].description);
				}
			});
		}
	
});
</script>

</body>
</html>
