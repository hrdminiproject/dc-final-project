<%@ page import="model.dto.Categories" %> 
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">
  <meta name="description" content="">
  <meta name="author" content="ThemeBucket">

  <title>Add Language</title>
  
  <!--common styles -->
  <jsp:include page="includes/common_styles.jsp"></jsp:include>
  <link rel="stylesheet" href="css/sub-style.css">
  
  <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!--[if lt IE 9]>
  <script src="js/html5shiv.js"></script>
  <script src="js/respond.min.js"></script>
  <![endif]-->
</head>

<body class="sticky-header">

<section>
    <!-- left side start -->
    <jsp:include page="includes/left_side.jsp"></jsp:include>
    <!-- left side end -->
    
    <!-- main content start -->
    <div class="main-content" >

        <!-- header section start-->
        <jsp:include page="includes/top_side.jsp"></jsp:include>
        <!-- header section end-->

        <!--body wrapper start-->
        <div class="wrapper">
        	<!-- Pannel for Categories -->
            <div class="row" id="add_cat">
            
                <div class="col-md-12"> 
                   <!-- panel skill categories -->
                   <form action="addLang.hrd" method="post">
		            <div class="panel panel-success">
		                 <div class="panel-heading">
		                     <h3 class="panel-title">Form Language</h3>
		                 </div>
		                 
		                 <!-- Categories contents -->
		                 <div class="panel-body">
		                     <div class="form-group">
		                         <!-- categories name  -->
		                         <label class="col-sm-1 control-label">Language: </label>
		                         <div class="col-sm-11">
		                             <input class="form-control" id="language" name="language" type="text" placeholder="Enter language name">
		                         </div>
		                         
		                     </div>
		                 </div>
		                 
		                <!-- button categories submit -->
		                <div class="panel-body">
		                	<div class="pull-right">
			                	<a href="myaddlanguage.jsp"><button class="btn btn-info" type="button">Cancel</button></a>
			                    <button class="btn btn-success" id="btn-add" type="submit">Save</button>
							</div>			                
		             	</div>
		             </div>
		            </form>
                </div>
            </div>
        </div>  
        <!--body wrapper end--> 

        <!--footer section start-->
        <jsp:include page="includes/footer.jsp"></jsp:include>
        <!--footer section end-->
    </div>
    <!-- main content end-->
</section>


<!-- common scripts -->
<jsp:include page="includes/common_scripts.jsp"></jsp:include>
<script>
$(function(){
	function getParameterByName(name) {
	    name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
	    var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
	        results = regex.exec(location.search);
	    return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
	}
	
	var x = getParameterByName("id");

		if (x != "") {
			/* change button Save to Update */
			$("#btn-add").text("Update");
			
			/* change url of form from categories_add to categories_edit */
			$('form').attr("action","updateLang.hrd?id="+x);
			$.ajax({
				url : "getLang.hrd",
				data : {
					'id' : x
				},
				success : function(data) {
					/* load data to textbox */
					$('#language').val(data.language);
			}
		});
	}
	
	
});
</script>

</body>
</html>
