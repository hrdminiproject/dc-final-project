<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">
  <meta name="description" content="">
  <meta name="author" content="ThemeBucket">

  <title>DevSubjects</title>
  
  <!--common styles -->
  <jsp:include page="includes/common_styles.jsp"></jsp:include>
  <link rel="stylesheet" href="css/sub-style.css">
  
  <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!--[if lt IE 9]>
  <script src="js/html5shiv.js"></script>
  <script src="js/respond.min.js"></script>
  <![endif]-->
  
</head>

<body class="sticky-header">

<section>
    <!-- left side start -->
    <jsp:include page="includes/left_side.jsp"></jsp:include>
    <!-- left side end -->
    
    <!-- main content start -->
    <div class="main-content" >

        <!-- header section start-->
        <jsp:include page="includes/top_side.jsp"></jsp:include>
        <!-- header section end-->

        <!--body wrapper start-->
        <div class="wrapper">
        	<!-- Pannel for Dev_Subjects -->
            <div class="row">
                <div class="col-md-12"> 
                   <!-- pannel subject -->
		            <div class="panel panel-success">
		                 <div class="panel-heading">
		                     <h3 class="panel-title">Developer Subject Information</h3>
		                 </div>
		                
		             </div>
		             
		             <!-- Dev_Subject table information -->
	             	<section class="panel">
	                                       
	                    <!-- table skill dev_subject showing -->
	                    <div class="panel-body" style="display: block;">
	                   <table class="table table-hover general-table">
	                    <form method="POST" id="save_form"></form>
                        <thead>
                            <tr>
                                <th> #</th>
                                <th>Subject</th>
                                <th>Category</th>
                                <th>Rate</th>
                                <th>Action</th>
                            </tr>
                             <tr>
							    <td>
							        <input type="hidden" name="description" value="No Description" form="save_form" />
								</td>
							     <td>
							        <select class="form-control m-bot15" id="sid" name="sid" form="save_form">
							           
							        </select>
							    </td>
							   		
							    <td>
							     	 <input class="form-control"  type="text" form="save_form" disabled />
							    </td>
							    <td>
							        <select class="form-control" id="progress" name="progress" form="save_form">
							            <option value="0">0</option>
							            <option value="1">1</option>
							            <option value="2">2</option>
							            <option value="3">3</option>
							            <option value="4">4</option>
							            <option value="5">5</option>
							            <option value="6">6</option>
							            <option value="7">7</option>
							            <option value="8">8</option>
							            <option value="9">9</option>
							            <option value="10">10</option>
							        </select>
							    </td>
							   
							    <td>
							        <button type="submit" class="btn btn-success" id="btnSave" form="save_form">Save</button>
							    </td>
							</tr>
                        </thead>
			
                        <tbody id="subjectTbody" ng-app="myApp" ng-controller="tableCtrl">
                            <tr ng-repeat="item in listDevSubjects">
                            	<td>{{ $index+1 }}</td>
                                <td>{{item.subjname}}</td>
                                <td>{{item.catname}}</td>
                                <td>{{item.rate}}</td>
                                <td>
                                    <a href="adddevsubjects.jsp?subid={{item.sid}}&subject={{item.subjname}}"><i class="fa fa-gear fa-lg"></i></a>&nbsp;
									<a href="#" ng-click="mydelete(item.sid)"><i class="fa fa-trash-o fa-lg"></i></a>
                                </td>
                            </tr>
                        </tbody>
                    </table>
	                    </div>
	                </section>
                </div>
            </div>
        </div>  
        <!--body wrapper end--> 

        <!--footer section start-->
        <jsp:include page="includes/footer.jsp"></jsp:include>
        <!--footer section end-->
    </div>
    <!-- main content end-->
</section>

<!-- common scripts -->
<jsp:include page="includes/common_scripts.jsp"></jsp:include>
<script>
var app = angular.module('myApp', []);
app.controller('tableCtrl', function($scope, $http) {
	$scope.load = function(){
		
		$http({
		    method: 'post',
		    url: 'listDevSub.hrd',
		    headers: {'Content-Type': 'application/x-www-form-urlencoded'}
		}).success(function(response) {
	    	$scope.listDevSubjects = response;
	    });
	}
	$scope.load();
	
	/*delete record of delete button*/
	$scope.mydelete =function(sid){
		new $.flavr({
			title		: 'Are your sure ?',
		    dialog      : 'confirm',
		    onConfirm   : function( $container ){
				$.ajax({
			        method: 'POST',
			        url: 'deleteDevSub.hrd',
			        data: {
			        	sid : sid
			        },
			        success:function(result){
			        	$scope.load();
			        }
			    });
		        return true;
		    }
		});
	}
});
</script>
<script>

/*load skill from database categories*/
function loadCboSkillDB() {
	$.post("listSub.hrd", function(data){
		$('#sid').empty();
		var subject_select = '';
		for (var i = 0; i < data.length; i++) {
			subject_select += '<option value="'+ data[i].id +'">'+ data[i].subjects +'</option>';
			//console.log(data[i]);
		}
		$('#sid').append(subject_select);
	});
}
loadCboSkillDB();

//Program a custom submit function for the form
$("form#save_form").submit(function(event){

  //disable the default form submission
  event.preventDefault();
  if($( "#sid option").length == 0){
	  alert("You cannot insert more subject");
	  return;
  }
 
  //grab all form data  
  $.ajax({  
        type: "POST",  
        url: "addDevSub.hrd",  
        data: {
        	'sid': $( "#sid" ).val(),
        	'progress': $( "#progress" ).val(),
        	'description': $( "#description" ).val()
        },
        success: function(dataString) { 
        	if(dataString == "fail"){
                alert("This subject already inserted!");
                return;
            }
            
          angular.element('#subjectTbody').scope().load();
          loadCboSkillDB();
          $('form#save_form')[0].reset(); 
        }
  }); 
 
  return false;
});

</script>

</body>
</html>
