
var app = angular.module('myApp', []);
app.controller('tableCtrl', function($scope, $http) {
	$scope.load = function(){
		$http({
		    method: 'post',
		    url: 'listLang.hrd',
		    headers: {'Content-Type': 'application/x-www-form-urlencoded'}
		}).success(function(response) {
	    	$scope.listLanguages = response;
	    });
	}
	$scope.load();
	
	/*delete record of delete button*/
	$scope.mydelete =function(id){
		
		$.post("deleteLang.hrd",{
			id : id
		},function(data){
			$scope.load();
		});
	}
});


