var Script = function () {

    $().ready(function() {
        // validate education adding form on keyup and submit
        $("#frmAchieve").validate({
            rules: {
            	title: {
            		required: true,
            	},
            	year: {
            		required: true,
                    minlength: 4,
                    maxlength: 4,
                    number:true
            	},
                location: {
                    required: true,
                }
            }
        });
    });
}();