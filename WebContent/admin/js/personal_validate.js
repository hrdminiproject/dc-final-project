var Script = function () {
	
    $().ready(function() {
        // validate education adding form on keyup and submit
        $("#frmPersonal").validate({
            rules: {
            	fName: {
            		required: true
            	},
            	lName: {
            		required: true
            	},
                nationality: {
                    required: true
                },
                dob: {
                    required: true
                },
                pob: {
                    required: true
                },
                phone: {
                    required: true,
                    minlength: 9
                },
                email: {
                    required: true,
                    email: true
                },
                address: {
                    required: true,
                },
                salary: {
                    required: true,
                    number: true,
                    minlength: 3
                },
                wanted_position: {
                	required: true
                }
            }
        });
    });
}();