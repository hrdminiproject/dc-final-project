function getParameterByName(name) {
    name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
    var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
    results = regex.exec(location.search);
    return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
}
	
$(function(){
	/* Pending Developer List */
	$.ajax({
		method: 'get',
	    url: 'listPendingDev.hrd',
	    success: function(data){
	    	var li = "";
	    	for (var x in data){
	    		li += '<li class="new">' +
				            '<a href="profile.jsp?dev_id=' + data[x].id + '">' +
				                '<span class="thumb"><img src="../images/uploads/developer/'+ data[x].avatar +'" alt="" /></span>' +
				                '<span class="desc">' +
				                    '<span class="name">' + data[x].fullname + '<span class="badge badge-success">new</span></span>' +
				                    '<span class="msg">' + data[x].specialty_skill + '</span>' +
				                '</span>' +
				            '</a>' +
				      '</li>';
	    	}
	    	
	    	var str = '<a href="#" class="btn btn-default dropdown-toggle info-number" data-toggle="dropdown">' +
                	     '<i class="fa fa-envelope-o"></i>' +
                         '<span class="badge">' + data.length + '</span>' +
                      '</a>' +
                      '<div class="dropdown-menu dropdown-menu-head pull-right">' +
                      	  '<h5 class="title">You have ' + data.length + ' Requests </h5>' +
                      	  '<ul class="dropdown-list normal-list">' + li +
                      	      '<li class="new"><a href="">Check All Requests</a></li>' +
                          '</ul>' +
                      '</div>';
	    	$('ul.notification-menu > li.notification').html(str);
	    }
	});
	
	$('#btnCancel').click(function(){
		window.location.assign("achievement.jsp");
	});
});