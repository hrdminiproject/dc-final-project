var Script = function () {

//    $.validator.setDefaults({
//        submitHandler: function() { alert("submitted!"); }
//    });

    $().ready(function() {
        // validate education adding form on keyup and submit
        $("#frmEducation").validate({
            rules: {
            	school: {
            		required: true,
                    minlength: 2
            	},
                username: {
                    required: true,
                    minlength: 2
                },
                start_year: {
                    required: true,
                    minlength: 4,
                    maxlength:4
                },
                end_year: {
                	required: true,
                	minlength: 4,
                    maxlength:4
                },
                other: {
                	required: true
                }
            }
        });
    });
}();