var Script = function () {
	
    $().ready(function() {
        // validate education adding form on keyup and submit
        $("#frmExp").validate({
            rules: {
            	place: {
            		required: true
            	},
            	position: {
            		required: true
            	},
                duration: {
                    required: true,
                    number: true,
                    maxlength:2
                }
            }
        });
    });
}();