$(function(){
	var x = getParameterByName("catid");
		if (x != "") {
			/* change button Save to Update */
			$("#btn-add").text("Update");
			
			/* change url of form from categories_add to categories_edit */
			$('form').attr("action","updateCat.hrd?id="+x);
			$.ajax({
				url : "getCat.hrd",
				data : {
					'id' : x
				},
				success : function(data) {
					/* load data to textbox */
					$('#id').val(data.id);
					$('#name').val(data.name);
					$('#description').val(data.description);
			}
		});
	}
});