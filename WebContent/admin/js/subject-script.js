loadData();
	function loadData(){
		$.ajax({
			method: 'post',
			url: 'listSub.hrd',
			success: function(response){
				var tr = "", index = 1;
				for (var x in response){
					tr += '<tr class="gradeX">' +
				            '<td class="text-center">' + index + '</td>' +
				            '<td class="center">' + response[x].subjects + '</td>' +
				            '<td class="center">' + response[x].categories + '</td>' +
				            '<td class="center">' +
				            	'<a href="addsubjects.jsp?id=' + response[x].id + '">' +
				            		'<i class="fa fa-gear fa-lg"></i>' +
				            	'</a>&nbsp;' + 
				            	
				            	'<a href="#" onclick="deleteSubject(' + response[x].id + ')">' +
				            		'<i class="fa fa-trash-o fa-lg"></i>' +
				            	'</a>' + 
				            '</td>' +
				          '</tr>';
				    index++;
				}
				$('#allSub').html(tr);
				$('#dynamic-table').dataTable();
			}
		});
	}
	
	function deleteSubject(id){
		new $.flavr({
			title		: 'Are your sure ?',
		    dialog      : 'confirm',
		    onConfirm   : function( $container ){
		    	$.post("deleteSub.hrd",{
					id : id
				},function(data){
					loadData();
				});
		        return true;
		    }
		});
		
	}
	
	loadCboSkillDB();
	//load skill from database categories
	function loadCboSkillDB() {
		$.post("listCat.hrd", function(data){	
			$('#catid').empty();
			var skill_select = '';
			for (var i = 0; i < data.length; i++) {
				skill_select += '<option value="'+ data[i].id +'">'+ data[i].name +'</option>';
				//console.log(data[i]);
			}
			$('#catid').append(skill_select);
		});
	}