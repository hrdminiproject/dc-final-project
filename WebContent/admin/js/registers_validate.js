var Script = function () {
    $().ready(function() {
        // validate signup form on keyup and submit
        $("#frmRegister").validate({
            rules: {
                firstname:{
					 required: true,
					 minlength: 2
				},
                lastname: {
					required: true,
					minlength: 2
				},
				email: {
                    required: true,
                    email: true
                },
                username: {
                    required: true,
                    minlength: 4
                },
                password: {
                    required: true,
                    minlength: 6
                },
                confirmpassword: {
                    required: true,
                    minlength: 6,
                    equalTo: "#password"
                }
			}
        });
    });
}();