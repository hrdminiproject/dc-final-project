<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="sql" uri="http://java.sun.com/jsp/jstl/sql" %>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta name="viewport"
	content="width=device-width, initial-scale=1.0, maximum-scale=1.0">
<meta name="description" content="">
<meta name="author" content="ThemeBucket">

<title>Dashboard</title>

<!--common styles -->
<jsp:include page="includes/common_styles.jsp"></jsp:include>

<!--icheck-->
<link href="js/iCheck/skins/minimal/minimal.css" rel="stylesheet">
<link href="js/iCheck/skins/square/square.css" rel="stylesheet">
<link href="js/iCheck/skins/square/red.css" rel="stylesheet">
<link href="js/iCheck/skins/square/blue.css" rel="stylesheet">

<!--Morris Chart CSS -->
<link rel="stylesheet" href="js/morris-chart/morris.css">

<!--common-->
<link href="css/style.css" rel="stylesheet">
<link href="css/style-responsive.css" rel="stylesheet">

<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!--[if lt IE 9]>
  <script src="js/html5shiv.js"></script>
  <script src="js/respond.min.js"></script>
  <![endif]-->
</head>

<body class="sticky-header">

	<section>
		<!-- left side start -->
		<jsp:include page="includes/left_side.jsp"></jsp:include>
		<!-- left side end -->

		<!-- main content start -->
		<div class="main-content">

			<!-- header section start-->
			<jsp:include page="includes/top_side.jsp"></jsp:include>
			<!-- header section end-->

			<!--body wrapper start-->
			<div class="wrapper">
				<!-- <h3>Dashboard</h3> -->

				<div class="panel panel-success">
					<div class="panel-heading">
						<h3 class="panel-title">Dashboard</h3>
					</div>

					<div class="panel-body">
						<div class="row">
							<!-- <div class="col-md-6">
								<img src="images/employment-banner.jpg" width="635px" >
							</div> -->
							<div class="col-md-12">
								<img src="images/banner1.jpg" width="1300px" height="245px"
									class="img-responsive">
							</div>
						</div>

						<br>

						<div class="row">
							<div class="col-md-12">
								<!--statistics start-->
								<div class="row state-overview">

									<div class="col-md-6 col-xs-12 col-sm-6">
										<div class="panel purple">
											<div class="symbol">
												<i class="fa fa-users"></i>
											</div>
											<div class="state-value">
												<div id="all_users" class="value"> </div>
												<div class="title">Total Users</div>
											</div>
										</div>
									</div>
									<div class="col-md-6 col-xs-12 col-sm-6">
										<div class="panel red">
											<div class="symbol">
												<i class="fa fa-user"></i>
											</div>
											<div class="state-value">
												<div id="approved_dev" class="value"> </div>
												<div class="title">Developers Approved</div>
											</div>
										</div>
									</div>

								</div>
								<!--statistics end-->
							</div>
						</div>

						<div class="row">
							<div class="col-md-12">
								<div class="row state-overview">
									<div class="col-md-6 col-xs-12 col-sm-6">
										<div class="panel blue">
											<div class="symbol">
												<i class="fa fa-user"></i>
											</div>
											<div class="state-value">
												<div id="normal_dev" class="value"> </div>
												<div class="title">Total Normal Users</div>
											</div>
										</div>
									</div>
									<div class="col-md-6 col-xs-12 col-sm-6">
										<div class="panel green">
											<div class="symbol">
												<i class="fa fa-eye"></i>
											</div>
											<div class="state-value">
												<div id="reject_dev" class="value"> </div>
												<div class="title">Developers Rejected</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
						<!-- -------------------------------------------------------------------------------------------------- -->
					</div>
				</div>

			</div>
			<!--body wrapper end-->
			<!--footer section start-->
			<jsp:include page="includes/footer.jsp"></jsp:include>
			<!--footer section end-->

		</div>
		<!-- main content end-->
	</section>
	<!-- JS -->
	  <script src = "http://ajax.googleapis.com/ajax/libs/angularjs/1.3.14/angular.min.js"></script>
	<!-- common scripts -->
	<jsp:include page="includes/common_scripts.jsp"></jsp:include>

	<!-- ------------------------------------------------------------- -->
	<!-- jQuery Flot Chart-->
	<script src="js/flot-chart/jquery.flot.js"></script>
	<script src="js/flot-chart/jquery.flot.tooltip.js"></script>
	<script src="js/flot-chart/jquery.flot.resize.js"></script>

	<!--icheck -->
	<script src="js/iCheck/jquery.icheck.js"></script>
	<script src="js/icheck-init.js"></script>

	<!--Morris Chart-->
	<script src="js/morris-chart/morris.js"></script>
	<script src="js/morris-chart/raphael-min.js"></script>

	<!--Dashboard Charts-->
	<script src="js/dashboard-chart-init.js"></script>

	<script>
	var devs = {};
	
	//TODO: ADD NEW BRAND
	devs.listSummary = function(){
		$.ajax({
			url: "listSummary.hrd",
			type: "POST",
			dataType: "JSON",
			success: function(data){
				$("#approved_dev").text(data[0].approve);
				$("#all_users").text(data[1].users);
				$("#normal_dev").text(data[2].normal);
				$("#reject_dev").text(data[3].reject);
			},
			error: function(data){
				console.log(data);
			}
		});
	};
	
	devs.listSummary();
	</script>
</body>
</html>
