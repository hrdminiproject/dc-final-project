<div class="left-side sticky-left-side" style="background-color: #424f63 !important;">

    <!--logo and iconic logo start-->
    <div class="logo">
        <a href="index.jsp"><img src="images/logo.png" alt=""></a>
    </div>

    <div class="logo-icon text-center">
        <a href="index.jsp"><img src="images/logo_icon.png" alt=""></a>
    </div>
    <!--logo and iconic logo end-->

    <div class="left-side-inner">

        <!-- visible to small devices only -->
        <div class="visible-xs hidden-sm hidden-md hidden-lg">
            <div class="media logged-user">
                <img alt="" src="images/photos/user-avatar.png" class="media-object">
                <div class="media-body">
                    <h4><a href="#">John Doe</a></h4>
                    <span>"Hello There..."</span>
                </div>
            </div>

            <h5 class="left-nav-title">Account Information</h5>
            <ul class="nav nav-pills nav-stacked custom-nav">
                <li><a href="#"><i class="fa fa-user"></i> <span>Profile</span></a></li>
                <li><a href="#"><i class="fa fa-cog"></i> <span>Settings</span></a></li>
                <li><a href="#"><i class="fa fa-sign-out"></i> <span>Sign Out</span></a></li>
            </ul>
        </div>

        <!--sidebar nav start-->
        <ul class="nav nav-pills nav-stacked custom-nav">
        	<%
        		if(Integer.parseInt(session.getAttribute("role").toString()) == 0){
        	%>
        		<li><a href="index.jsp"><i class="fa fa-home"></i> <span>Dashboard</span></a></li>
        		<li class="menu-list"><a href=""><i class="fa fa-users"></i> <span>Developers</span></a>
	                <ul class="sub-menu-list">
	                    <li><a href="view_emp.jsp"> All Active</a></li>
	                </ul>
	            </li>
	            <li class="menu-list"><a href=""><i class="fa fa-thumb-tack"></i> <span>Subject &amp; Category</span></a>
	                <ul class="sub-menu-list">
	                	<li><a href="myaddsubject.jsp"> List Subject</a></li>
	                    <li><a href="myaddcategory.jsp"> List Category</a></li>
	                </ul>
	            </li>
	            <li><a href="myaddlanguage.jsp"><i class="fa fa-bullhorn"></i> <span>Language</span></a></li>
	            <li><a href="user.jsp"><i class="fa fa-user"></i> <span>Users</span></a></li>
        	<%
        		}else if(Integer.parseInt(session.getAttribute("role").toString()) == 1){
        	%>
        		<li><a href="profile.jsp"><i class="fa fa-user user"></i> <span>Overview</span></a></li>
            	<li><a href="form_personal.jsp"><i class="fa fa-user user"></i> <span>Personal Information</span></a></li>
	            <li><a href="education.jsp"><i class="fa fa-book user"></i> <span>Education</span></a></li>
	            <li><a href="experience.jsp"><i class="fa fa-bookmark"></i> <span>Experience</span></a></li>
	            <li><a href="achievement.jsp"><i class="fa fa-bullhorn"></i> <span>Achievement</span></a></li>
	            <li><a href="myadddevlanguage.jsp"><i class="fa fa-globe"></i> <span>Language</span></a></li>
	            <li><a href="myadddevsubject.jsp"><i class="fa fa-star"></i> <span>Skill Rating</span></a></li>
            <%
            	}
            %>
            
        </ul>
        <!--sidebar nav end-->

    </div>
</div>
