<div class="header-section">

    <!--toggle button start-->
    <a class="toggle-btn"><i class="fa fa-bars"></i></a>
    <!--toggle button end-->

    <!--notification menu start -->
    <div class="menu-right">
        <ul class="notification-menu">
        	<% if (Integer.parseInt(session.getAttribute("role").toString()) == 0){ %>
            <li class="notification">
                <!-- Get Data from Ajax my-script.js -->
            </li>
            <% } %>

            <li>
                <a href="#" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                    <img src='images/uploads/avatar.png' alt='Avatar' />
                    <%= request.getSession().getAttribute("user") %>
                    <span class="caret"></span>
                </a>
                <ul class="dropdown-menu dropdown-menu-usermenu pull-right">
                    <li>
                    	<a href="#myPass" data-toggle="modal" class="mn-has-sub">
                    		<i class="fa  fa-wrench"></i> Change Password
                    	</a>
                    </li>
                    <li><a href="userOut.hrd"><i class="fa fa-sign-out"></i> Log Out</a></li>
                </ul>
            </li>

        </ul>
    </div>
    <!--notification menu end -->
</div>





<!-- Password Modal -->
	<div aria-hidden="true" aria-labelledby="myModalLabel2" role="dialog"
		tabindex="-1" id="myPass" class="modal fade" style="display: none;">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button aria-hidden="true" data-dismiss="modal" class="close"
						type="button">
						<span class="fa fa-times"></span>
					</button>
					<h4 class="modal-title">Change Password</h4>
				</div>
				<div class="modal-body">
	
					<form role="form" action="changePasswordAdmin.hrd" method="post">
						<div class="form-group">
							<label for="exampleInputEmail1">Old Password</label> <input
								type="password" class="form-control" id="oldpwd" name="oldpwd">
						</div>
						<div class="form-group">
							<label for="exampleInputPassword1">New Password</label> <input
								type="password" class="form-control" id="newpwd" name="newpwd">
						</div>
						<div class="form-group">
							<label for="exampleInput">Confirm New Password</label> <input
								type="password" class="form-control" id="confirmnewpwd" name="confirmnewpwd">
						</div>
						<button class="btn btn-success btn-md btn-block" type="submit">Save</button>
					</form>
				</div>
			</div>
		</div>
	</div>