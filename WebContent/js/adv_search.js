function getParameterByName(name) {
    name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
    var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
    results = regex.exec(location.search);
    return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
}

var app = angular.module('myApp', ['ui.bootstrap']);

app.filter('startFrom', function() {
    return function(input, start) {
        if(input) {
            start = +start; //parse to int
            return input.slice(start);
        }
        return [];
    };
});

app.controller('devCtrl', function ($scope, $http, $timeout) {
	$scope.haveParam = true;
	$scope.loadCat = function (){
		$http({
    		method: 'POST',
    		url: 'listAdvancedSearchField.hrd'
    	}).success(function(response) {
    		$scope.category = response;
    	});
    };
    $scope.loadCat();
	
    $scope.loadData = function () {
    	// Radio
		var salary = $('input[name=salary]:checked', '#frmadv_search').val();
		var experience = $('input[name=experience]:checked', '#frmadv_search').val();

		var keyword = $("#txt-keyword").val();
		var degree = $("#degree").val();
		var major = $("#major").val();
		var position = $("#position").val();
		var category = $("#category").val();
		var skill = $("#skill").val();
		var language = $("#language").val();
		
		// Click count from index.jsp
		if ($scope.haveParam && getParameterByName("exp") != ""){
			$('input[value=' + getParameterByName("exp") + ']').prop('checked', true);
			experience = getParameterByName("exp");
		}
		
		if ($scope.haveParam && getParameterByName("degree") != ""){
			degree = getParameterByName("degree");
		}
		
		if ($scope.haveParam && getParameterByName("category") != ""){
			category = getParameterByName("category");
		}

		// Click from category page
		if ($scope.haveParam && getParameterByName("skill") != ""){
			skill = getParameterByName("skill");
		}
		
		$scope.haveParam = false;
		
		if(skill == null){
			skill = "";
		}
		
		if(language == null){
			language = "";
		}
		//alert(major + position + category + language + degree + skill);
		
		$http({
    		method: 'POST',
    		url: 'advSearch.hrd',
    		data: $.param({
    			keyword: keyword,
    	    	degree: degree,
    			major: major,
    	    	position: position,
    	    	category: category,
    	    	skill: skill.toString(),
    	    	language: language.toString(),
    	    	salary: salary,
    	    	experience: experience
    		}),
    		headers: {'Content-Type': 'application/x-www-form-urlencoded'}
    	}).success(function(response) {
			$scope.devs = response;
			$scope.currentPage = 1 ; //current page
	        $scope.entryLimit = 5; //max no of items to display in a page
	        $scope.filteredItems = $scope.devs.length; //Initially for no filter
	        $scope.totalItems = $scope.devs.length;

	    	    $('html, body').animate({
	    	        scrollTop: $("#result").offset().top
	    	    }, 500);
    	});
    };   
    
    if($scope.haveParam && getParameterByName("exp") != "" || $scope.haveParam && getParameterByName("degree") != "" || $scope.haveParam && getParameterByName("category") != ""  || $scope.haveParam && getParameterByName("skill") != ""){
    	$scope.loadData();
    }
    
    $scope.myff =function(id){
    	var url='developer_detail.jsp';
    	$.ajax({
            type: "POST",
            url: url,
            data: {
            	dev_id:id
            },
            dataType: 'json',
            success: function(data)
            {
         	   if(data != "fail"){
         			   //location.replace("admin/index.jsp"); can't back
         		   location.href = url + "?dev_id=" + id;
         	   }
                else{
                	$('#myModal123').modal('show');
                } 
            }
          });
    }
    
    $scope.setPage = function(pageNo) {
    	$scope.currentPage = pageNo; //current page
    };
    
    $scope.filter = function() {
    	$timeout(function() { 
            $scope.filteredItems = $scope.filtered.length;
        }, 10);
    };
    
    
   	
});
