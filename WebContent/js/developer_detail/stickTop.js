jQuery(
function($) {

	if(($("#single-company").length) || ($("#single-job").length) || ($("#single-resume").length)) {
	
		$(document).ready(function(){
			var contentButton = [];
			var contentTop = [];
			var content = [];
			var lastScrollTop = 0;
			var scrollDir = '';
			var itemClass = '';
			var itemHover = '';
			var menuSize = null;
			var stickyHeight = 0;
			var stickyMarginB = 0;
			var currentMarginT = 0;
			var topMargin = 0;
			$(window).scroll(function(event){
	   			var st = $(this).scrollTop();
	   			if (st > lastScrollTop){
	       			scrollDir = 'down';
	   			} else {
	      			scrollDir = 'up';
	   			}
	  			lastScrollTop = st;
			});
			$.fn.stickUp = function( options ) {
				// adding a class to users div
				$(this).addClass('stuckMenu');
	        	//getting options
	        	var objn = 0;
	        	if(options != null) {
	        		
		        	for(var o in options.parts) {
		        		if (options.parts.hasOwnProperty(o)){
		        		
		        			content[objn] = options.parts[objn];
		        			objn++;
		        		}
		        	}
		        	if(objn == 0) {
		  				console.log('error:needs arguments');
		  			}

		  			itemClass = options.itemClass;
		  			itemHover = options.itemHover;
		  			
		  			if(options.topMargin != null) {
		  				if(options.topMargin == 'auto') {
		  					topMargin = parseInt($('.stuckMenu').css('margin-top'));
		  				} else {
		  					if(isNaN(options.topMargin) && options.topMargin.search("px") > 0){
		  						topMargin = parseInt(options.topMargin.replace("px",""));
		  					} else if(!isNaN(parseInt(options.topMargin))) {
		  						topMargin = parseInt(options.topMargin);
		  					} else {
		  						console.log("incorrect argument, ignored.");
		  						topMargin = 0;
		  					}	
		  				}
		  			} else {
		  				topMargin = 0;
		  			}
		  			
		  			menuSize = $('.'+itemClass).size() - 1;
	  			}			
	        	
				stickyHeight = parseInt($(this).height());
				stickyMarginB = parseInt($(this).css('margin-bottom'));
				currentMarginT = parseInt($(this).next().closest('div').css('margin-top'));
				vartop = parseInt($(this).offset().top);
				//$(this).find('*').removeClass(itemHover);
			}
			$(document).on('scroll', function() {
				varscroll = parseInt($(document).scrollTop());
				var x=0;
				if(menuSize != null){
					for(var i=0;i < menuSize;i++)
					{	contentTop[i] = $('#'+content[i]+'').offset().top;
						
						//Add Class active to menu when scroll down across the block
						if(scrollDir == 'down' && varscroll > contentTop[i]-50 && varscroll < contentTop[i]+50) {
							$('.'+itemClass).removeClass(itemHover);
							$('.'+itemClass+':eq('+(i+1)+')').addClass(itemHover);	
						}
						
						if(scrollDir == 'up') {
							if(varscroll > contentTop[i]){
								$('.'+itemClass).removeClass(itemHover);
								$('.'+itemClass+':eq('+(i+1)+')').addClass(itemHover);
							} else if(varscroll < 50){
								$('.'+itemClass).removeClass(itemHover);
								$('.'+itemClass+':eq(1)').addClass(itemHover);
							}
						}
					}
				}



			});
		});
	
	}

});