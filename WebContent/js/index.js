var app = angular.module('myApp', ['ui.bootstrap']);

app.filter('startFrom', function() {
    return function(input, start) {
        if(input) {
            start = +start; //parse to int
            return input.slice(start);
        }
        return [];
    };
});


app.controller('devCtrl', function ($scope, $http, $timeout, $window, $q) {

   $scope.loadData = function () {
	   
    	 $http.get('list10Dev.hrd').success(function(data){
    		
    	        $scope.devs = data;
    	        $scope.currentPage = 1 ; //current page
    	        $scope.entryLimit = 5; //max no of items to display in a page
    	        $scope.filteredItems = $scope.devs.length; //Initially for no filter
    	        $scope.totalItems = $scope.devs.length;
    	       
    	 });
//    	 
//    	 $http.get('listCat.hrd').success(function(data){
// 	        $scope.cats = data;
// 	       $scope.totalCat = $scope.cats.length;
// 	      $window.alert( $scope.cats);
// 	       	
//    	 });
    	
    	 
    	 
     };
     $scope.loadData();
    
     //kpnew
     $scope.getCat =function(){
     	var url='admin/listCat.hrd';
     	$.ajax({
             type: "POST",
             url: url,
             dataType: 'json',
             success: function(data)
             {
            	 $scope.cats = data;
            	 
             }
           });
     };
     $scope.getCat();
     
     //kpnew
     $scope.skills =function(){
     	var url='admin/listAllDevInSub.hrd';
     	$.ajax({
             type: "POST",
             url: url,
             dataType: 'json',
             success: function(data)
             {
            	 $scope.skills = data;
             }
           });
     };
     $scope.skills();
     
     //kpnew
     $scope.edus =function(){
     	var url='admin/listAllDevInEdu.hrd';
     	$.ajax({
             type: "POST",
             url: url,
             dataType: 'json',
             success: function(data)
             {
            	 $scope.edus = data;
             }
           });
     };
     $scope.edus();

     
   //kpnew
     $scope.exps =function(){
     	var url='admin/listAllDevInExp.hrd';
     	$.ajax({
             type: "POST",
             url: url,
             dataType: 'json',
             success: function(data)
             {
            	 $scope.exps = data;
             }
           });
     };
     $scope.exps();
     
     
     
    $scope.myff =function(id){
    	var url='developer_detail.jsp';
    	$.ajax({
            type: "POST",
            url: url,
            data: {
            	dev_id:id
            },
            dataType: 'json',
            success: function(data)
            {
         	   if(data != "fail"){
         			   //location.replace("admin/index.jsp"); can't back
         		   location.href = url + "?dev_id=" + id;
         	   }
                else{
                	$('#myModal123').modal('show');
                } 
            }
          });
    }
     
    $scope.setPage = function(pageNo) {
    	$scope.currentPage = pageNo; //current page    	       
    };
    $scope.filter = function() {
    	  $timeout(function() { 
            $scope.filteredItems = $scope.filtered.length;
        }, 10);
    };
   
    $scope.developers = [];
    
    $scope.includeDev = function(dev) {
        var i = $.inArray(dev, $scope.developers);
        if (i > -1) {
            $scope.developers.splice(i, 1);
        } else {
            $scope.developers.push(dev);
        }
    }
    
    $scope.devFilter = function(devs) {
        if ($scope.developers.length > 0) {
            if ($.inArray(devs.degree, $scope.developers) < 0)
                return;
        }
        return devs;
    }	
});
