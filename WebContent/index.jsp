<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="sql" uri="http://java.sun.com/jsp/jstl/sql" %>
    <!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
    <html>
	
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
        <title>IT GURU</title>
        <link rel="stylesheet" href="css/index_dev_list.css" type="text/css">
        <link rel="stylesheet" href="css/responsive.css" type="text/css">

        <jsp:include page="includes/head.jsp"></jsp:include>

        <link rel="stylesheet" href="css/index.css">

		<!-- portfolio at bottom -->
        <link rel="stylesheet" href="plugins/portfolio/css/portfolio.css">
		<style type="text/css">
			*::-webkit-input-placeholder {
			    color: white !important;
			}
			*:-moz-placeholder {
			    /* FF 4-18 */
			    color: white !important;
			}
			*::-moz-placeholder {
			    /* FF 19+ */
			    color: white !important;
			}
			*:-ms-input-placeholder {
			    /* IE 10+ */
			    color: white !important;
			}
			h2{color: white !important;}
		</style>
	
    </head>

    <body ng-app="myApp" ng-controller="devCtrl">
        <!-- Navigation panel -->
        <jsp:include page="includes/header.jsp"></jsp:include>
        <!-- End Navigation panel -->
		
        <!-- job-search Section -->
        <section class="page-section" id="job-search" style="padding-bottom: 30px;">
            <div class="container relative">
                <!-- Row -->
                <div class="row">
                    <!-- Col -->
                    <div class="col-sm-12">
                        <!-- Form -->
                        <form method="get" action="advance_search.jsp" role="form" class="form">
                            <div class="home-content">
		                        <div class="home-text">
		                            <h2 class="hs-line-16 font-alt mb-16 mb-xs-30">
		                                Welcome to IT GURU!
		                            </h2>
		                            
		                            <h2 class="hs-line-8 mb-30 mb-xs-30">
		                               Find the best employee in your dream here...
		                            </h2>
		                             <div class="row">
		                            	<div class="col-xs-offset-3 col-sm-6">
		                                    <select class="input-md form-control btn btn-mod btn-border-w" id="category" name="category">
		                                        <option value="">Choose Skill Categories...</option>
		                                        <!-- kpnew  -->
		                                          <option ng-repeat="cat in cats | orderBy:'name'" value="{{cat.name}}">{{cat.name}}</option>
		                                    </select>
		                                     <input  style="margin-top:10px;" type="submit" value="Find by Skill" id="btn-find-job" class="input-md form-control btn btn-mod btn-border-w btn-round btn-medium" style="margin-top: 30px;"/>
		                                </div>
		                               
		                            </div>	
		                            <div class="row">
	                                    <div align="center" style="margin-top:20px;">
	                                        <a href="advance_search.jsp" id="btn-find-job" class="">Advanced Search</a>
	                                    </div>
	                                </div>	                            
		                        </div>
		                    </div> 
                           
                        </form>
                        <!-- End Form -->
                    </div>
                    <!-- End Col -->
                </div>
                <!-- End Row -->
            </div>
            <!-- End container -->
        </section>
        <!-- End job-search Section -->

        <!-- body Section -->
        <section class="page-section" style="padding:0">
            <div class="container">
                <div class="row">
                    <div class="col-md-9">
                        <!-- box listing  -->
                        <div class="block-section-sm box-list-area">
                            <!-- top desc -->
                            <div class="row hidden-xs">
                                <div class="col-sm-12 ">
                                    <label class="h2 uppercase"  style=" text-transform: bold; color:#50A253; padding:0;margin:0 0 15px 0" >Developers</label>
                                    
                                </div>
                            </div>
                            <!-- end top desc -->
							
                            <div class="resume-skills homepage-recent-listings">
                                <div id="tabs" class="full">

                                    <div class="pane" style="display: block;">
                                        <div class="full">
                                            <ul id="companies-block-list-ul">
                                                <li id="1" class="animate" ng-repeat="dev in filtered= devs | startFrom:(currentPage-1)*entryLimit | limitTo:entryLimit">
                                                    <a ng-click="myff(dev.id)">
                                                        <div class="company-holder-block">
                                                            <span class="company-list-icon rounded-img"><img src="images/uploads/developer/{{dev.avatar}}" alt="{{dev.avatar}}">
										                    </span>

                                                            <span class="company-list-name-block" style="max-width: 380px;">
                    											<span class="company-list-name">{{ dev.name }} <span class="resume-prof-title">{{ dev.specialty_skill }}</span><span class="resume-prof-title">{{ totalCat }}</span></span>
                                                                <span class="company-list-location"><span class="resume_job_1"><i class="fa fa-map-marker" ></i>{{dev.current_city_state}}</span>

                                                                </span>
                                                            </span>

                                                            <span class="company-list-view-profile">
                    											<span class="company-view-profile">
                    												<span class="company-view-profile-title-holder">
                    													<span class="company-view-profile-title">View</span>
                                                                        <span class="company-view-profile-subtitle">Resume
                                                                            </span>
                                                                        </span>
                                                                        <i class="fa fa-eye"></i>
                                                                </span>
                                                            </span>
                                                            <span class="company-list-badges" style="margin-top: 19px;">

                    											<span class="job-offers-post-badge" style="max-width: 220px; background-color: #50A253; border: solid 2px #50A253;">
                    												<span class="job-offers-post-badge-job-type" style="width: 110px; color: #7f8c8d; line-height: 16px; padding-top: 9px; text-align: right;">{{dev.exp  == "null" ? "N/A" : dev.exp}} Years Experience</span>
                                                                    <span class="job-offers-post-badge-amount">$ {{dev.salary}}</span>
                                                                    <span class="job-offers-post-badge-amount-per">/Month</span>
                                                                </span>
                                                            </span>
                                                        </div>
                                                    </a>
                                                </li>
                                                
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-12" ng-show="filteredItems > 0">     
                                    <div pagination="" page="currentPage" max-size="10" on-select-page="setPage(page)" boundary-links="true" total-items="filteredItems" items-per-page="entryLimit" class="pagination-small" previous-text="&lt;&lt;" next-text="&gt;&gt;"></div> 
                                </div>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                        <!-- end  box listing  -->
                        
                    </div>
                    <div class="col-md-3">
                        <div class="block-section-sm side-right">
                            <!-- filter   -->
                            <div class="row">
                                <div class="col-xs-6">
                                    <p><strong>Sort by: </strong>
                                    </p>
                                </div>
                            </div>


                            <!-- filter   -->
                            <div class="result-filter">
                                <h5 id="technology" class="no-margin-top font-bold margin-b-20 "><a href="#s_collape_1" data-toggle="collapse" aria-expanded="true" class="">Educations <i class="fa ic-arrow-toogle fa-angle-right pull-right"></i></a> </h5>
                                <div class="collapse in" id="tech" aria-expanded="true">
                                    <div class="list-area">
                                        <ul class="list-unstyled">
                                         <!-- kpnew  -->
		                                <li ng-repeat="edu in edus | orderBy:'-edu'">
											<a href="advance_search.jsp?degree={{edu.deg}}">{{edu.deg}}  </a> ({{edu.edu}})
										</li>  
                                        </ul>
                                    </div>
                                </div>
                               
                                <h5 id="experience" class="font-bold  margin-b-20"><a href="#s_collape_3" data-toggle="collapse">Years of work experience <i class="fa ic-arrow-toogle fa-angle-right pull-right"></i></a> </h5>
                                <div class="collapse" id="exp">
                                    <div class="list-area">
                                    <!-- kpnew  -->
                                        <ul class="list-unstyled ">                                        
                                        		 <li>
                                                	<a href="advance_search.jsp?exp=1-2">1-2 years  </a>({{exps[1].count}})
                                            	</li>
                                        		 <li>
                                                	<a href="advance_search.jsp?exp=3-5">3-5 years  </a>({{exps[3].count}})
                                            	</li>
                                        		 <li>
                                                	<a href="advance_search.jsp?exp=6-10">6-10 years  </a> ({{exps[4].count}})
                                            	</li>
                                        		 <li>
                                                	<a href="advance_search.jsp?exp=11-99">10+ years  </a> ({{exps[2].count}})
                                            	</li>
                                        </ul>
                                    </div>
                                </div>

                                <h5 id="company" class="font-bold  margin-b-20"><a href="#s_collape_4" data-toggle="collapse">Skills <i class="fa ic-arrow-toogle fa-angle-right pull-right"></i> </a> </h5>
                                <div class="collapse" id="com">
                                    <div class="list-area">
                                        <ul class="list-unstyled ">
                                        <!-- kpnew  -->
		                                <li ng-repeat="skill in skills | orderBy:'cat'">
											<a href="advance_search.jsp?category={{skill.cat}}">{{skill.cat}}  </a> ({{skill.skill}})
										</li>                                       
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <!-- end filter   -->
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- End body Section -->

       
        <!-- Footer Section -->
        <jsp:include page="includes/footer.jsp"></jsp:include>
        <!-- End Footer Section -->
        
    
        <!-- JS -->
        <jsp:include page="includes/script.jsp"></jsp:include>
      	<script src="admin/js/angular.min.js"></script>
      	<script src="js/ui-bootstrap-tpls-0.10.0.min.js"></script>
        <script src="js/index.js"></script>
        
        <!-- Category Box script -->
        <script type="text/javascript" src="plugins/jquery/jquery.easing.1.3.min.js"></script>	
        <script type="text/javascript" src="plugins/portfolio/js/jquery.mixitup.min.js"></script>

        <script type="text/javascript">
       
        
        
        	/* Advance search form */
      		$(function(){
      			$(".panel-group").hide();
      			$("#btn-find-job").click(function(){
      				$(".panel-group").toggle(1000);
      			});
      		});
        	
      		/* Right form  */
            $(function () {
            	$('h5').click(function(){
                    $(this).siblings('div').slideUp();
                    $(this).next().toggle();
                    $(this).find('i').toggleClass('fa-angle-right').toggleClass('fa-angle-up');
	        	});
           	});	

         // this is the id of the form
    		$("#myModal123 form").submit(function() {
    			$.ajax({
    				type : "POST",
    				url : 'userIn.hrd',
    				data : $("#myModal123 form").serialize(), // serializes all the form's elements.
    				success : function(data) {
    					if (data != "fail") {
    						if (data.uri == "") {
    							if (data.role == "0")
    								//location.replace("admin/index.jsp"); can't back
    								location.href = "admin";
    							else
    								location.reload();
    						} else {
    							location.href = data.uri;
    						}
    					} else {
    						$('#myModal123 .modal-body .alert').show();
    						$('#myModal123').on('hidden.bs.modal', function() {
    							$('#myModal123 .modal-body .alert').hide();
    							$('#username').val("");
    							$('#password').val("");
    						});
    					}
    				}
    			});

    			return false; // avoid to execute the actual submit of the form.
    		});
        </script>
    </body>

    </html>
    
    