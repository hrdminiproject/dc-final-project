<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1" %>
    <!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
    <html>
	
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
        <title>Category Page</title>
        <link rel="stylesheet" href="css/index_dev_list.css" type="text/css">
        <link rel="stylesheet" href="css/responsive.css" type="text/css">

        <jsp:include page="includes/head.jsp"></jsp:include>

       
		<!-- portfolio at bottom -->
        <link rel="stylesheet" href="plugins/portfolio/css/portfolio.css">
        
    <style>
        
		.nav-tabs {
			border-bottom: 2px solid #DDD;
		}
		
		.nav-tabs>li.active>a, .nav-tabs>li.active>a:focus, .nav-tabs>li.active>a:hover
			{
			border-width: 0;
		}
		
		.nav-tabs>li>a {
			border: none;
			color: #666;
		}
		
		.nav-tabs>li.active>a, .nav-tabs>li>a:hover {
			border: none;
			color: #4285F4 !important;
			background: transparent;
		}
		
		.nav-tabs>li>a::after {
			content: "";
			background: #4285F4;
			height: 2px;
			position: absolute;
			width: 100%;
			left: 0px;
			bottom: -1px;
			transition: all 250ms ease 0s;
			transform: scale(0);
		}
		
		.nav-tabs>li.active>a::after, .nav-tabs>li:hover>a::after {
			transform: scale(1);
		}
		
		.tab-nav>li>a::after {
			background: #21527d none repeat scroll 0% 0%;
			color: #fff;
		}
		
		.tab-pane {
			padding: 15px 0;
		}
		
		.tab-content {
			padding: 20px;
		}
		
		.card {
			background: #FFF none repeat scroll 0% 0%;
			box-shadow: 0px 1px 3px rgba(0, 0, 0, 0.3);
			margin-bottom: 30px;
		}
		
		body {
			background: #EDECEC;
			padding-top: 50px;
		}
	</style>
</head>

    <body>
        <!-- Navigation panel -->
        <jsp:include page="includes/header.jsp"></jsp:include>
        <!-- End Navigation panel -->
       
	<!-- category section -->
	<section class="page-section"
		style="padding-top:50px; padding-bottom:20px">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<!-- Nav tabs -->
				<div class="card">
					<!-- category menu -->
					<ul id="filters" class="nav nav-tabs">

					</ul>

					<!-- category images -->
					<div id="portfoliolist" class="clearfix"></div>
				</div>
			</div>
		</div>
	</div>
	</section>
	<!-- end category section -->

		<!-- Footer Section -->
        <jsp:include page="includes/footer.jsp"></jsp:include>
        <!-- End Footer Section -->
        
    
        <!-- JS -->
        <jsp:include page="includes/script.jsp"></jsp:include>
      	      
        <!-- Category Box script -->
        <script type="text/javascript" src="plugins/jquery/jquery.easing.1.3.min.js"></script>	
        <script type="text/javascript" src="plugins/portfolio/js/jquery.mixitup.min.js"></script>
       
        <script>
        $(function() {
			var loadElement = function(callbackAnimate) {
				$.post("admin/listSubCat.hrd", function(data) {
					$('#filters').html(getElement(data).category);
					$("#portfoliolist").html(getElement(data).subject);
					callbackAnimate();
				});
			}

			var applyAnimate = function() {
				var filterList = {

					init : function() {
						// MixItUp plugin
						$('#portfoliolist').mixitup({
							targetSelector : '.portfolio',
							filterSelector : '.filter',
							effects : [ 'fade' ],
							easing : 'ease',
							showOnLoad : 'web-design',
							// call the hover effect
							onMixEnd : filterList.hoverEffect()
						});	
						
						$("#filters li:first-child" ).addClass( "active" );
					},

					hoverEffect : function() {

						// Simple parallax effect
						$('#portfoliolist .portfolio').hover(function() {
							$(this).find('.label').stop().animate({
								bottom : 10
							}, 200, 'easeOutQuad');
							
							$(this).find('img').stop().animate({
								top : -55
							}, 300, 'easeOutQuad');
							
						}, function() {
							$(this).find('.label').stop().animate({
								bottom : -40
							}, 200, 'easeInQuad');
							
							$(this).find('img').stop().animate({
								top : 0
							}, 500, 'easeOutQuad');
						});

					}

				};

				// Run the show!
				filterList.init();
			}
			loadElement(applyAnimate);
		});

		function getElement(data) {
			
			var sub = "", cat = "", img = "";
			var subject = data.subject, category = data.category;
			
			for (var i = 0; i < subject.length; i++) {
				img = subject[i].categories.replace(/ /g, "-").toLowerCase();
				
				sub += '<div class="portfolio '+ img +'" data-cat="'+img+'">'
						+ '<a href="advance_search.jsp?skill=' + subject[i].subjects + '"><div class="portfolio-wrapper" id="">'
						+ '<img class="" src="images/uploads/subject/'+subject[i].image_url+'"/>'
						+ '<div class="label">' + '<div class="label-text">'
						+ '<span class="text-title">' + subject[i].subjects + '</span>'
						+ '<span class="text-category">' + subject[i].categories
						+ '</span>' + '</div></div></div></a></div>';
			}
			
			for(var i=0; i< category.length;i++){
				//<li><a href="#" role="tab" data-toggle="tab" class="filter" data-filter="web-programming">Home</a></li>
				//cat += '<li><span class="filter" data-filter="'+ category[i].name.replace(/ /g, "-").toLowerCase() +'">'+ category[i].name+ '</span></li> '
				cat += '<li><a href="#" data-toggle="tab" class="filter" data-filter="'+ category[i].name.replace(/ /g, "-").toLowerCase() +'">'+ category[i].name+ '</a></li> '
			}

			var result = { subject:sub, category:cat }
			console.log(result);
			return result;
		}
		
		  // this is the id of the form
		$("#myModal123 form").submit(function() {
			$.ajax({
				type : "POST",
				url : 'userIn.hrd',
				data : $("#myModal123 form").serialize(), // serializes all the form's elements.
				success : function(data) {
					if (data != "fail") {
						if (data.uri == "") {
							if (data.role == "0")
								//location.replace("admin/index.jsp"); can't back
								location.href = "admin";
							else
								location.reload();
						} else {
							location.href = data.uri;
						}
					} else {
						$('#myModal123 .modal-body .alert').show();
						$('#myModal123').on('hidden.bs.modal', function() {
							$('#myModal123 .modal-body .alert').hide();
							$('#username').val("");
							$('#password').val("");
						});
					}
				}
			});

			return false; // avoid to execute the actual submit of the form.
		});
        </script>
    </body>

    </html>