<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1" %>
    <!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
    <html>

    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
        <title>Profile Details</title>

        <link rel="shortcut icon" href="images/favicon.png">

        <link rel='stylesheet' href='css/developer-detail.css' type='text/css' media='all' />
        <link rel='stylesheet' href='css/responsive.css' type='text/css' media='all' />

        <link rel='stylesheet' href='plugins/font-awesome-4.4.0/css/font-awesome.min.css' type='text/css' media='all' />
        <link rel="stylesheet" href="plugins/animate/animate.css">
        <!-- font -->
        <link rel='stylesheet' href='http://fonts.googleapis.com/css?family=Montserrat%3A400%2C70&amp;ver=4.2.3' type='text/css' media='all' />
        <link rel='stylesheet' href='http://fonts.googleapis.com/css?family=Roboto%3A400%2C400italic%2C500%2C500italic%2C700%2C700italic&amp;ver=4.2.3' type='text/css' media='all' />
        <link rel='stylesheet' href='http://fonts.googleapis.com/css?family=PT+Serif%3A400%2C700%2C400italic%2C700italic&amp;ver=4.2.3' type='text/css' media='all' />
        <link rel='stylesheet' href='http://fonts.googleapis.com/css?family=Roboto+Slab%3A700%7CMontserrat%3A400&amp;subset=latin&amp;ver=1422121534' type='text/css' media='all' />

        <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
        <style>
            .parent2 {
                top: 7px;
                right: 20px;
                position: fixed;
                display: block;
                width: 50px;
                height: 50px;
                background-color: transparent;
                border-radius: 50%;
                z-index: 20;
                -webkit-box-shadow: 0px 0px 5px 0px rgba(0, 0, 0, 0.75);
                -moz-box-shadow: 0px 0px 5px 0px rgba(0, 0, 0, 0.75);
                box-shadow: 0px 0px 5px 0px rgba(0, 0, 0, 0.75);
            }
            
            .test1 {
                width: 80%;
                height: 80%;
                background-color: dimGray;
                border-radius: 50%;
                position: absolute;
                color: white;
                text-align: center;
                line-height: 70px;
                padding-top: 7px;
                top: 18%;
                left: 12%;
                z-index: 19;
                transition: all 500ms cubic-bezier(0.680, -0.550, 0.265, 1.550);
                position: absolute;
            }
            
            .test2 {
                width: 80%;
                height: 80%;
                background-color: dimGray;
                border-radius: 50%;
                position: absolute;
                color: white;
                text-align: center;
                line-height: 70px;
                padding-top: 7px;
                top: 17%;
                left: 5%;
                z-index: 19;
                transition: all 500ms cubic-bezier(0.680, -0.550, 0.265, 1.550) .2s;
            }
            
          
            .test3 {
                width: 80%;
                height: 80%;
                background-color: dimGray;
                border-radius: 50%;
                position: absolute;
                color: white;
                text-align: center;
                line-height: 70px;
                padding-top: 7px;
                top: 10%;
                left: 3%;
                transition: all 500ms cubic-bezier(0.680, -0.550, 0.265, 1.550) .6s;
            }
            
            .mask2 {
                top: 7px;
                right: 20px;
                width: 50px;
                height: 50px;
                background: #048853;
                border-radius: 50%;
                position: absolute;
                z-index: 21;
                color: white;
                text-align: center;
                line-height: 120px;
                padding-top: 5px;
                cursor: pointer;
                position: fixed;
            }
            
			div.resume-skills{
			    border-bottom-width: 0px !important;
			    padding-bottom: 0px !important;
			    margin-bottom: 0px !important;
			}
			
			section#resume-experience-block, section#resume-skills-block{
				padding-top: 0px !important;
			}
			
			section#resume-education-block{
				padding-top: 40px !important;
				padding-bottom: 0px !important;
			}

</style>

        <script type='text/javascript' src='js/developer_detail/jquery-1.11.2.min.js'></script>

        <!-- Effect progress bar -->
        <script type='text/javascript' src='js/developer_detail/jquery-migrate.min.js'></script>

    </head>

    <body id="single-resume" class="single single-resume postid-287 single-author pdStuck" ng-app="myApp" ng-controller="MyCtrl">

        <!-- top fixed menu -->
        <section id="resume-menu" class="stuckMenu isStuck" style="position: fixed; top: 0px;">

            <div class="container">

                <!-- list menu -->
                <ul class="nav navbar-nav">

                    <li class="menuItem active"><a href="index.jsp"><i class="fa fa-home"></i>Home</a>
                    </li>
                    <li class="menuItem"><a href="#resume-about-block"><i class="fa fa-file-text-o"></i>About</a>
                    </li>
                    <li class="menuItem"><a href="#resume-skills-block"><i class="fa fa-bar-chart-o"></i>Skills</a>
                    </li>
                    <li class="menuItem"><a href="#resume-education-block"><i class="fa fa-university"></i>Education</a>
                    </li>
                    <li class="menuItem"><a href="#resume-experience-block"><i class="fa fa-building"></i>Experience</a>
                    </li>
                    <li class="menuItem"><a href="#resume-contact-block"><i class="fa fa-envelope"></i>Contact</a>
                    </li>
                </ul>

                <!-- round menu -->
                <div class="parent2">
                    <div class="test1">
                       <a href="frontUserOut.hrd"> <i class="fa  fa-sign-out fa-2x"></i></a>
                    </div> 
                     <div class="test2">
                      <% if (pageContext.getAttribute("role", PageContext.SESSION_SCOPE).toString().contains("0") || pageContext.getAttribute("role", PageContext.SESSION_SCOPE).toString().contains("1")) { %>
							 <a href="admin/profile.jsp"><i class="fa fa-wrench fa-2x"></i></a>
						<% } else {	%>
							 <a href="admin/becomeDeveloper.hrd"><i class="fa fa-github-alt fa-2x"></i></a>
						<% } %>
                    </div>
                     <div class="test3">
                        <a href="advance_search.jsp"><i class="fa fa-search fa-2x"></i></a>
                    </div>
                    <div class="mask2">
                    	<i class="fa fa-home fa-3x"></i>
                    </div>
                </div>

                <select id="mobile-nav-bar" onchange="location = this.options[this.selectedIndex].value;">
                    <option value="#backtop">Home</option>
                    <option value="#resume-about-block">About</option>
                    <option value="#resume-skills-block">Skills</option>
                    <option value="#resume-education-block">Education</option>
                    <option value="#resume-experience-block">Experience</option>
                    <option value="#resume-contact-block">Contact</option>
                </select>

            </div>

        </section>
        <!-- end top fixed menu -->

        <!-- about -->
        <section id="resume-about-block">

            <div class="container">

                <div class="three_fifth first">

                    <h1 class="resume-author-name">{{info.gender == "Male" ? "MR." : "MS."}} {{info.fname}} {{info.lname}}</h1>
                    <h2 class="resume-author-subtitle">Position: {{ info.wantedPosition }}</h2>

                    <div class="full">
                        <span class="resume-experience-years-block">
                        <i class="fa fa-clock-o"></i>
                        <span class="experience-period">{{ getExperienceYear() > 1 ? getExperienceYear() + " Years" : getExperienceYear() + " Year" }} </span>
                        <span class="experience-subtitle">of experience</span>
                        </span>

                        <span class="resume-experience-years-block">
                        <i class="fa fa-thumbs-o-up"></i>
                        <span class="experience-period">{{info.specialtySkill}}</span>
                        <span class="experience-subtitle">Specialty</span>
                        </span>

                        <span class="resume-expect-revenue-block">
                        <i class="fa fa-money"></i>
                        <span class="experience-period">{{info.salary}}$</span>
                        <span class="experience-subtitle">/Month</span>
                        </span>
                    </div>

                    <div class="full">
                        {{ info.motivation }}
                    </div>

                </div>

                <div class="two_fifth">
                    <div class="resume-author-avatar">
                        <span class="resume-author-avatar-holder">
                        <img src="images/uploads/developer/{{info.avatar}}" width="260px" alt='{{info.avatar}}'/>					</span>

                        <span class="resume-download-file">
                        
                        
                        <a href="report.pdf?id={{info.id}}" target="_blank" rel="external"><i class="fa fa-cloud-download"></i>Download Resume</a>
                       <!--  <a href="files/cv-sample13.pdf" rel="external"><i class="fa fa-cloud-download"></i>Download Resume</a> -->
                    </span>
                    </div>
                </div>
            </div>
        </section>
        <!-- end about -->

        <!-- skill -->
        <section id="resume-skills-block">
            <div class="container">
                <div class="resume-skills">
                    <h1 class="resume-section-title"><i class="fa fa-bar-chart-o"></i>Skills</h1>
                    <h3 class="resume-section-subtitle">Here is an overview of my skills.</h3>

                    <div id="skill" class="one_half first single-resume-skills">
                        <div class="one_half first"><span class="main-skills-item-title-language">Skills</span>
                        </div>
                    </div>

                    <div class="one_half single-resume-skills">

                        <div class="one_half first"><span class="main-skills-item-title-language">{{getAmountLanguages()}} Languages</span>
                        </div>
                        
                        <div class="full main-skills-item-language">

                            <div class="full" ng-repeat="lang in languages">
                                <span class="main-skills-item-title-language native-language-all">{{lang.language}}</span>

                                <div class="full" style="margin-bottom: 0; margin-top:5px">
                                    <div class="one_half first" style="margin-bottom: 0px;">
                                        <span class="main-skills-item-title-language native-small-language-all">Understanding</span>
                                    </div>

                                    <div class="one_half" style="margin-bottom: 0px;">
                                        <span ng-repeat="i in getNumber(5) track by $index">
                               		 <span class="main-skills-item-title-language">
                            			<i class="fa fa-star{{(lang.understandingLevel < ($index+1))  ? '-o' : ''}}" style="color: #16a085"></i>
                            		 </span>
                                        </span>
                                    </div>
                                </div>

                                <div class="full" style="margin-bottom: 0;">
                                    <div class="one_half first" style="margin-bottom: 0px;">
                                        <span class="main-skills-item-title-language native-small-language-all">Speaking</span>
                                    </div>

                                    <div class="one_half" style="margin-bottom: 0px;">
                                        <span ng-repeat="i in getNumber(5) track by $index">
                               		 <span class="main-skills-item-title-language">
                            			<i class="fa fa-star{{(lang.speakingLevel < ($index+1))  ? '-o' : ''}}" style="color: #2980B9"></i>
                            		 </span>
                                        </span>
                                    </div>
                                </div>

                                <div class="full" style="margin-bottom: 0;">
                                    <div class="one_half first" style="margin-bottom: 0px;">
                                        <span class="main-skills-item-title-language native-small-language-all">Writing</span>
                                    </div>

                                    <div class="one_half" style="margin-bottom: 0px;">
                                        <span ng-repeat="i in getNumber(5) track by $index">
                               		 <span class="main-skills-item-title-language">
                            			<i class="fa fa-star{{(lang.writingLevel < ($index+1))  ? '-o' : ''}}" style="color: #E74C3C"></i>
                            		 </span>
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- end skill -->

        <!-- education -->
        <section id="resume-education-block">
            <div class="container">
                <div class="two_third first">

                    <h1 class="resume-section-title"><i class="fa fa-university"></i>Education</h1>
                    <h3 class="resume-section-subtitle">Here is an overview of education institutions I attended.</h3>

                    <div class="wow bounceInUp" ng-repeat="edu in educations">
                        <!-- add class animate to show div because it conflict with angular  -->
                        <span class="education-institution-block animate">
                 
                    <span class="education-period-circle">
                        <span class="education-period-time">{{edu.start_year}}</span>
                        <span class="education-period-time">-</span>
                        <span class="education-period-time">{{edu.end_year}}</span>
                        </span>
                        <span class="education-institution-name">{{edu.school}}</span>
                        <span class="education-institution-faculty-name">{{edu.degree}} in {{edu.major}}</span>
                        <span class="education-institution-location"><i class="fa fa-map-marker"></i>Phnom Penh, Cambodia</span>
                        <span class="education-institution-notes">{{ edu.description }}</span>
                        </span>
                    </div>
                </div>

                <div class="one_third">
                    <div class="resume-skills awards-trophy">
                        <h1 class="resume-section-title"><i class="fa fa-trophy"></i>Achievement</h1>
                        <div class="divider"></div>
                        <div class="wow bounceInUp" ng-repeat="ach in achievements">
                            <!-- add class animate to show div because it conflict with angular  -->
                            <span class="education-institution-block animate">
                        <span class="education-period-circle">
                            <span class="education-period-trophy"><i class="fa fa-trophy"></i></span>
                            <span class="education-period-time">{{ach.year}}</span>
                            </span>
                            <span class="education-institution-name">{{ach.title}}</span>
                            <span class="education-institution-faculty-name">{{ach.description}}</span>
                            <span class="education-institution-location"><i class="fa fa-map-marker"></i>{{ach.location}}</span>
                            </span>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- end education -->

        <!-- experience -->
        <section id="resume-experience-block">

            <div class="container">

                <div class="resume-skills">

                    <h1 class="resume-section-title"><i class="fa fa-building"></i>Work Experience</h1>
                    <h3 class="resume-section-subtitle">Here is a list of companies where I worked and gained my professional experience.</h3>

                    <div class="divider" style="margin-top: 10px;"></div>

                    <div class="work-experience-holder">
                        <div class="wow bounceInUp" ng-repeat="exp in experiences">
                            <span class="work-experience-block animate">

                        <span class="work-experience-first-block">
                            <span class="work-experience-first-block-content">
                                <span class="work-experience-org-name">{{exp.place}}</span>
                                
	                            <span id="single-company">
	                                <span class="work-experience-job-title">{{exp.position}}</span>
	                            </span>
                            
                            	<span class="work-experience-job-title">{{exp.skill}}</span>

                            </span>
                       </span>

                            <span class="work-experience-second-block">

                            <span class="work-experience-second-block-content">

                                <span class="work-experience-time-line"></span>

                            <span class="work-experience-period">{{exp.duration > 1 ? exp.duration + " Years" : exp.duration  + " Year"}}</span>
                            <span class="work-experience-job-type">{{exp.type}}</span>

                            </span>

                            </span>

                            <span class="work-experience-third-block">

                            <span class="work-experience-third-block-content">

                                <span class="work-experience-notes">{{exp.description}}</span>
                            </span>

                            </span>

                            </span>
                        </div>
                    </div>
                </div>

            </div>

        </section>
        <!-- end experience -->


        <!-- Contact Form and Contact Details -->
        <section id="resume-contact-block">

            
           
            <div class="container">

                <div class="resume-contact">

                    <div class="two_third first">


                        <h1 class="resume-section-title" style="margin-bottom: 10px;"><i class="fa fa-list-ul"></i>Contact Developer</h1>



                        <h3 class="resume-section-subtitle">Use this contact form to send an email.</h3>


                        <div id="resume-contact">


                            <form id="contact" type="post" action="#">

                                <span class="contact-name">
                                <input type="text"  name="contactName" id="contactName" value="" class="input-textarea" placeholder="Name*" />
                            </span>

                                <span class="contact-email">
                                <input type="text" name="email" id="email" value="" class="input-textarea" placeholder="Email*" />
                            </span>

                                <span class="contact-message">
                                <textarea name="message" id="message" cols="8" rows="8" ></textarea>
                            </span>

                                <span class="contact-test">
                                <p style="margin-top: 20px;">Human test. Please input the result of 5+3=?</p>
                                <input type="text" onfocus="if(this.value=='')this.value='';" onblur="if(this.value=='')this.value='';" name="answer" id="humanTest" value="" class="input-textarea" />
                            </span>

                                <input type="text" name="receiverEmail" id="receiverEmail" value="oliviamurphy@wpjobus.com" class="input-textarea" style="display: none;" />

                                <input type="hidden" name="action" value="wpjobContactForm" />
                                <input type="hidden" id="scf_nonce" name="scf_nonce" value="883ece70f7" />
                                <input type="hidden" name="_wp_http_referer" value="/themes/wpjobus/resume/287/" />
                                <input style="margin-bottom: 0;" name="submit" type="submit" value="Send Message" class="input-submit">

                                <span class="submit-loading"><i class="fa fa-refresh fa-spin"></i></span>

                            </form>

                            <div id="success">
                                <span>
                                <h3>Thank you! We will get back to you as soon as possible.</h3>
                            </span>
                            </div>

                            <div id="error">
                                <span>
                                <h3>Something went wrong, try refreshing and submitting the form again.</h3>
                            </span>
                            </div>

                            <script type="text/javascript">
                                jQuery(function ($) {
                                    jQuery('#contact').validate({
                                        rules: {
                                            contactName: {
                                                required: true
                                            },
                                            email: {
                                                required: true,
                                                email: true
                                            },
                                            message: {
                                                required: true
                                            },
                                            answer: {
                                                required: true,
                                                answercheck: true
                                            }
                                        },
                                        messages: {
                                            name: {
                                                required: "Come on, you have a name donÃ¢Â€Â™t you?"
                                            },
                                            email: {
                                                required: "No email, no message."
                                            },
                                            message: {
                                                required: "You have to write something to send this form."
                                            },
                                            answer: {
                                                required: "Sorry, wrong answer!"
                                            }
                                        },
                                        submitHandler: function (form) {
                                            jQuery('#contact .input-submit').css('display', 'none');
                                            jQuery('#contact .submit-loading').css('display', 'block');
                                            jQuery(form).ajaxSubmit({
                                                type: "POST",
                                                data: jQuery(form).serialize(),
                                                url: 'http://alexgurghis.com/themes/wpjobus/wp-admin/admin-ajax.php',
                                                success: function (data) {
                                                    jQuery('#contact :input').attr('disabled', 'disabled');
                                                    jQuery('#contact').fadeTo("slow", 0, function () {
                                                        jQuery('#contact').css('display', 'none');
                                                        jQuery(this).find(':input').attr('disabled', 'disabled');
                                                        jQuery(this).find('label').css('cursor', 'default');
                                                        jQuery('#success').fadeIn();
                                                    });
                                                },
                                                error: function (data) {
                                                    jQuery('#contact').fadeTo("slow", 0, function () {
                                                        jQuery('#error').fadeIn();
                                                    });
                                                }
                                            });
                                        }
                                    });
                                });
                            </script>


                        </div>

                    </div>

                    <div class="one_third">

                        <h1 class="resume-section-title" style="margin-bottom: 80px;"><i class="fa fa-envelope"></i>Contact Details</h1>


                        <span class="resume-contact-info">

                        <i class="fa fa-map-marker"></i><span>{{info.address}}</span>

                        </span>



                        <span class="resume-contact-info">

                        <i class="fa fa-mobile"></i><span>{{info.phone}}</span>

                        </span>



                        <span class="resume-contact-info">


                        <i class="fa fa-link"></i><span>{{info.email}}</span>

                        </span>





                        <span class="resume-contact-info">


                        <i class="fa fa-facebook-square"></i><span><a href="http://www.facebook.com/">Facebook</a></span>

                        </span>



                        <span class="resume-contact-info">


                        <i class="fa fa-linkedin-square"></i><span><a href="http://#">LinkedIn</a></span>

                        </span>



                        <span class="resume-contact-info">


                        <i class="fa fa-twitter-square"></i><span><a href="http://#">Twitter</a></span>

                        </span>



                        <span class="resume-contact-info">


                        <i class="fa fa-google-plus-square"></i><span><a href="http://#">Google+</a></span>

                        </span>


                    </div>

                </div>

            </div>

        </section>

        <footer>

            <div class="container">

                <div class="full" style="margin-bottom: 0;">

                    <div class="one_fourth first" style="margin-bottom: 0;">
                        <div class="widget">
                            <h4 class="block-title">Useful Links</h4>
                            <div class="menu-footer-widget-container">
                                <ul id="menu-footer-widget" class="menu">
                                    <li id="menu-item-550" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-550"><a href="#">About Us</a>
                                    </li>
                                    <li id="menu-item-551" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-551"><a href="#">Our Blog</a>
                                    </li>
                                    <li id="menu-item-552" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-552"><a href="#">Contact Us</a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>

                    <div class="one_fourth" style="margin-bottom: 0;">
                        <div class="widget">
                            <h4 class="block-title">Follow Us</h4>
                            <div class="menu-social-links-container">
                                <ul id="menu-social-links" class="menu">
                                    <li id="menu-item-83" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-83"><a title="alt" href="https://www.facebook.com/">Facebook</a>
                                    </li>
                                    <li id="menu-item-549" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-549"><a href="#">Twitter</a>
                                    </li>
                                    <li id="menu-item-553" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-553"><a href="#">Google +</a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>

                    <div class="one_fourth" style="margin-bottom: 0;">
                        <div class="widget">
                            <h4 class="block-title">About IT Guru</h4>
                            <div class="textwidget">
                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. </p>
                                <p>Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
                            </div>
                        </div>
                    </div>

                    <div class="one_fourth" style="margin-bottom: 0;">
                        <div class="widget">
                            <h4 class="block-title">Newsletter</h4>
                            <div class="textwidget">
                                <p>Sign me up for the newsletter!</p>


                                <!-- Form by MailChimp for WordPress plugin v2.0.1 - http://dannyvankooten.com/mailchimp-for-wordpress/ -->
                                <form method="post" action="" id="mc4wp-form-1" class="form mc4wp-form">
                                    <p>
                                        <input type="email" id="mc4wp_email" name="EMAIL" required placeholder="Your email address" />
                                    </p>

                                    <p>
                                        <input type="submit" value="Sign up" />
                                    </p>
                                    <textarea name="_mc4wp_required_but_not_really" style="display: none !important;"></textarea>
                                    <input type="hidden" name="_mc4wp_form_submit" value="1" />
                                    <input type="hidden" name="_mc4wp_form_instance" value="1" />
                                    <input type="hidden" name="_mc4wp_form_nonce" value="a9b7a13d54" />
                                </form>
                                <!-- / MailChimp for WP Plugin -->
                            </div>
                        </div>
                    </div>

                </div>



            </div>

        </footer>

        <section class="socket">

            <div class="container">

                <div class="site-info">

                    Made with <i class="fa fa-heart"></i> by <a class="target-blank" href="http://themeforest.net/user/agurghis/portfolio?ref=agurghis">Phnom Penh Group 3</a>

                </div>

                <div class="footer_menu">
                </div>

                <div class="backtop">
                    <a href="#backtop"><i class="fa fa-chevron-up"></i></a>
                </div>

            </div>

        </section>



        <script src="admin/js/angular.min.js"></script>
        <script type='text/javascript' src='https://maps.googleapis.com/maps/api/js?v=3.exp&amp;sensor=false&amp;ver=2013-07-18'></script>


        <script type='text/javascript' src='js/developer_detail/gmap3.infobox.js'></script>

        <!-- form validation -->
        <script type='text/javascript' src='js/developer_detail/jquery.form.js'></script>
        <script type='text/javascript' src='js/developer_detail/jquery.validate.min.js'></script>

        <!-- effect progress bar -->
        <script type='text/javascript' src='js/developer_detail/jquery.isotope.min.js'></script>
        <script type='text/javascript' src='js/developer_detail/jquery-ui.js'></script>
        <script type='text/javascript' src='js/developer_detail/gmap3.min.js'></script>

        <!-- stick menu to the top -->
        <script type='text/javascript' src='js/developer_detail/stickTop.js'></script>

        <!-- effect -->
        <script type='text/javascript' src='js/developer_detail/jquery.inview.js'></script>

        <!-- Style -->
        <script type='text/javascript' src='js/developer_detail/custom.js'></script>
        <script type='text/javascript' src='js/developer_detail/owl.carousel.js'></script>

        <script src="plugins/jquery/wow.min.js"></script>
        <script>
            new WOW().init();
        </script>

        <script>
            function getParameterByName(name) {
                name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
                var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
                    results = regex.exec(location.search);
                return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
            }

            var x = getParameterByName("dev_id");
            var app = angular.module('myApp', []);
            app.controller('MyCtrl', function ($scope, $http) {
                $http({
                    method: 'post',
                    url: 'admin/listDevInfo.hrd?dev_id=' + x,
                    headers: {
                        'Content-Type': 'application/x-www-form-urlencoded'
                    }
                }).success(function (response) {
                    $scope.skills = response.skills;
                    $scope.achievements = response.achievements;
                    $scope.educations = response.educations;
                    $scope.experiences = response.experiences;
                    $scope.info = response.info;
                    $scope.languages = response.languages;

                    $scope.getExperienceYear = function () {
                        var total = 0;
                        for (var i = 0; i < $scope.experiences.length; i++) {
                            var amount = parseInt($scope.experiences[i].duration);
                            total += amount;
                        }
                        return total;
                    }

                    $scope.getAmountLanguages = function () {
                        var total = 0;
                        for (var i = 0; i < $scope.languages.length; i++) {
                            total = i;
                        }
                        total++;
                        return total;
                    }

                    $scope.getNumber = function (num) {
                        return new Array(num);
                    }


                    $(function () {
                        var loadElement = function (callbackAnimate) {
                            $('#skill').append(getElement());
                            callbackAnimate();
                        }

                        loadElement(progressBarAnimate);
                    });

                    function getElement() {
                        var str = "";
                        for (var i = 0; i < $scope.skills.length; i++) {
                            str += ' <span class="main-skills-item bar-skills-item">' + '<span class="main-skills-item-title skill-title">' + $scope.skills[i].name + '&nbsp;&nbsp;&nbsp;-&nbsp;&nbsp;&nbsp;' + $scope.skills[i].rate + '0%</span>' + ' <span class="main-skills-item-bar">' + '<span class="main-skills-item-bar-color skill-bg" style="width:' + $scope.skills[i].rate + '0%;"></span></span></span>';
                        }
                        return str;
                    }
                    
                	var mapDiv,
                    map,
                    infobox;
                jQuery(document).ready(function ($) {
                	var latitude, longitude;
                	switch($scope.info.currentCityState){	
                	case "Battambong" :
                		latitude = 13.0957599;
                		longitude = 103.1977154;
                		break;
                	case "Bonteay Meanchey" :
                		latitude = 13.796013;
                		longitude = 102.8921469;
                		break;
                	case "Kandal" :
                		latitude = 11.4828967;
                		longitude = 104.9508421;
                		break;
                	case "Kep" :
                		latitude = 10.5404394;
                		longitude = 104.3193393;
                		break;
                	case "Koh Kong" :
                		latitude = 11.3706219;
                		longitude = 103.114934;
                		break;
                	case "Kompong Chhnang" :
                		latitude = 12.2508196;
                		longitude = 104.6668983;
                		break;
                	case "Kompong Speu" :
                		latitude = 11.4729348;
                		longitude = 104.5137013;
                		break;
                	case "Kompong Thom" :
                		latitude = 12.6510318;
                		longitude = 104.9230025;
                		break;
                	case "Kompot" :
                		latitude = 10.5940343;
                		longitude = 104.1669084;
                		break;
                	case "Kratie" :
                		latitude = 12.4881022;
                		longitude = 106.0296;
                		break;
                	case "Mondulkiri" :
                		latitude = 12.4782969;
                		longitude = 107.1502804;
                		break;
                	case "Oddor Meanchey" :
                		latitude = 14.2685809;
                		longitude = 103.6067176;
                		break;
                	case "Pailin" :
                		latitude = 12.8366942;
                		longitude = 102.6103735;
                		break;
                	case "Phnom Penh" :
                		latitude = 11.5494709;
                		longitude = 104.8954224;
                		break;
                	case "Preah Vihear" :
                		latitude = 13.7359324;
                		longitude = 104.9765003;
                		break;
                	case "Preah Sikhaknouk" :
                		latitude = 10.6246313;
                		longitude = 103.5250597;
                		break;
                	case "Prey Veng" :
                		latitude = 11.4775742;
                		longitude = 105.336914;
                		break;
                	case "Pursat" :
                		latitude = 12.467624;
                		longitude = 103.9026082;
                		break;
                	case "Ratanakiri" :
                		latitude = 13.7136597;
                		longitude = 107.0324922;
                		break;
                	case "Siem Reap" :
                		latitude = 13.3525742;
                		longitude = 103.8670744;
                		break;
                	case "Steung Treng" :
                		latitude = 13.5768236;
                		longitude = 106.0172939;
                		break;
                	case "Svay Reang" :
                		latitude = 11.0906845;
                		longitude = 105.813827;
                		break;
                	case "Takeo" :
                		latitude = 10.9858673;
                		longitude = 104.8047983;
                		break;
                	case "Thboung Khhmom" :
                		latitude = 11.9296606;
                		longitude = 105.6674051;
                		break;
                	}
                	
                    mapDiv = $("#resume-map");
                    mapDiv.height(600).gmap3({
                        map: {
                            options: {
                                "center": [latitude, longitude],
                                "zoom": 12,
                                "draggable": true,
                                "mapTypeControl": true,
                                "mapTypeId": google.maps.MapTypeId.ROADMAP,
                                "scrollwheel": false,
                                "panControl": true,
                                "rotateControl": false,
                                "scaleControl": true,
                                "streetViewControl": true,
                                "zoomControl": true,
                                "styles": [
                                    {
                                        "featureType": "landscape.natural",
                                        "elementType": "geometry.fill",
                                        "stylers": [
                                            {
                                                "visibility": "on"
                                        },
                                            {
                                                "color": "#e0efef"
                                        }
                                    ]
                                },
                                    {
                                        "featureType": "poi",
                                        "elementType": "geometry.fill",
                                        "stylers": [
                                            {
                                                "visibility": "on"
                                        },
                                            {
                                                "hue": "#1900ff"
                                        },
                                            {
                                                "color": "#c0e8e8"
                                        }
                                    ]
                                },
                                    {
                                        "featureType": "landscape.man_made",
                                        "elementType": "geometry.fill"
                                },
                                    {
                                        "featureType": "road",
                                        "elementType": "geometry",
                                        "stylers": [
                                            {
                                                "lightness": 100
                                        },
                                            {
                                                "visibility": "simplified"
                                        }
                                    ]
                                },
                                    {
                                        "featureType": "road",
                                        "elementType": "labels",
                                        "stylers": [
                                            {
                                                "visibility": "off"
                                        }
                                    ]
                                },
                                    {
                                        "featureType": "water",
                                        "stylers": [
                                            {
                                                "color": "#7dcdcd"
                                        }
                                    ]
                                },
                                    {
                                        "featureType": "transit.line",
                                        "elementType": "geometry",
                                        "stylers": [
                                            {
                                                "visibility": "on"
                                        },
                                            {
                                                "lightness": 700
                                        }
                                    ]
                                }
                            ]
                            }
                        },
                        marker: {
                            values: [
                                {
                                    latLng: [latitude, longitude],
                                    options: {
                                        icon: "http://alexgurghis.com/themes/wpjobus/wp-content/themes/wpjobus/images/icon-services.png",
                                        shadow: "http://alexgurghis.com/themes/wpjobus/wp-content/themes/wpjobus/images/shadow.png",
                                    }
                            }
                        ],
                            options: {
                                draggable: false
                            }
                        }
                    });
                    map = mapDiv.gmap3("get");
                    infobox = new InfoBox({
                        pixelOffset: new google.maps.Size(-50, -65),
                        closeBoxURL: '',
                        enableEventPropagation: true
                    });
                    mapDiv.delegate('.infoBox .close', 'click', function () {
                        infobox.close();
                    });
                    if (Modernizr.touch) {
                        map.setOptions({
                            draggable: false
                        });
                        var draggableClass = 'inactive';
                        var draggableTitle = "Activate map";
                        var draggableButton = $('<div class="draggable-toggle-button ' + draggableClass + '">' + draggableTitle + '</div>').appendTo(mapDiv);
                        draggableButton.click(function () {
                            if ($(this).hasClass('active')) {
                                $(this).removeClass('active').addClass('inactive').text("Activate map");
                                map.setOptions({
                                    draggable: false
                                });
                            } else {
                                $(this).removeClass('inactive').addClass('active').text("Deactivate map");
                                map.setOptions({
                                    draggable: true
                                });
                            }
                        });
                    }
                });
                  
                });
            });
            
            
            /* ProgressBar Animate------------------------------------------------------------------------------------ */
            function progressBarAnimate() {
                jQuery('.main-skills-item-bar-color').bind('inview', function (event, isInView, visiblePartX, visiblePartY) {
                    if (isInView) {
                        // element is now visible in the viewport
                        if (jQuery(this).hasClass('animate')) {

                        } else {
                            jQuery(this).addClass('animate');

                            jQuery(this)
                                .data("origWidth", jQuery(this).width())
                                .width(0)
                                .animate({
                                    width: jQuery(this).data("origWidth")
                                }, 1200);
                        }
                    }
                });
            }
        </script>
        
        <script>
            $(function () {

                var active1 = false;
                var active2 = false;
                var active3 = false;
                var active4 = false;

                $('.parent2').on('mousedown touc0hstart', function (e) {

                    if (!active1) $(this).find('.test1').css({
                        'background-color': '#048853',
                        'transform': 'translate(0px,87.5px)'
                    });
                    else $(this).find('.test1').css({
                        'background-color': '#fff',
                        'transform': 'none'
                    });
                    if (!active2) $(this).find('.test2').css({
                        'background-color': '#048853',
                        'transform': 'translate(-63px,60px)'
                    });
                    else $(this).find('.test2').css({
                        'background-color': '#fff',
                        'transform': 'none'
                    });
                    
                    if (!active3) $(this).find('.test3').css({
                        'background-color': '#048853',
                        'transform': 'translate(-87.5px,0px)'
                    });
                    else $(this).find('.test3').css({
                        'background-color': '#fff',
                        'transform': 'none'
                    });
                    active1 = !active1;
                    active2 = !active2;
                    active3 = !active3;
                });

            });
        </script>


    </body>

    </html>