<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/sql" prefix="sql"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>

<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Advance Search</title>
<link rel="stylesheet" href="css/index.css">
<jsp:include page="includes/head.jsp"></jsp:include>

<!--list developers style, borrow from index -->
<link rel="stylesheet" href="css/index_dev_list.css" type="text/css">
<link rel="stylesheet" href="css/responsive.css" type="text/css">
<!-- dropdown list style  -->
<link href="dropdownlist/sumoselect.css" rel="stylesheet" />
<style>
section.panel {
	margin-bottom: 0px !important;
}
</style>
</head>

<body ng-app="myApp" ng-controller="devCtrl" style="background: gray;">

	<!-- Navigation panel -->
	<jsp:include page="includes/header.jsp"></jsp:include>
	<!-- End Navigation panel -->

	<!-- job-search Section -->
	<section class="page-section" id="job-search"
		style="padding-bottom: 30px;">
	<div class="container relative">
		<!-- Row -->
		<div class="row">
			<!-- Col -->
			<div class="col-sm-12">
				<div class="home-content">
					<div class="home-text">
						<h2 class="hs-line-16 font-alt mb-16 mb-xs-30">WELCOME TO IT
							GURU Advance Search !</h2>

						<h2 class="hs-line-8 mb-30 mb-xs-30">Find the best employee
							in your dream here...</h2>
					</div>
				</div>
			</div>
			<!-- End Col -->
		</div>
		<!-- End Row -->
	</div>
	<!-- End container --> </section>
	<!-- End job-search Section -->

	<!-- body Section -->
	<section class="panel">
	<div class="container">
		<!-- Advance search -->
		<div class="panel-group">
			<div class="row">
				<br>
				<br>
			</div>
			<div class="panel panel-success">
				<div class="panel-heading"
					style="background: #50A253; color: white; font-size: 16px;">IT
					GURU Advance Search</div>
				<div class="panel-body ">
					<div class="panel-body">
						<form role="form" id="frmadv_search" ng-submit="loadData()">
							<fieldset style="height: 100px;">
								<legend>Education</legend>
								<div class="row">
									<div class="col-sm-6">
										<div class="form-group">
											<select id="degree" name="degree" class="SlectBox"
												placeholder="Degree/Level">
												<option value="">Any Degree</option>
												<option value="Associate">Associate</option>
												<option value="Bachelor">Bachelor</option>
												<option value="Master">Master</option>
												<option value="PhD">PhD</option>
												<option value="Doctorate">Doctorate</option>
											</select>
										</div>
									</div>
									<div class="col-sm-6">
										<div class="form-group">
											<select class="SlectBox" placeholder="Major" id="major"
												name="major">
												<option value="">Any Major</option>
											</select>
										</div>
									</div>
								</div>
							</fieldset>
							<fieldset style="height: 100px;">
								<legend>Experience</legend>
								<div class="row">
									<div class="col-sm-3">
										<div class="form-group">
											<select class="form-control"
												placeholder="Position/Responsibility" id="position"
												name="position">
												<option value="">Any Position</option>
											</select>
										</div>
									</div>
									<div class="col-sm-1">
										<div class="form-group">
											<div class="mb-20 mb-md-10">
												<div class="radio">
													<label> <input type="radio" name="experience"
														id="experience2" value="1-99" checked="checked">
														Any
													</label>
												</div>
											</div>
										</div>
									</div>
									<div class="col-sm-2">
										<div class="form-group">
											<div class="mb-20 mb-md-10">
												<div class="radio">
													<label> <input type="radio" name="experience"
														id="experience0" value="1-2"> 1-2 Years
													</label>
												</div>
											</div>
										</div>
									</div>
									<div class="col-sm-2">
										<div class="form-group">
											<div class="mb-20 mb-md-10">
												<div class="radio">
													<label> <input type="radio" name="experience"
														id="experience" value="3-5"> 3-5 Years
													</label>
												</div>
											</div>
										</div>
									</div>
									<div class="col-sm-2">
										<div class="form-group">
											<div class="mb-20 mb-md-10">
												<div class="radio">
													<label> <input type="radio" name="experience"
														id="experience1" value="6-10"> 6-10 Years
													</label>
												</div>
											</div>
										</div>
									</div>
									<div class="col-sm-2">
										<div class="form-group">
											<div class="mb-20 mb-md-10">
												<div class="radio">
													<label> <input type="radio" name="experience"
														id="experience1" value="11-99"> 10+ Years
													</label>
												</div>
											</div>
										</div>
									</div>
								</div>
							</fieldset>
							<fieldset style="height: 100px;">
								<legend>Skill</legend>
								<div class="row">
									<div class="col-sm-12" style="padding: 0px;">
										<div class="col-sm-6">
											<div class="form-group">
												<select class="SlectBox" placeholder="Skill Categories"
													id="category" name="category">
													<option value="">Any Category</option>
												</select>
											</div>
										</div>

										<div class="col-sm-6">
											<div class="form-group">
												<select multiple="multiple" class="SlectBox"
													placeholder="Skills" id="skill" name="skill">
												</select>
											</div>
										</div>
									</div>
								</div>
							</fieldset>

							<fieldset style="height: 100px;">
								<legend>Language</legend>
								<div class="row">
									<div class="col-sm-12" style="padding: 0px;">
										<div class="col-sm-6">
											<div class="form-group">
												<select multiple="multiple" class="SlectBox"
													placeholder="Languages" id="language" name="language">
												</select>
											</div>
										</div>
									</div>
								</div>
							</fieldset>

							<fieldset style="height: 100px;">
								<legend>Expected Salary Range</legend>
								<div class="row">
									<div class="col-sm-2">
										<div class="form-group">
											<div class="mb-20 mb-md-10">
												<div class="radio">
													<label> <input type="radio" name="salary"
														id="salary" value="100-10000" checked="checked">
														Any
													</label>
												</div>
											</div>
										</div>
									</div>
									<div class="col-sm-2">
										<div class="form-group">
											<div class="mb-20 mb-md-10">
												<div class="radio">
													<label> <input type="radio" name="salary"
														id="salary" value="100-300"> 100-300 USD
													</label>
												</div>
											</div>
										</div>
									</div>
									<div class="col-sm-2">
										<div class="form-group">
											<div class="mb-20 mb-md-10">
												<div class="radio">
													<label> <input type="radio" name="salary"
														id="salary1" value="301-500"> 300-500 USD
													</label>
												</div>
											</div>
										</div>
									</div>
									<div class="col-sm-2">
										<div class="form-group">
											<div class="mb-20 mb-md-10">
												<div class="radio">
													<label> <input type="radio" name="salary"
														id="salary2" value="501-10000"> 500+ USD
													</label>
												</div>
											</div>
										</div>
									</div>

									<div class="col-sm-2" id="result">
										<div class="form-group">
											<div class="mb-20 mb-md-10">
												<button type="submit"
													class="btn btn-mod btn-round btn-medium"
													style="background: #50A253;" id="btnSearch">Search</button>
											</div>
										</div>
									</div>
								</div>
							</fieldset>
						</form>
					</div>
				</div>
			</div>
		</div>

		<!-- Search Result  -->
		<div class="row">
			<div class="col-md-12">

				<!-- box listing  -->
				<div class="block-section-sm box-list-area">

					<!-- top desc -->
					<div class="row hidden-xs" ng-show="filteredItems >= 0">
						<div class="col-sm-12 ">
							<h2 class="title uppercase"
								style="text-transform: bold; color: #50A253; padding: 0; margin: 0 0 15px 0">Search
								Results Total: {{devs.length}}</h2>
						</div>
					</div>

					<!-- end top desc -->
					<div class="resume-skills homepage-recent-listings">
						<div id="tabs" class="full">

							<div class="pane" style="display: block;">
								<div class="full">
									<ul id="companies-block-list-ul">
										<li id="{{$index+1}}" class="animate"
											ng-show="devs.length > 0"
											ng-repeat="dev in filtered= devs | startFrom:(currentPage-1)*entryLimit | limitTo:entryLimit">
											<a ng-click="myff(dev.id)">
												<div class="company-holder-block">
													<span class="company-list-icon rounded-img"><img
														src="images/uploads/developer/{{dev.avatar}}"
														alt="{{dev.avatar}}"></span> <span
														class="company-list-name-block" style="max-width: 380px;">
														<span class="company-list-name">{{dev.fname + ' ' +
															dev.lname}} <span class="resume-prof-title">{{dev.specialty_skill}}</span>
													</span> <span class="company-list-location"><span
															class="resume_job_1"><i class="fa fa-map-marker"></i>{{dev.address}}</span>
													</span>
													</span> <span class="company-list-view-profile"> <span
														class="company-view-profile"> <span
															class="company-view-profile-title-holder"> <span
																class="company-view-profile-title">View</span> <span
																class="company-view-profile-subtitle">Resume</span>
														</span> <i class="fa fa-eye"></i>
													</span>
													</span> <span class="company-list-badges"
														style="margin-top: 19px;"> <span
														class="job-offers-post-badge"
														style="max-width: 220px; background-color: #50A253; border: solid 2px #50A253;">
															<span class="job-offers-post-badge-job-type"
															style="width: 110px; color: #50A253; line-height: 16px; padding-top: 9px; text-align: right;">{{dev.experiences}}
																Years Experience</span> <span
															class="job-offers-post-badge-amount">$
																{{dev.salary}}</span> <span
															class="job-offers-post-badge-amount-per">/Month</span>
													</span>
													</span>
												</div>
										</a>
										</li>
									</ul>
								</div>
							</div>
						</div>
						<div class="col-md-12" ng-show="filteredItems == 0">
							<div class="col-md-12">
								<h2 class="text-center">No Results found</h2>
							</div>
						</div>
						<div class="col-md-12" ng-show="filteredItems > 0">
							<div pagination="" page="currentPage" max-size="10"
								on-select-page="setPage(page)" boundary-links="true"
								total-items="filteredItems" items-per-page="entryLimit"
								class="pagination-small" previous-text="&lt;&lt;"
								next-text="&gt;&gt;"></div>
						</div>
					</div>
					<div class="clearfix"></div>
				</div>
				<!-- end  box listing  -->
			</div>
		</div>
	</div>
	</section>
	<!-- End body Section -->

	<!-- Footer Section -->
	<jsp:include page="includes/footer.jsp"></jsp:include>
	<!-- End Footer Section -->

	<!-- JS -->
	<jsp:include page="includes/script.jsp"></jsp:include>

	<!-- dropdown list -->
	<script src="dropdownlist/jquery.sumoselect.js"></script>
	<script type="text/javascript">
		
		 // this is the id of the form
		$("#myModal123 form").submit(function() {
			$.ajax({
				type : "POST",
				url : 'userIn.hrd',
				data : $("#myModal123 form").serialize(), // serializes all the form's elements.
				success : function(data) {
					if (data != "fail") {
						if (data.uri == "") {
							if (data.role == "0")
								//location.replace("admin/index.jsp"); can't back
								location.href = "admin";
							else
								location.reload();
						} else {
							location.href = data.uri;
						}
					} else {
						$('#myModal123 .modal-body .alert').show();
						$('#myModal123').on('hidden.bs.modal', function() {
							$('#myModal123 .modal-body .alert').hide();
							$('#username').val("");
							$('#password').val("");
						});
					}
				}
			});

			return false; // avoid to execute the actual submit of the form.
		});
		 
			var fields = {};

			//TODO: ADD NEW BRAND
			fields.listDataToField = function() {
				$.ajax({
					url : "listAdvancedSearchField.hrd",
					type : "POST",
					dataType : "JSON",
					success : function(data) {
						if (data[0].major.length > 0) {
							$.each(data[0].major, function(index, value) {
								$('#major').append(
										$("<option></option>").attr("value",
												value).text(value));
							});
						}

						if (data[4].position.length > 0) {
							$.each(data[4].position, function(index, value) {
								$('#position').append(
										$("<option></option>").attr("value",
												value).text(value));
							});
						}

						if (data[2].category.length > 0) {
							$.each(data[2].category, function(index, value) {
								$('#category').append(
										$("<option></option>").attr("value",
												value).text(value));
							});

						}
						if (data[3].subject.length > 0) {
							$.each(data[3].subject, function(index, value) {
								$('#skill').append(
										$("<option></option>").attr("value",
												value).text(value));
							});
						}

						if (data[1].language.length > 0) {
							$.each(data[1].language, function(index, value) {
								$('#language').append(
										$("<option></option>").attr("value",
												value).text(value));
							});
						}
						
						fields.runSlectBox();
					},
					error : function(data) {
						console.log(data);
					}
				});
			};

			fields.listDataToField();

			fields.runSlectBox = function(){
				$('.SlectBox').SumoSelect({
					csvDispCount : 4
				});
				$('#btnSelected').click(function() {
					var selected = $(".SlectBox option:selected");
					var message = "";
					selected.each(function() {
						message += $(this).val() + "\n";
					});
				});
			};
		</script>


	<script src="admin/js/angular.min.js"></script>
	<script src="js/ui-bootstrap-tpls-0.10.0.min.js"></script>
	<script src="js/adv_search.js"></script>
</body>
</html>
