package utilities;

import java.sql.ResultSet;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.json.JSONArray;
import org.json.JSONObject;

public class Convertor {
public static java.sql.Date utilToSQLDate(java.util.Date u) throws ParseException{
	   return new java.sql.Date(u.getTime());
}
public static java.util.Date stringToDate(String date) throws ParseException{
	SimpleDateFormat sd=new SimpleDateFormat("dd-MM-yyyy");
	return sd.parse(date);
	
}
public static String convertResultSetIntoJSON(ResultSet resultSet) throws Exception {
    JSONArray jsonArray = new JSONArray();
    while (resultSet.next()) {
        int total_rows = resultSet.getMetaData().getColumnCount();
        JSONObject obj = new JSONObject();
        for (int i = 0; i < total_rows; i++) {
            String columnName = resultSet.getMetaData().getColumnLabel(i + 1).toLowerCase();
            Object columnValue = resultSet.getObject(i + 1);
            // if value in DB is null, then we set it to default value
            if (columnValue == null){
                columnValue = "null";
            }
            /*
            Next if block is a hack. In case when in db we have values like price and price1 there's a bug in jdbc - 
            both this names are getting stored as price in ResulSet. Therefore when we store second column value,
            we overwrite original value of price. To avoid that, i simply add 1 to be consistent with DB.
             */
            if (obj.has(columnName)){
                columnName += "1";
            }
            obj.put(columnName, columnValue);
        }
        jsonArray.put(obj);
    }
    return jsonArray.toString();
}

public static String convertListResultSetIntoJSON(List<ResultSet> listResultSet) throws Exception {
	JSONArray jsonArray = new JSONArray();
	
	for (ResultSet resultSet : listResultSet) {
		ArrayList<Object> listValue = new ArrayList<Object>();
        String columnName = "";
        JSONObject obj = new JSONObject();
		while (resultSet.next()) {
	        int total_rows = resultSet.getMetaData().getColumnCount();
	        for (int i = 0; i < total_rows; i++) {
	            columnName = resultSet.getMetaData().getColumnLabel(i + 1).toLowerCase();
	            Object columnValue = resultSet.getObject(i + 1);
	            // if value in DB is null, then we set it to default value
	            if (columnValue == null){
	                columnValue = "null";
	            }
	            /*
	            Next if block is a hack. In case when in db we have values like price and price1 there's a bug in jdbc - 
	            both this names are getting stored as price in ResulSet. Therefore when we store second column value,
	            we overwrite original value of price. To avoid that, i simply add 1 to be consistent with DB.
	             */
	            
	            listValue.add(columnValue);
	           
	        }
	        if (obj.has(columnName)){
                columnName += "1";
            }
           
	    }
		 obj.put(columnName, listValue);
	     jsonArray.put(obj);
	}
    return jsonArray.toString();
}

public static void main(String[] args) throws Exception {
	//System.out.println(Convertor.convertResultSetIntoJSON(new ListCategory().listCategoriesRS()));
}


}

