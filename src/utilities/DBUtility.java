package utilities;
import java.sql.Connection;
import java.sql.DriverManager;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Properties;

public class DBUtility {
	public Connection getConnection(){
		Connection c = null;
		Properties props=null;
	    try {
	    	Class.forName("org.postgresql.Driver");
	    	props=new Properties();
	    	props.put("user", "postgres");
	    	props.put("password", "123");

	    	c = DriverManager.getConnection("jdbc:postgresql://localhost:5432/dev_community", props);
	       // c = DriverManager.getConnection("jdbc:postgresql://ec2-54-83-0-61.compute-1.amazonaws.com:5432/d1d17c4102c719", props);
	        return c;
	    } catch (Exception e) {
	        System.out.println("Connection not found !");
	    }
	    return null;
	}
	public static void main(String[] args) {
		DBUtility db = new DBUtility();
		db.getConnection();
		
	}
}

 