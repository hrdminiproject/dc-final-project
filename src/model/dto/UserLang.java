package model.dto;

public class UserLang {
	private int id, lid, understandingLevel, speakingLevel, writingLevel;
	private String language;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getLid() {
		return lid;
	}

	public void setLid(int lid) {
		this.lid = lid;
	}

	public int getUnderstandingLevel() {
		return understandingLevel;
	}

	public void setUnderstandingLevel(int understandingLevel) {
		this.understandingLevel = understandingLevel;
	}

	public int getSpeakingLevel() {
		return speakingLevel;
	}

	public void setSpeakingLevel(int speakingLevel) {
		this.speakingLevel = speakingLevel;
	}

	public int getWritingLevel() {
		return writingLevel;
	}

	public void setWritingLevel(int writingLevel) {
		this.writingLevel = writingLevel;
	}

	public String getLanguage() {
		return language;
	}

	public void setLanguage(String language) {
		this.language = language;
	}
}
