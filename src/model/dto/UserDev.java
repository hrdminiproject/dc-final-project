package model.dto;

public class UserDev {
	private int id;
	private String username;
	private String password;
	private String fname;
	private String lname;
	private String email;
	private String gender;
	private String type;
	private String status;
	private String pob;
	private String dob;
	private String address;
	private String phone;
	private String nationality;
	private String avatar;
	private String marital;
	private double salary;
	private String currentCityState;
	private String specialtySkill;
	private String wantedPosition;
	private String motivation;
	
	public String getWantedPosition() {
		return wantedPosition;
	}
	public void setWantedPosition(String wantedPosition) {
		this.wantedPosition = wantedPosition;
	}
	public String getMotivation() {
		return motivation;
	}
	public void setMotivation(String motivation) {
		this.motivation = motivation;
	}
	public String getSpecialtySkill() {
		return specialtySkill;
	}
	public void setSpecialtySkill(String specialtySkill) {
		this.specialtySkill = specialtySkill;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getFname() {
		return fname;
	}
	public void setFname(String fname) {
		this.fname = fname;
	}
	public String getLname() {
		return lname;
	}
	public void setLname(String lname) {
		this.lname = lname;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getGender() {
		return gender;
	}
	public void setGender(String gender) {
		this.gender = gender;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getPob() {
		return pob;
	}
	public void setPob(String pob) {
		this.pob = pob;
	}
	public String getDob() {
		return dob;
	}
	public void setDob(String dob) {
		this.dob = dob;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getPhone() {
		return phone;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}
	public String getNationality() {
		return nationality;
	}
	public void setNationality(String nationality) {
		this.nationality = nationality;
	}
	public String getAvatar() {
		return avatar;
	}
	public void setAvatar(String avatar) {
		this.avatar = avatar;
	}
	public String getMarital() {
		return marital;
	}
	public void setMarital(String marital) {
		this.marital = marital;
	}
	public double getSalary() {
		return salary;
	}
	public void setSalary(double salary) {
		this.salary = salary;
	}
	public String getCurrentCityState() {
		return currentCityState;
	}
	public void setCurrentCityState(String currentCityState) {
		this.currentCityState = currentCityState;
	}
	
	
}
