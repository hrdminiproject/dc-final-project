package model.dto;

public class Developers {
	private int id;
	private String pob;
	private String dob;
	private String current_city_state;
	private String address;
	private String phone;
	private String nationality;
	private String avatar;
	private String marital;
	private double salary;
	private String specialtySkill;
	private String currentCityState;
	private String wantedPosition;
	private String motivation;

	public String getWantedPosition() {
		return wantedPosition;
	}

	public void setWantedPosition(String wantedPosition) {
		this.wantedPosition = wantedPosition;
	}

	public String getMotivation() {
		return motivation;
	}

	public void setMotivation(String motivation) {
		this.motivation = motivation;
	}

	private boolean isApproved;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getPob() {
		return pob;
	}

	public void setPob(String pob) {
		this.pob = pob;
	}

	public String getDob() {
		return dob;
	}

	public void setDob(String string) {
		this.dob = string;
	}

	public String getCurrent_city_state() {
		return current_city_state;
	}

	public void setCurrent_city_state(String current_city_state) {
		this.current_city_state = current_city_state;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getNationality() {
		return nationality;
	}

	public void setNationality(String nationality) {
		this.nationality = nationality;
	}

	public String getAvatar() {
		return avatar;
	}

	public void setAvatar(String avatar) {
		this.avatar = avatar;
	}

	public String getMarital() {
		return marital;
	}

	public void setMarital(String marital) {
		this.marital = marital;
	}

	public double getSalary() {
		return salary;
	}

	public void setSalary(double salary) {
		this.salary = salary;
	}

	public String getSpecialtySkill() {
		return specialtySkill;
	}

	public void setSpecialtySkill(String specialtySkill) {
		this.specialtySkill = specialtySkill;
	}

	public String getCurrentCityState() {
		return currentCityState;
	}

	public void setCurrentCityState(String currentCityState) {
		this.currentCityState = currentCityState;
	}

	public boolean isApproved() {
		return isApproved;
	}

	public void setApproved(boolean isApproved) {
		this.isApproved = isApproved;
	}
}
