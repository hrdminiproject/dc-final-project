package model.dto;

public class DevLanguages {
	private int devid;
	private int lid;
	private int understandingLevel, speakingLevel, writingLevel;
	private String description;
	
	public int getDevid() {
		return devid;
	}
	public void setDevid(int devid) {
		this.devid = devid;
	}
	public int getLid() {
		return lid;
	}
	public void setLid(int lid) {
		this.lid = lid;
	}
	public int getUnderstandingLevel() {
		return understandingLevel;
	}
	public void setUnderstandingLevel(int understandingLevel) {
		this.understandingLevel = understandingLevel;
	}
	public int getSpeakingLevel() {
		return speakingLevel;
	}
	public void setSpeakingLevel(int speakingLevel) {
		this.speakingLevel = speakingLevel;
	}
	public int getWritingLevel() {
		return writingLevel;
	}
	public void setWritingLevel(int writingLevel) {
		this.writingLevel = writingLevel;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	
	
}
