package model.dao.changepwd;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import model.dto.Users;
import utilities.DBUtility;

public class UpdateChangePwd {
	Connection con;
	// get connection when create object
		public UpdateChangePwd() throws Exception {
			DBUtility utility = new DBUtility();
			con = utility.getConnection();
			System.out.println("Connecting to database...");
		}
		
	public boolean updatechangepassword(Users user, String newPwd){
		PreparedStatement ps = null;
		
		try {
			ps=con.prepareStatement("SELECT password FROM users WHERE id =?");
			ps.setInt(1, user.getId());
			ResultSet rs= ps.executeQuery();
			if(rs.next()){
				if(rs.getString("password").equals(user.getPassword())){
					PreparedStatement ps1=null;
					try {
						ps1 = con.prepareStatement("UPDATE users SET password = ? WHERE id = ?");
						ps1.setString(1, newPwd);
						ps1.setInt(2, user.getId());
						
						if(ps1.executeUpdate() > 0)
							return true;
					} catch (Exception e) {
						e.printStackTrace();
					}
					finally{
						try {
							ps1.close();
						} catch (SQLException e) {
							e.printStackTrace();
						}
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				ps.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return false;
	}
	
}
