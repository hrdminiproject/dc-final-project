package model.dao.devlanguage;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import model.dto.DevLanguages;
import model.dto.DevSubjects;
import utilities.DBUtility;

public class UpdateDevLanguage {
	Connection con;
	
	//get connection when create object
	public UpdateDevLanguage() throws Exception{
		DBUtility utility = new DBUtility();
		con = utility.getConnection();
		System.out.println("Connecting to database...");
	}
	
	public boolean udpateDevLanguages (DevLanguages dev_language){
		PreparedStatement ps = null;
		
		try{
			ps = con.prepareStatement("UPDATE dev_language SET understanding_level = ?, speaking_level = ?, writing_level = ?, description = ?, status ='f' WHERE dev_id = ? AND lid = ?");
			ps.setInt(1,  dev_language.getUnderstandingLevel());
			ps.setInt(2,  dev_language.getSpeakingLevel());
			ps.setInt(3,  dev_language.getWritingLevel());
			ps.setString(4, dev_language.getDescription());
			ps.setInt(5,  dev_language.getDevid());
			ps.setInt(6, dev_language.getLid());

			
			if (ps.executeUpdate() > 0)
				return true;
		} catch (Exception e){
			e.printStackTrace();
		} finally {
			try{
				ps.close();
			} catch (SQLException e){
				e.printStackTrace();
			}
		}
		return false;
	}
}
