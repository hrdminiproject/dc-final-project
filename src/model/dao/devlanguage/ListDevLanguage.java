package model.dao.devlanguage;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.PreparedStatement;
import utilities.DBUtility;

public class ListDevLanguage {
	Connection con;
	
	// get connection when create object
	public ListDevLanguage() throws Exception {
		DBUtility utility = new DBUtility();
		con = utility.getConnection();
		System.out.println("Connecting to database...");
	}
	
	public ResultSet listDevLanguages(int did) throws Exception{
		
		PreparedStatement ps = con.prepareStatement("SELECT * FROM v_dev_language WHERE id=? ORDER BY language asc");
		ps.setInt(1, did);
		
		ResultSet rs = null;
		if ((rs=ps.executeQuery()) != null) return rs;
		if (ps != null) try {ps.close();} catch (SQLException e) {throw e;}
		if (con != null) try {con.close();} catch (SQLException e) {throw e;}
		return null;
	}
}
