package model.dao.devlanguage;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import utilities.DBUtility;

public class GetDevLanguageRecord {
	Connection con;
	// get connection when create object
		public GetDevLanguageRecord() throws Exception {
			DBUtility utility = new DBUtility();
			con = utility.getConnection();
			System.out.println("Connecting to database...");
		}
		
		public ResultSet listDevLanguageEdit(int did, int lid) throws Exception{
			PreparedStatement ps = con.prepareStatement("SELECT * FROM dev_language where dev_id = ? AND lid= ?");
			ps.setInt(1, did);
			ps.setInt(2, lid);
			ResultSet rs = null;
			if ((rs=ps.executeQuery()) != null) return rs;
			if (ps != null) try {ps.close();} catch (SQLException e) {throw e;}
			if (con != null) try {con.close();} catch (SQLException e) {throw e;}
			return null;
		}
}
