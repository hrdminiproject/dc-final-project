package model.dao.devlanguage;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.PreparedStatement;
import utilities.DBUtility;

public class DeleteDevLanguage {
	Connection con;
	
	// get connection when create object
	public DeleteDevLanguage() throws Exception {
		DBUtility utility = new DBUtility();
		con = utility.getConnection();
		System.out.println("Connecting to database...");
	}
	
	public boolean deleteDevLanguages(int devid, int lid){
		PreparedStatement ps = null;
		try{
			ps = con.prepareStatement("DELETE FROM dev_language WHERE dev_id = ? AND lid = ?");
			ps.setInt(1, devid);
			ps.setInt(2, lid);
			if (ps.executeUpdate() > 0)
				return true;
		}catch (Exception e){
			e.printStackTrace();
		} finally {
			try {
				ps.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	return false;
	}
}
