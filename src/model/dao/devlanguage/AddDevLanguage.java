package model.dao.devlanguage;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import model.dto.DevLanguages;
import utilities.DBUtility;

public class AddDevLanguage {
	Connection con;
	
	// get connection when create object
	public AddDevLanguage() throws Exception {
		DBUtility utility = new DBUtility();
		con = utility.getConnection();
		System.out.println("Connecting to database...");
	}
	
	public boolean addDevLanguages(DevLanguages dev_language){
		PreparedStatement ps = null;
		try {
			ps = con.prepareStatement("INSERT INTO dev_language VALUES(?, ?, ?, ?, ?, ?, 'f')");
			ps.setInt(1, dev_language.getDevid());
			ps.setInt(2,  dev_language.getLid());
			ps.setString(3, dev_language.getDescription());
			ps.setInt(4, dev_language.getUnderstandingLevel());
			ps.setInt(5, dev_language.getSpeakingLevel());
			ps.setInt(6, dev_language.getWritingLevel());
			if (ps.executeUpdate() > 0)
				return true;
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				ps.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return false;
	}	
}
