package model.dao.education;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import utilities.DBUtility;

public class ListAllDevInEducation {
	Connection con;
	
	// get connection when create object
	public ListAllDevInEducation() throws Exception {
		DBUtility utility = new DBUtility();
		con = utility.getConnection();
		System.out.println("Connecting to database...");
	}
	
	public ResultSet listDevInEducation() throws Exception{
		
		PreparedStatement ps = con.prepareStatement("SELECT degree deg, count(DISTINCT (dev_id)) edu FROM educations WHERE dev_id IN (SELECT id FROM developers WHERE is_approved='approved') group by degree order by degree desc");
		ResultSet rs = null;
		if ((rs=ps.executeQuery()) != null) return rs;
		if (ps != null) try {ps.close();} catch (SQLException e) {throw e;}
		if (con != null) try {con.close();} catch (SQLException e) {throw e;}
		return null;
	}
}
