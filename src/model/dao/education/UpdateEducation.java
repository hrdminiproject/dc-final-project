package model.dao.education;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import model.dto.Education;
import utilities.DBUtility;

public class UpdateEducation {
	Connection con;
	// get connection when create object
		public UpdateEducation() throws Exception {
			DBUtility utility = new DBUtility();
			con = utility.getConnection();
			System.out.println("Connecting to database...");
		}
		
	public boolean updateEducation(Education edu){
		PreparedStatement ps = null;
		
		try {
			ps = con.prepareStatement("UPDATE educations SET school = ?, "
							+ "degree = ?, major = ?, current_year = ?, "
							+ "start_year = ?, end_year = ?, description = ?, image_url = ?, location = ? WHERE id = ?");
			ps.setString(1, edu.getSchool());
			ps.setString(2, edu.getDegree());
			ps.setString(3, edu.getMajor());
			ps.setString(4, edu.getCurrent_year());
			ps.setString(5, edu.getStart_year());
			ps.setString(6, edu.getEnd_year());
			ps.setString(7, edu.getDescription());
			ps.setString(8, edu.getImage_url());
			ps.setString(9, edu.getLocation());
			ps.setInt(10,edu.getId());
			
			if (ps.executeUpdate() > 0)
				return true;
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				ps.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return false;
	}
}
