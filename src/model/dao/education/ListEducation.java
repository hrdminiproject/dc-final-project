package model.dao.education;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import model.dto.Education;
import utilities.DBUtility;

public class ListEducation {
	Connection con;
	// get connection when create object
	public ListEducation() throws Exception{
		DBUtility utility = new DBUtility();
		con = utility.getConnection();
		System.out.println("Connecting to database...");
	}
	
	public ArrayList<Education> listEducation(int dev_id) throws Exception{
		PreparedStatement ps = con.prepareStatement("SELECT * FROM educations WHERE dev_id=? ORDER BY start_year asc");
		ps.setInt(1, dev_id);
		ResultSet rs = ps.executeQuery();
		ArrayList<Education> education = new ArrayList<Education>();
		try{
			while(rs.next()){
				Education edu = new Education();
				edu.setId(rs.getInt("id"));
				edu.setSchool(rs.getString("school"));
				edu.setDegree(rs.getString("degree"));
				edu.setMajor(rs.getString("major"));
				edu.setCurrent_year(rs.getString("current_year"));
				edu.setStart_year(rs.getString("start_year"));
				edu.setEnd_year(rs.getString("end_year"));
				edu.setDescription(rs.getString("description"));
				edu.setImage_url(rs.getString("image_url"));
				edu.setDevid(rs.getInt("dev_id"));
				education.add(edu);
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			if (rs != null) try {rs.close();} catch (SQLException e) {throw e;}
			if (ps != null) try {ps.close();} catch (SQLException e) {throw e;}
			if (con != null) try {con.close();} catch (SQLException e) {throw e;}
		}
		return education;
	}
	
	public  ArrayList<Education> getEducation(int id) throws SQLException{
		
		PreparedStatement ps=con.prepareStatement("select * from educations where id=?");
		ps.setInt(1,id);
		ResultSet rs=ps.executeQuery();
		ArrayList<Education> education = new ArrayList<Education>();
		try{
			while(rs.next()){
				Education edu = new Education();
				edu.setId(rs.getInt("id"));
				edu.setSchool(rs.getString("school"));
				edu.setDegree(rs.getString("degree"));
				edu.setMajor(rs.getString("major"));
				edu.setStart_year(rs.getString("start_year"));
				edu.setEnd_year(rs.getString("end_year"));
				edu.setCurrent_year(rs.getString("current_year"));
				edu.setDescription(rs.getString("description"));
				edu.setImage_url(rs.getString("image_url"));
				edu.setLocation(rs.getString("location"));
				education.add(edu);
			}		
			return education;
		}catch(Exception e){
			e.printStackTrace();
		}
		finally {
			if (rs != null) try {rs.close();} catch (SQLException e) {throw e;}
			if (ps != null) try {ps.close();} catch (SQLException e) {throw e;}
			if (con != null) try {con.close();} catch (SQLException e) {throw e;}
		}
		return null;
	}
}
