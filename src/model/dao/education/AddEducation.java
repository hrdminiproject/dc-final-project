package model.dao.education;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import model.dto.Education;
import utilities.DBUtility;

public class AddEducation {
Connection con;
	
	// get connection when create object
	public AddEducation() throws Exception {
		DBUtility utility = new DBUtility();
		con = utility.getConnection();
	}
	
	public boolean addEducation(Education edu){
		PreparedStatement ps = null;
		try {
			ps = con.prepareStatement("INSERT INTO educations (school, degree, major, current_year, start_year, end_year, image_url, description, dev_id, location) "
										+ "VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)");
			ps.setString(1, edu.getSchool());
			ps.setString(2, edu.getDegree());
			ps.setString(3, edu.getMajor());
			ps.setString(4, edu.getCurrent_year());
			ps.setString(5, edu.getStart_year());
			ps.setString(6, edu.getEnd_year());
			ps.setString(7, edu.getImage_url());
			ps.setString(8, edu.getDescription());
			ps.setInt(9, edu.getDevid());
			ps.setString(10, edu.getLocation());
			
			if (ps.executeUpdate() > 0)
				return true;
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try { ps.close(); } catch (SQLException e) { e.printStackTrace();	}
		}
		return false;
	}
}
