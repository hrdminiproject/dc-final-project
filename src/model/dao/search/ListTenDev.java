package model.dao.search;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import utilities.Convertor;
import utilities.DBUtility;

public class ListTenDev {
	Connection con;
	public ListTenDev() throws Exception{
		DBUtility utility = new DBUtility();
		con = utility.getConnection();
		System.out.println("Connecting to database...");
	}
	
	public String listTenDev() throws Exception{
		PreparedStatement ps = con.prepareStatement("SELECT * FROM v_ten_dev WHERE is_approved='approved'");
		ResultSet rs = null;
		String str="";
		if ((rs=ps.executeQuery()) != null){
			str = Convertor.convertResultSetIntoJSON(rs);
		}
		if (rs != null) try {rs.close();} catch (SQLException e) {throw e;}
		if (ps != null) try {ps.close();} catch (SQLException e) {throw e;}
		if (con != null) try {con.close();} catch (SQLException e) {throw e;}
		
		return str;
	}

}
