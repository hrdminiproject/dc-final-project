package model.dao.search;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import utilities.DBUtility;

public class ListAdvancedSearchField {
	Connection con;
	public ListAdvancedSearchField() throws Exception {
		DBUtility utility = new DBUtility();
		con = utility.getConnection();
		System.out.println("Connecting to database...");
	}
	
	public ResultSet listMajor() throws Exception{
		PreparedStatement ps = con.prepareStatement("SELECT distinct(major) as major from educations ORDER BY major ASC");
		ResultSet rs=null;
		if ((rs=ps.executeQuery()) != null) return rs;
		if (ps != null) try {ps.close();} catch (SQLException e) {throw e;}
		if (con != null) try {con.close();} catch (SQLException e) {throw e;}
		return null;
	}
	
	public ResultSet listLanguage() throws Exception{
		PreparedStatement ps = con.prepareStatement("SELECT distinct(language) as language from languages ORDER BY language ASC");
		ResultSet rs=null;
		if ((rs=ps.executeQuery()) != null) return rs;
		if (ps != null) try {ps.close();} catch (SQLException e) {throw e;}
		if (con != null) try {con.close();} catch (SQLException e) {throw e;}
		return null;
	}
	
	public ResultSet listCategorie() throws Exception{
		PreparedStatement ps = con.prepareStatement("SELECT name as category from categories ORDER BY name ASC");
		ResultSet rs=null;
		if ((rs=ps.executeQuery()) != null) return rs;
		if (ps != null) try {ps.close();} catch (SQLException e) {throw e;}
		if (con != null) try {con.close();} catch (SQLException e) {throw e;}
		return null;
	}
	
	public ResultSet listSubject() throws Exception{
		PreparedStatement ps = con.prepareStatement("SELECT name as subject from subjects ORDER BY name ASC");
		ResultSet rs=null;
		if ((rs=ps.executeQuery()) != null) return rs;
		if (ps != null) try {ps.close();} catch (SQLException e) {throw e;}
		if (con != null) try {con.close();} catch (SQLException e) {throw e;}
		return null;
	}
	
	public ResultSet listPosition() throws Exception{
		PreparedStatement ps = con.prepareStatement("SELECT distinct(position) as position from experiences ORDER BY position ASC");
		ResultSet rs=null;
		if ((rs=ps.executeQuery()) != null) return rs;
		if (ps != null) try {ps.close();} catch (SQLException e) {throw e;}
		if (con != null) try {con.close();} catch (SQLException e) {throw e;}
		return null;
	}
}
