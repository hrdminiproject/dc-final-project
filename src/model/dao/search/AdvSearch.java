package model.dao.search;

import java.sql.Array;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Arrays;

import utilities.DBUtility;

public class AdvSearch {
	Connection con;
	public AdvSearch() throws Exception{
		DBUtility utility = new DBUtility();
		con = utility.getConnection();
		System.out.println("Connecting to database...");
	}
	
	public ResultSet search(String degree, String major, String position, String category, String skill[], String language[], String[] experience, String[] salary) throws Exception{
		
		if (skill.length == 1 && skill[0] == "") {
			Arrays.fill(skill, null);
			skill[0] = "%%";
		}
		
		if (language.length == 1 && language[0] == ""){
			Arrays.fill(language, null);
			language[0] = "%%";
		}
		
		System.out.println("is " + skill[0]);
		
		PreparedStatement ps = con.prepareStatement("SELECT * FROM adv_search(?,?,?,?,?,?,?,?,?,?)");
		Array skillArray = con.createArrayOf("text", skill);
		Array languageArray = con.createArrayOf("text", language);
		
		ps.setString(1, degree);
		ps.setString(2, major);
		ps.setString(3, position);
		ps.setString(4, category);
		ps.setArray(5, skillArray);
		ps.setArray(6, languageArray);
		ps.setDouble(7, Double.parseDouble(salary[0]));
		ps.setDouble(8, Double.parseDouble(salary[1]));
		ps.setInt(9, Integer.parseInt(experience[0]));
		ps.setInt(10, Integer.parseInt(experience[1]));
		
		
		ResultSet rs = null;
		try {
			if((rs = ps.executeQuery()) != null) return rs;
		} catch (Exception e) {
			e.printStackTrace();
		}		
		
		/*finally{
			if (rs != null) try {rs.close();} catch (SQLException e) {throw e;}
			if (ps != null) try {ps.close();} catch (SQLException e) {throw e;}
			if (con != null) try {con.close();} catch (SQLException e) {throw e;}
		}*/
		
		return null;
	}	
}
