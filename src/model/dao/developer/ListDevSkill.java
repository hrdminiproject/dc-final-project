package model.dao.developer;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import model.dto.UserDev;
import model.dto.UserSkill;
import utilities.DBUtility;

public class ListDevSkill {
Connection con;
	
	// get connection when create object
	public ListDevSkill() throws Exception {
		DBUtility utility = new DBUtility();
		con = utility.getConnection();
		System.out.println("Connecting to database...");
	}
	
	public ArrayList<UserSkill> listUserDevSkill(int id) throws SQLException {
		UserSkill userSkill =null;
		PreparedStatement ps = con.prepareStatement("SELECT * FROM v_dev_skill WHERE id=?");
		ps.setInt(1, id);
		ArrayList<UserSkill> al=new ArrayList<UserSkill>();
		ResultSet rs = ps.executeQuery();
		
		try {
			while (rs.next()) {
				userSkill= new UserSkill();
				userSkill.setId(rs.getInt("id"));
				userSkill.setSid(rs.getInt("sid"));
				userSkill.setRate(rs.getInt("rate"));
				userSkill.setName(rs.getString("name"));
				al.add(userSkill);
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		finally {
			if (rs != null) try {rs.close();} catch (SQLException e) {throw e;}
			if (ps != null) try {ps.close();} catch (SQLException e) {throw e;}
			if (con != null) try {con.close();} catch (SQLException e) {throw e;}
		}
		return al;
	}

}
