package model.dao.developer;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import model.dto.DevSubjects;
import utilities.DBUtility;

public class UpdatePendingDev {
	Connection con;
	
	//get connection when create object
	public UpdatePendingDev() throws Exception{
		DBUtility utility = new DBUtility();
		con = utility.getConnection();
		System.out.println("Connecting to database...");
	}
	
	public boolean updateDevSubjects (String status, int id){
		PreparedStatement ps = null;
		try{
			ps = con.prepareStatement("UPDATE developers SET is_approved = ? WHERE id = ?");
			ps.setString(1,  status);
			ps.setInt(2, id);
			
			if (ps.executeUpdate() > 0)
				return true;
		} catch (Exception e){
			e.printStackTrace();
		} finally {
			try{
				ps.close();
				con.close();
			} catch (SQLException e){
				e.printStackTrace();
			}
		}
		return false;
	}	
}
