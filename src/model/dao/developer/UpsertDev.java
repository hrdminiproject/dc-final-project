package model.dao.developer;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import model.dto.Developers;
import model.dto.Users;
import utilities.Convertor;
import utilities.DBUtility;

public class UpsertDev {
Connection con;
	
	//get connection when create object
	public UpsertDev() throws Exception{
		DBUtility utility = new DBUtility();
		con = utility.getConnection();
		System.out.println("Connecting to database...");
	}
	
	public boolean upsertDeveloper(Users user, Developers dev){
		PreparedStatement ps1 = null, ps2=null;
		try {
			con.setAutoCommit(false);
			ps1 = con.prepareStatement("UPDATE users SET lname = ?, fname = ?, gender = ?, email = ? WHERE id = ?");
			ps1.setString(1, user.getLname());
			ps1.setString(2, user.getFname());
			ps1.setString(3, user.getGender());
			ps1.setString(4, user.getEmail());
			ps1.setInt(5, user.getId());
			
			ps2 = con.prepareStatement("SELECT upsert_developers(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)");
			ps2.setInt(1, user.getId());
			ps2.setString(2,dev.getPob());
			ps2.setDate(3, new java.sql.Date(Convertor.stringToDate(dev.getDob()).getTime()));
			ps2.setString(4, dev.getCurrentCityState());
			ps2.setString(5, dev.getAddress());
			ps2.setString(6, dev.getPhone());
			ps2.setString(7, dev.getNationality());
			ps2.setString(8, dev.getAvatar());
			ps2.setString(9, dev.getMarital());
			ps2.setDouble(10, dev.getSalary());	
			ps2.setString(11, dev.getSpecialtySkill());
			ps2.setString(12, dev.getWantedPosition());
			ps2.setString(13, dev.getMotivation());
			
			if(ps1.executeUpdate() > 0 && ps2.execute() ==true){
				con.commit();
				return true;
			}			
		} catch (Exception e) {
			e.printStackTrace();
			try {
				con.rollback();
			} catch (SQLException e1) {
				e1.printStackTrace();
			}
		} finally {
			try{
				ps1.close();
				ps2.close();
			} catch (SQLException e){
				e.printStackTrace();
			}
		}
		return false;
	}	
}
