package model.dao.developer;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.PreparedStatement;

import model.dto.UserDev;
import utilities.Convertor;
import utilities.DBUtility;

public class ListDev {
	Connection con;
	
	// get connection when create object
	public ListDev() throws Exception {
		DBUtility utility = new DBUtility();
		con = utility.getConnection();
		System.out.println("Connecting to database...");
	}
	
	public ResultSet listDevelopers() throws Exception{
		PreparedStatement ps = con.prepareStatement("SELECT * FROM v_info WHERE type='1'");
		ResultSet rs=null;
		if ((rs=ps.executeQuery()) != null) return rs;
		if (ps != null) try {ps.close();} catch (SQLException e) {throw e;}
		if (con != null) try {con.close();} catch (SQLException e) {throw e;}
		return null;
	}
	
	public ResultSet getDevRecord(int id) throws SQLException {
		PreparedStatement ps = con.prepareStatement("SELECT * FROM v_info WHERE id=?");
		ps.setInt(1, id);
		ResultSet rs = null;
		if ((rs=ps.executeQuery()) != null) return rs;
		if (ps != null) try {ps.close();} catch (SQLException e) {throw e;}
		if (con != null) try {con.close();} catch (SQLException e) {throw e;}
		return null;
	}
	
	public ResultSet countApprovedUser() throws SQLException {
		PreparedStatement ps = con.prepareStatement("SELECT count(id) approve FROM developers where is_approved = 'approved';");
		ResultSet rs = null;
		if ((rs=ps.executeQuery()) != null) return rs;
		if (ps != null) try {ps.close();} catch (SQLException e) {throw e;}
		if (con != null) try {con.close();} catch (SQLException e) {throw e;}
		return null;
	}
	
	public ResultSet countAllUser() throws SQLException {
		PreparedStatement ps = con.prepareStatement("SELECT count(id) users FROM users;");
		ResultSet rs = null;
		if ((rs=ps.executeQuery()) != null) return rs;
		if (ps != null) try {ps.close();} catch (SQLException e) {throw e;}
		if (con != null) try {con.close();} catch (SQLException e) {throw e;}
		return null;
	}
	
	public ResultSet countNormalUser() throws SQLException {
		PreparedStatement ps = con.prepareStatement("SELECT count(id) normal FROM users WHERE users.id NOT IN(SELECT id FROM developers);");
		ResultSet rs = null;
		if ((rs=ps.executeQuery()) != null) return rs;
		if (ps != null) try {ps.close();} catch (SQLException e) {throw e;}
		if (con != null) try {con.close();} catch (SQLException e) {throw e;}
		return null;
	}
	
	public ResultSet countRejectUser() throws SQLException {
		PreparedStatement ps = con.prepareStatement("SELECT count(id) reject FROM developers WHERE is_approved='reject';");
		ResultSet rs = null;
		if ((rs=ps.executeQuery()) != null) return rs;
		if (ps != null) try {ps.close();} catch (SQLException e) {throw e;}
		if (con != null) try {con.close();} catch (SQLException e) {throw e;}
		return null;
	}
	
	
	
	

	public UserDev getUserDevInfo(int id) throws SQLException {
		UserDev udev = new UserDev();
		PreparedStatement ps = con.prepareStatement("SELECT * FROM v_info WHERE id=?");
		ps.setInt(1, id);
		ResultSet rs = ps.executeQuery();
		
		try {
			while (rs.next()) {
				udev.setId(rs.getInt("id"));
				udev.setUsername(rs.getString("username"));
				udev.setFname(rs.getString("fname"));
				udev.setLname(rs.getString("lname"));
				udev.setEmail(rs.getString("email"));
				udev.setGender(rs.getString("gender"));
				udev.setDob(rs.getString("dob"));
				udev.setPob(rs.getString("pob"));
				udev.setCurrentCityState(rs.getString("current_city_state"));
				udev.setAddress(rs.getString("address"));
				udev.setPhone(rs.getString("phone"));
				udev.setNationality(rs.getString("nationality"));
				udev.setAvatar(rs.getString("avatar"));
				udev.setMarital(rs.getString("marital"));
				udev.setSalary(rs.getDouble("salary"));
				udev.setSpecialtySkill(rs.getString("specialty_skill"));
				udev.setWantedPosition(rs.getString("wanted_position"));
				udev.setMotivation(rs.getString("motivation"));
				
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		finally {
			if (rs != null) try {rs.close();} catch (SQLException e) {throw e;}
			if (ps != null) try {ps.close();} catch (SQLException e) {throw e;}
			if (con != null) try {con.close();} catch (SQLException e) {throw e;}
		}
		return udev;
	}
	
	public String listPendingDev() throws Exception{
		PreparedStatement ps = con.prepareStatement("SELECT * FROM v_pending_dev");
		ResultSet rs = null;
		String str="";
		if ((rs=ps.executeQuery()) != null){
			str = Convertor.convertResultSetIntoJSON(rs);
		}
		if (rs != null) try {rs.close();} catch (SQLException e) {throw e;}
		if (ps != null) try {ps.close();} catch (SQLException e) {throw e;}
		if (con != null) try {con.close();} catch (SQLException e) {throw e;}
		
		return str;
	}

}
