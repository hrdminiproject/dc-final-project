package model.dao.developer;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import model.dto.UserDev;
import model.dto.UserLang;
import model.dto.UserSkill;
import utilities.DBUtility;

public class ListDevLang {
Connection con;
	
	// get connection when create object
	public ListDevLang() throws Exception {
		DBUtility utility = new DBUtility();
		con = utility.getConnection();
		System.out.println("Connecting to database...");
	}
	
	public ArrayList<UserLang> listUserDevLang(int id) throws SQLException {
		UserLang userLang =null;
		PreparedStatement ps = con.prepareStatement("SELECT * FROM v_dev_language WHERE id=?");
		ps.setInt(1, id);
		ArrayList<UserLang> al=new ArrayList<UserLang>();
		ResultSet rs = ps.executeQuery();
		
		try {
			while (rs.next()) {
				userLang= new UserLang();
				userLang.setId(rs.getInt("id"));
				userLang.setLid(rs.getInt("lid"));
				userLang.setLanguage(rs.getString("language"));
				userLang.setUnderstandingLevel(rs.getInt("understanding_level"));
				userLang.setSpeakingLevel(rs.getInt("understanding_level"));
				userLang.setWritingLevel(rs.getInt("understanding_level"));
				al.add(userLang);
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		finally {
			if (rs != null) try {rs.close();} catch (SQLException e) {throw e;}
			if (ps != null) try {ps.close();} catch (SQLException e) {throw e;}
			if (con != null) try {con.close();} catch (SQLException e) {throw e;}
		}
		return al;
	}

}
