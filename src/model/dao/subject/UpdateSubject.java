package model.dao.subject;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import model.dto.Subjects;
import utilities.DBUtility;

public class UpdateSubject {
	Connection con;
	// get connection when create object
		public UpdateSubject() throws Exception {
			DBUtility utility = new DBUtility();
			con = utility.getConnection();
			System.out.println("Connecting to database...");
		}
		
	public boolean updateSubjects(Subjects sub){
		PreparedStatement ps = null;
		
		try {
			ps = con.prepareStatement("UPDATE subjects SET name = ?, description = ?, catid = ?, image_url = ? WHERE id = ?");
			ps.setString(1, sub.getName());
			ps.setString(2, sub.getDescription());
			ps.setInt(3, sub.getCatid());
			ps.setString(4, sub.getImage_url());
			ps.setInt(5, sub.getId());
			
			if (ps.executeUpdate() > 0)
				return true;
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				ps.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return false;
	}
}
