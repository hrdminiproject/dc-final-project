package model.dao.subject;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import utilities.DBUtility;

public class ListSubject {
	Connection con;
	// get connection when create object
	public ListSubject() throws Exception{
		DBUtility utility = new DBUtility();
		con = utility.getConnection();
		System.out.println("Connecting to database...");
	}
	
	public ResultSet listSubjects(int dev_id) throws Exception{
		PreparedStatement ps = con.prepareStatement("SELECT * FROM v_subject WHERE id NOT IN (SELECT sid FROM dev_subjects WHERE dev_id = ?)");
		ps.setInt(1, dev_id);
		ResultSet rs = null;
		if ((rs=ps.executeQuery()) != null) return rs;
		if (ps != null) try {ps.close();} catch (SQLException e) {throw e;}
		if (con != null) try {con.close();} catch (SQLException e) {throw e;}
		return null;
	}
	
	public ResultSet listAllSubjects() throws Exception{
		PreparedStatement ps = con.prepareStatement("SELECT * FROM subjects");
		ResultSet rs = null;
		if ((rs=ps.executeQuery()) != null) return rs;
		if (ps != null) try {ps.close();} catch (SQLException e) {throw e;}
		if (con != null) try {con.close();} catch (SQLException e) {throw e;}
		return null;
	}
}
