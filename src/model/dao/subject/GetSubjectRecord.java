package model.dao.subject;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import utilities.DBUtility;
import model.dto.Subjects;

public class GetSubjectRecord {
	Connection con;
	// get connection when create object
		public GetSubjectRecord() throws Exception {
			DBUtility utility = new DBUtility();
			con = utility.getConnection();
			System.out.println("Connecting to database...");
		}
		
		public Subjects listSubjectEdit(int id) throws Exception{
			PreparedStatement ps = con.prepareStatement("SELECT * FROM subjects where id=?");
			ps.setInt(1, id);
			ResultSet rs = ps.executeQuery();
			Subjects sub =null;
			try{
				while(rs.next()){
					sub = new Subjects();
					sub.setId(rs.getInt("id"));
					sub.setName(rs.getString("name"));
					sub.setDescription(rs.getString("description"));
					sub.setCatid(rs.getInt("catid"));
					sub.setImage_url(rs.getString("image_url"));
				}			
			} catch (SQLException e) {
				e.printStackTrace();
			} finally {
				if (rs != null)
					try {
						rs.close();
					} catch (SQLException e) {
						throw e;
					}
				if (ps != null)
					try {
						ps.close();
					} catch (SQLException e) {
						throw e;
					}
				if (con != null)
					try {
						con.close();
					} catch (SQLException e) {
						throw e;
					}
			}
			return sub;
		}
}
