package model.dao.subject;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import model.dto.Subjects;
import utilities.DBUtility;

public class AddSubject {
Connection con;
	
	// get connection when create object
	public AddSubject() throws Exception {
		DBUtility utility = new DBUtility();
		con = utility.getConnection();
		System.out.println("Connecting to database...");
	}
	
	public boolean addSubjects(Subjects sub){
		PreparedStatement ps = null;
		try {
			ps = con.prepareStatement("INSERT INTO subjects(name, description,catid, image_url) VALUES(?, ?, ?, ?)");
			ps.setString(1, sub.getName());
			ps.setString(2, sub.getDescription());
			ps.setInt(3, sub.getCatid());
			ps.setString(4, sub.getImage_url());
			
			if (ps.executeUpdate() > 0)
				return true;
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				ps.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return false;
	}
}
