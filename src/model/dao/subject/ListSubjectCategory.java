package model.dao.subject;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import utilities.Convertor;
import utilities.DBUtility;

public class ListSubjectCategory {
	Connection con;
	// get connection when create object
	public ListSubjectCategory() throws Exception{
		DBUtility utility = new DBUtility();
		con = utility.getConnection();
		System.out.println("Connecting to database...");
	}
	
	
	
	
	public String listSubjects() throws Exception{
		PreparedStatement ps = con.prepareStatement("SELECT * FROM v_subject");
		ResultSet rs = null;
		String str="";
		if ((rs=ps.executeQuery()) != null){
			str = Convertor.convertResultSetIntoJSON(rs);
		}
		if (rs != null) try {rs.close();} catch (SQLException e) {throw e;}
		if (ps != null) try {ps.close();} catch (SQLException e) {throw e;}
		if (con != null) try {con.close();} catch (SQLException e) {throw e;}
		
		return str;
		}
	
	public String listCategories() throws Exception{
		PreparedStatement ps = con.prepareStatement("SELECT * FROM categories");
		ResultSet rs = null;
		String str="";
		if ((rs=ps.executeQuery()) != null){
			str = Convertor.convertResultSetIntoJSON(rs);
		}
		if (rs != null) try {rs.close();} catch (SQLException e) {throw e;}
		if (ps != null) try {ps.close();} catch (SQLException e) {throw e;}
		if (con != null) try {con.close();} catch (SQLException e) {throw e;}
		
		return str;
	}	
}
