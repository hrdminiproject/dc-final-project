package model.dao.file;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import model.dto.Files;
import utilities.DBUtility;

public class UpdateFile {
	Connection con;
	// get connection when create object
		public UpdateFile() throws Exception {
			DBUtility utility = new DBUtility();
			con = utility.getConnection();
			System.out.println("Connecting to database...");
		}
		
	public boolean updateFiles(Files file){
		PreparedStatement ps = null;
		
		try {
			ps = con.prepareStatement("UPDATE files SET title = ?, url = ?, description = ? WHERE id = ?");
			ps.setString(1, file.getTitle());
			ps.setString(2, file.getUrl());
			ps.setString(3, file.getDescription());
			ps.setInt(4, file.getId());
			
			if (ps.executeUpdate() > 0)
				return true;
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				ps.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return false;
	}
}
