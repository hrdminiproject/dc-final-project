package model.dao.file;

import model.dto.Files;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import utilities.DBUtility;

public class ListFile {
	Connection con;
	// get connection when create object
	public ListFile() throws Exception{
		DBUtility utility = new DBUtility();
		con = utility.getConnection();
		System.out.println("Connecting to database...");
	}
	
	public ArrayList<Files> listFiles() throws Exception{
		PreparedStatement ps = con.prepareStatement("SELECT * FROM files");
		ResultSet rs = ps.executeQuery();
		ArrayList<Files> files = new ArrayList<Files>();
		try{
			while(rs.next()){
				Files file = new Files();
				file.setId(rs.getInt("id"));
				file.setTitle(rs.getString("name"));
				file.setUrl(rs.getString("url"));
				file.setDescription(rs.getString("description"));
				file.setAchieveId(rs.getInt("achieve_id"));
				file.setEduId(rs.getInt("edu_id"));
				file.setExpId(rs.getInt("exp_id"));
				files.add(file);
				System.out.println(files);
			}			
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			if (rs != null)
				try {
					rs.close();
				} catch (SQLException e) {
					throw e;
				}
			if (ps != null)
				try {
					ps.close();
				} catch (SQLException e) {
					throw e;
				}
			if (con != null)
				try {
					con.close();
				} catch (SQLException e) {
					throw e;
				}
		}
		return files;
	}	
}
