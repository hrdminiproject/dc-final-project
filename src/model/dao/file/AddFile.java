package model.dao.file;

import model.dto.Files;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import utilities.DBUtility;

public class AddFile {
Connection con;
	
	// get connection when create object
	public AddFile() throws Exception {
		DBUtility utility = new DBUtility();
		con = utility.getConnection();
	}
	
	public boolean addFiles(Files file){
		PreparedStatement ps = null;
		try {
			ps = con.prepareStatement("INSERT INTO files (url, exp_id) VALUES (?, (SELECT (last_value -1) FROM files_id_seq))");
			ps.setString(1, file.getUrl());
			
			if (ps.executeUpdate() > 0)
				return true;
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				ps.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return false;
	}
}
