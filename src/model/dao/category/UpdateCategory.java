package model.dao.category;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import model.dto.Categories;
import utilities.DBUtility;

public class UpdateCategory {
	Connection con;
	// get connection when create object
		public UpdateCategory() throws Exception {
			DBUtility utility = new DBUtility();
			con = utility.getConnection();
			System.out.println("Connecting to database...");
		}
		
	public boolean updateCategories(Categories cat){
		PreparedStatement ps = null;
		
		try {
			ps = con.prepareStatement("UPDATE categories SET name = ?, description = ?  WHERE id = ?");
			ps.setString(1, cat.getName());
			ps.setString(2, cat.getDescription());
			ps.setInt(3, cat.getId());
			
			
			if (ps.executeUpdate() > 0)
				return true;
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				ps.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return false;
	}
	
}
