package model.dao.category;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import utilities.DBUtility;

public class DeleteCategory {
	Connection con;
	// get connection when create object
		public DeleteCategory() throws Exception {
			DBUtility utility = new DBUtility();
			con = utility.getConnection();
			System.out.println("Connecting to database...");
		}
		
		public boolean deleteCategories(int id){
			PreparedStatement ps = null;
			try {
				ps = con.prepareStatement("DELETE FROM categories WHERE id = ?");
				ps.setInt(1, id);
				if (ps.executeUpdate() > 0)
					return true;
			} catch (Exception e) {
				e.printStackTrace();
			} finally {
				try {
					ps.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
			return false;
		}
}
