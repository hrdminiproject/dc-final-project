package model.dao.category;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import model.dto.Categories;
import utilities.DBUtility;


public class AddCategory {
Connection con;
	
	// get connection when create object
	public AddCategory() throws Exception {
		DBUtility utility = new DBUtility();
		con = utility.getConnection();
		System.out.println("Connecting to database...");
	}
	
	public boolean addCategories(Categories cat){
		PreparedStatement ps = null;
		try {
			ps = con.prepareStatement("INSERT INTO categories(name, description) VALUES(?, ?)");
			ps.setString(1, cat.getName());
			ps.setString(2, cat.getDescription());
			if (ps.executeUpdate() > 0)
				return true;
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				ps.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return false;
	}
}
