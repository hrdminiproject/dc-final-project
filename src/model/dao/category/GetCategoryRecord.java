package model.dao.category;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import utilities.DBUtility;
import model.dto.Categories;

public class GetCategoryRecord {
	Connection con;
	// get connection when create object
		public GetCategoryRecord() throws Exception {
			DBUtility utility = new DBUtility();
			con = utility.getConnection();
			System.out.println("Connecting to database...");
		}
		
		public Categories listCategoriesEdit(int id) throws Exception{
			PreparedStatement ps = con.prepareStatement("SELECT * FROM categories where id=?");
			ps.setInt(1, id);
			ResultSet rs = ps.executeQuery();
			Categories cat =null;
			try{
				while(rs.next()){
					cat = new Categories();
					cat.setId(rs.getInt("id"));
					cat.setName(rs.getString("name"));
					cat.setDescription(rs.getString("description"));
				}			
			} catch (SQLException e) {
				e.printStackTrace();
			} finally {
				if (rs != null)
					try {
						rs.close();
					} catch (SQLException e) {
						throw e;
					}
				if (ps != null)
					try {
						ps.close();
					} catch (SQLException e) {
						throw e;
					}
				if (con != null)
					try {
						con.close();
					} catch (SQLException e) {
						throw e;
					}
			}
			return cat;
		}
}
