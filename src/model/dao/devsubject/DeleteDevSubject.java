package model.dao.devsubject;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.PreparedStatement;
import utilities.DBUtility;

public class DeleteDevSubject {
	Connection con;
	
	// get connection when create object
	public DeleteDevSubject() throws Exception {
		DBUtility utility = new DBUtility();
		con = utility.getConnection();
		System.out.println("Connecting to database...");
	}
	
	public boolean deleteDevSubjects(int devid, int sid){
		PreparedStatement ps = null;
		try{
			ps = con.prepareStatement("DELETE FROM dev_subjects WHERE dev_id = ? AND sid = ?");
			ps.setInt(1, devid);
			ps.setInt(2, sid);
			if (ps.executeUpdate() > 0)
				return true;
		}catch (Exception e){
			e.printStackTrace();
		} finally {
			try {
				ps.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	return false;
	}
}
