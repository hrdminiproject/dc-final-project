package model.dao.devsubject;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.PreparedStatement;
import utilities.DBUtility;

public class ListDevSubject {
	Connection con;
	
	// get connection when create object
	public ListDevSubject() throws Exception {
		DBUtility utility = new DBUtility();
		con = utility.getConnection();
		System.out.println("Connecting to database...");
	}
	
	public ResultSet listDevSubjects(int id) throws Exception{
		
		PreparedStatement ps = con.prepareStatement("SELECT * FROM v_dev_subjects WHERE id = ? ORDER BY catname asc");
		ps.setInt(1, id);
		ResultSet rs = null;
		if ((rs=ps.executeQuery()) != null) return rs;
		if (ps != null) try {ps.close();} catch (SQLException e) {throw e;}
		if (con != null) try {con.close();} catch (SQLException e) {throw e;}
		return null;
	}
}
