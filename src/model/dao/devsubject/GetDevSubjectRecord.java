package model.dao.devsubject;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import utilities.DBUtility;

public class GetDevSubjectRecord {
	Connection con;
	// get connection when create object
		public GetDevSubjectRecord() throws Exception {
			DBUtility utility = new DBUtility();
			con = utility.getConnection();
			System.out.println("Connecting to database...");
		}
		
		public ResultSet listDevSubjectEdit(int did, int sid) throws Exception{
			PreparedStatement ps = con.prepareStatement("SELECT * FROM v_dev_subjects where id = ? AND sid= ?");
			ps.setInt(1, did);
			ps.setInt(2, sid);
			ResultSet rs = null;
			if ((rs=ps.executeQuery()) != null) return rs;
			if (ps != null) try {ps.close();} catch (SQLException e) {throw e;}
			if (con != null) try {con.close();} catch (SQLException e) {throw e;}
			return null;
		}
}
