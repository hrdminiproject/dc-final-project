package model.dao.devsubject;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import model.dto.DevSubjects;
import utilities.DBUtility;

public class AddDevSubject {
	Connection con;
	
	// get connection when create object
	public AddDevSubject() throws Exception {
		DBUtility utility = new DBUtility();
		con = utility.getConnection();
		System.out.println("Connecting to database...");
	}
	
	public boolean addDevSubjects(DevSubjects dev_subject){
		PreparedStatement ps = null;
		try {
			ps = con.prepareStatement("INSERT INTO dev_subjects VALUES(?, ?, ?, ?)");
			ps.setInt(1, dev_subject.getDevid());
			ps.setInt(2,  dev_subject.getSid());
			ps.setString(3, dev_subject.getRate());
			ps.setString(4, dev_subject.getDescription());
			
			if (ps.executeUpdate() > 0)
				return true;
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				ps.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return false;
	}	
}
