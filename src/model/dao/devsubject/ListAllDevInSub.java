package model.dao.devsubject;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import utilities.DBUtility;

public class ListAllDevInSub {
	Connection con;
	
	// get connection when create object
	public ListAllDevInSub() throws Exception {
		DBUtility utility = new DBUtility();
		con = utility.getConnection();
		System.out.println("Connecting to database...");
	}
	
	public ResultSet listDevInSubjects() throws Exception{
		
		PreparedStatement ps = con.prepareStatement("SELECT catname cat, count(DISTINCT (id)) skill FROM v_dev_subjects WHERE id IN (SELECT id FROM developers WHERE is_approved='approved') group by catname order by catname asc");
		ResultSet rs = null;
		if ((rs=ps.executeQuery()) != null) return rs;
		if (ps != null) try {ps.close();} catch (SQLException e) {throw e;}
		if (con != null) try {con.close();} catch (SQLException e) {throw e;}
		return null;
	}
}
