package model.dao.devsubject;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import model.dto.DevSubjects;
import utilities.DBUtility;

public class UpdateDevSubject {
	Connection con;
	
	//get connection when create object
	public UpdateDevSubject() throws Exception{
		DBUtility utility = new DBUtility();
		con = utility.getConnection();
		System.out.println("Connecting to database...");
	}
	
	public boolean updateDevSubjects (DevSubjects dev_subject){
		PreparedStatement ps = null;
		
		try{
			ps = con.prepareStatement("UPDATE dev_subjects SET rate = ?, description = ? WHERE dev_id = ? AND sid = ?");
			ps.setString(1,  dev_subject.getRate());
			ps.setString(2, dev_subject.getDescription());
			ps.setInt(3,  dev_subject.getDevid());
			ps.setInt(4, dev_subject.getSid());
			
			if (ps.executeUpdate() > 0)
				return true;
		} catch (Exception e){
			e.printStackTrace();
		} finally {
			try{
				ps.close();
			} catch (SQLException e){
				e.printStackTrace();
			}
		}
		return false;
	}	
}
