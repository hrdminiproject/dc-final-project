package model.dao.language;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import model.dto.Languages;
import utilities.DBUtility;


public class AddLanguage {
Connection con;
	
	// get connection when create object
	public AddLanguage() throws Exception {
		DBUtility utility = new DBUtility();
		con = utility.getConnection();
		System.out.println("Connecting to database...");
	}
	
	public boolean addLanguages(Languages lang){
		PreparedStatement ps = null;
		try {
			ps = con.prepareStatement("INSERT INTO languages(language) VALUES(?)");
			ps.setString(1, lang.getLanguage());
			
			if (ps.executeUpdate() > 0)
				return true;
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				ps.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return false;
	}
}
