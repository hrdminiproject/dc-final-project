package model.dao.language;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import utilities.DBUtility;

public class ListLanguage {
	Connection con;
	// get connection when create object
	public ListLanguage() throws Exception{
		DBUtility utility = new DBUtility();
		con = utility.getConnection();
		System.out.println("Connecting to database...");
	}
	
	public ResultSet listLanguages(int dev_id) throws Exception{
		PreparedStatement ps = con.prepareStatement("SELECT * FROM languages WHERE id NOT IN (SELECT DISTINCT lid FROM dev_language where dev_id = ?)");
		ps.setInt(1, dev_id);
		ResultSet rs = null;
		if ((rs=ps.executeQuery()) != null) return rs;
		if (ps != null) try {ps.close();} catch (SQLException e) {throw e;}
		if (con != null) try {con.close();} catch (SQLException e) {throw e;}
		return null;		
	}	
	
	public ResultSet listAdminLang() throws Exception{
		PreparedStatement ps = con.prepareStatement("SELECT * FROM languages");
		ResultSet rs = null;
		if ((rs=ps.executeQuery()) != null) return rs;
		if (ps != null) try {ps.close();} catch (SQLException e) {throw e;}
		if (con != null) try {con.close();} catch (SQLException e) {throw e;}
		return null;		
	}	
}
