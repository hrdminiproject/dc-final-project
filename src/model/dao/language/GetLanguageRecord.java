package model.dao.language;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import utilities.DBUtility;
import model.dto.Languages;

public class GetLanguageRecord {
	Connection con;
	// get connection when create object
		public GetLanguageRecord() throws Exception {
			DBUtility utility = new DBUtility();
			con = utility.getConnection();
			System.out.println("Connecting to database...");
		}
		
		public Languages listLanguagesEdit(int id) throws Exception{
			PreparedStatement ps = con.prepareStatement("SELECT * FROM languages where id=?");
			ps.setInt(1, id);
			ResultSet rs = ps.executeQuery();
			Languages lang =null;
			try{
				while(rs.next()){
					lang = new Languages();
					lang.setId(rs.getInt("id"));
					lang.setLanguage(rs.getString("language"));					
				}			
			} catch (SQLException e) {
				e.printStackTrace();
			} finally {
				if (rs != null)
					try {
						rs.close();
					} catch (SQLException e) {
						throw e;
					}
				if (ps != null)
					try {
						ps.close();
					} catch (SQLException e) {
						throw e;
					}
				if (con != null)
					try {
						con.close();
					} catch (SQLException e) {
						throw e;
					}
			}
			return lang;
		}
}
