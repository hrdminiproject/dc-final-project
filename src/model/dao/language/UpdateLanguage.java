package model.dao.language;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import model.dto.Languages;
import utilities.DBUtility;

public class UpdateLanguage {
	Connection con;
	// get connection when create object
		public UpdateLanguage() throws Exception {
			DBUtility utility = new DBUtility();
			con = utility.getConnection();
			System.out.println("Connecting to database...");
		}
		
	public boolean updateLanguages(Languages lang){
		PreparedStatement ps = null;
		
		try {
			ps = con.prepareStatement("UPDATE languages SET language = ? WHERE id = ?");
			ps.setString(1, lang.getLanguage());
			ps.setInt(2, lang.getId());
			
			if (ps.executeUpdate() > 0)
				return true;
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				ps.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return false;
	}
	
}
