package model.dao.language;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import utilities.DBUtility;

public class DeleteLanguage {
	Connection con;
	// get connection when create object
		public DeleteLanguage() throws Exception {
			DBUtility utility = new DBUtility();
			con = utility.getConnection();
			System.out.println("Connecting to database...");
		}
		
		public boolean deleteLanguages(int id){
			PreparedStatement ps = null;
			try {
				ps = con.prepareStatement("DELETE FROM languages WHERE id = ?");
				ps.setInt(1, id);
				if (ps.executeUpdate() > 0)
					return true;
			} catch (Exception e) {
				e.printStackTrace();
			} finally {
				try {
					ps.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
			return false;
		}
}
