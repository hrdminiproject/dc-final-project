package model.dao.achievement;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.sql.PreparedStatement;

import model.dto.Achievement;
import utilities.DBUtility;

public class ListAch {
	Connection con;
	
	// get connection when create object
	public ListAch() throws Exception {
		DBUtility utility = new DBUtility();
		con = utility.getConnection();
	}
	
	public ArrayList<Achievement> listAch(int dev_id) throws Exception{
		PreparedStatement ps = con.prepareStatement("SELECT * FROM achievements where dev_id=? ORDER BY year asc");
		ps.setInt(1, dev_id);
		ResultSet rs = ps.executeQuery();
		ArrayList<Achievement> achievement = new ArrayList<Achievement>();
		try{
			while(rs.next()){
				Achievement ach = new Achievement();
				ach.setId(rs.getInt("id"));
				ach.setTitle(rs.getString("title"));
				ach.setYear(rs.getString("year"));
				ach.setLocation(rs.getString("location"));
				ach.setImage_url(rs.getString("image_url"));
				ach.setDescription(rs.getString("description"));
				achievement.add(ach);
			}			
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			if (rs != null) try {rs.close();} catch (SQLException e) {throw e;}
			if (ps != null) try {ps.close();} catch (SQLException e) {throw e;}
			if (con != null) try {con.close();} catch (SQLException e) {throw e;}
		}
		return achievement;
	}
	
		public  ArrayList<Achievement> getRecord(int id) throws SQLException{
				
			PreparedStatement ps=con.prepareStatement("select * from achievements where id=?");
			ps.setInt(1,id);
			ResultSet rs=ps.executeQuery();
			ArrayList<Achievement> achievements = new ArrayList<Achievement>();
			try{
				while(rs.next()){
					Achievement ach = new Achievement();
					ach.setId(rs.getInt("id"));
					ach.setTitle(rs.getString("title"));
					ach.setYear(rs.getString("year"));
					ach.setLocation(rs.getString("location"));
					ach.setLocation(rs.getString("location"));
					ach.setImage_url(rs.getString("image_url"));
					ach.setDescription(rs.getString("description"));
					achievements.add(ach);
				}		
				return achievements;
			}catch(Exception e){
				e.printStackTrace();
			}
			finally {
				if (rs != null) try {rs.close();} catch (SQLException e) {throw e;}
				if (ps != null) try {ps.close();} catch (SQLException e) {throw e;}
				if (con != null) try {con.close();} catch (SQLException e) {throw e;}
			}
			return null;
		}

}
