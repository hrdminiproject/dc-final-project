package model.dao.achievement;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import model.dto.Achievement;
import utilities.DBUtility;

public class AddAch {
	Connection con;
	
	// get connection when create object
	public AddAch() throws Exception {
		DBUtility utility = new DBUtility();
		con = utility.getConnection();
	}
	
	public boolean addAch(Achievement ach){
		PreparedStatement ps = null;
		try {
			ps = con.prepareStatement("INSERT INTO achievements (title, year, location, image_url, description, dev_id) VALUES(?, ?, ?, ?, ?, ?)");
			ps.setString(1, ach.getTitle());
			ps.setString(2, ach.getYear());
			ps.setString(3, ach.getLocation());
			ps.setString(4, ach.getImage_url());
			ps.setString(5, ach.getDescription());
			ps.setInt(6, ach.getDev_id());
			if (ps.executeUpdate() > 0)
				return true;
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				ps.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return false;
	}
}
