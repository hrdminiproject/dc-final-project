package model.dao.achievement;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import model.dto.Achievement;
import utilities.DBUtility;

public class UpdateAch {
	Connection con;
	
	//get connection when create object
	public UpdateAch() throws Exception{
		DBUtility utility = new DBUtility();
		con = utility.getConnection();
	}
	
	public boolean udpateAch (Achievement ach){
		PreparedStatement ps = null;
		
		try{
			ps = con.prepareStatement("UPDATE achievements SET title = ?, year = ?, location = ?, image_url = ?, description = ? WHERE id = ?");
			ps.setString(1, ach.getTitle());
			ps.setString(2, ach.getYear());
			ps.setString(3, ach.getLocation());
			ps.setString(4, ach.getImage_url());
			ps.setString(5, ach.getDescription());
			ps.setInt(6, ach.getId());
			
			if (ps.executeUpdate() > 0)
				return true;
		} catch (Exception e){
			e.printStackTrace();
		} finally {
			try{
				ps.close();
			} catch (SQLException e){
				e.printStackTrace();
			}
		}
		return false;
	}	
}
