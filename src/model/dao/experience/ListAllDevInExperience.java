package model.dao.experience;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import utilities.DBUtility;

public class ListAllDevInExperience {
Connection con;
	
	// get connection when create object
	public ListAllDevInExperience() throws Exception {
		DBUtility utility = new DBUtility();
		con = utility.getConnection();
		System.out.println("Connecting to database...");
	}
	
	public ResultSet listDevInExperience() throws Exception{
		
		PreparedStatement ps = con.prepareStatement("select t.range, count(*) "
													+ "from ( "
													+ "select case "
													+ "		when total_exp between 1 and 2 then '1-2' "
													+ "		when total_exp between 3 and 5 then '3-5' "
													+ "		when total_exp between 6 and 10 then '6-10' "
													+ " 	when total_exp between 11 and 99 then '11-99' "
													+ "		else '0' end as range "
													+ "from v_front_dev_detail "
													+ "WHERE "
													+ "(id IN (SELECT id FROM developers WHERE is_approved='approved'))) t "
													+ "group by t.range order by t.range asc;");
		
		ResultSet rs = null;
		if ((rs=ps.executeQuery()) != null) return rs;
		if (ps != null) try {ps.close();} catch (SQLException e) {throw e;}
		if (con != null) try {con.close();} catch (SQLException e) {throw e;}
		return null;
	}
}
