package model.dao.experience;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import model.dto.Experiences;
import utilities.DBUtility;

public class AddExperience {
	Connection con;
	
	// get connection when create object
	public AddExperience() throws Exception {
		DBUtility utility = new DBUtility();
		con = utility.getConnection();
	}
	
	public boolean addExperiences(Experiences ex){
		PreparedStatement ps = null;
		try {
			ps = con.prepareStatement("INSERT INTO experiences (place, position, duration, type, skill, description, image_url, dev_id) "
									+ "VALUES (?, ?, ?, ?, ?, ?, ?, ?)");
			ps.setString(1, ex.getPlace());
			ps.setString(2, ex.getPosition());
			ps.setString(3, ex.getDuration());
			ps.setString(4, ex.getType());
			ps.setString(5, ex.getSkill());
			ps.setString(6, ex.getDescription());
			ps.setString(7, ex.getImage_url());
			ps.setInt(8, ex.getDevid());
			
			if (ps.executeUpdate() > 0)
				return true;
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				ps.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return false;
	}	
}
