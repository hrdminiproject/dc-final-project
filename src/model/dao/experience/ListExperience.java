package model.dao.experience;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.sql.PreparedStatement;
import model.dto.Experiences;
import utilities.DBUtility;

public class ListExperience {
	Connection con;
	
	// get connection when create object
	public ListExperience() throws Exception {
		DBUtility utility = new DBUtility();
		con = utility.getConnection();
	}
	
	public ArrayList<Experiences> listExperiences(int dev_id) throws Exception{
		PreparedStatement ps = con.prepareStatement("SELECT * FROM experiences WHERE dev_id=? ORDER BY position asc");
		ps.setInt(1, dev_id);
		ResultSet rs = ps.executeQuery();
		ArrayList<Experiences> experiences = new ArrayList<Experiences>();
		try{
			while(rs.next()){
				Experiences ex = new Experiences();
				ex.setId(rs.getInt("id"));
				ex.setPlace(rs.getString("place"));
				ex.setPosition(rs.getString("position"));
				ex.setDuration(rs.getString("duration"));
				ex.setType(rs.getString("type"));
				ex.setSkill(rs.getString("skill"));
				//ex.setDescription(rs.getString("description"));
				ex.setImage_url(rs.getString("image_url"));
				experiences.add(ex);
			}			
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			if (rs != null) try {rs.close();} catch (SQLException e) {throw e;}
			if (ps != null) try {ps.close();} catch (SQLException e) {throw e;}
			if (con != null) try {con.close();} catch (SQLException e) {throw e;}
		}
		return experiences;
	}
	
	public  ArrayList<Experiences> getRecord(int id) throws SQLException{
		
		PreparedStatement ps=con.prepareStatement("select * from experiences where id=?");
		ps.setInt(1,id);
		ResultSet rs=ps.executeQuery();
		ArrayList<Experiences> experiences = new ArrayList<Experiences>();
		try{
		while(rs.next()){
			Experiences ex = new Experiences();
			ex.setId(rs.getInt("id"));
			ex.setPlace(rs.getString("place"));
			ex.setPosition(rs.getString("position"));
			ex.setDuration(rs.getString("duration"));
			ex.setType(rs.getString("type"));
			ex.setSkill(rs.getString("skill"));
			ex.setDescription(rs.getString("description"));
			ex.setImage_url(rs.getString("image_url"));
			experiences.add(ex);
		}		
		return experiences;
		}catch(Exception e){e.printStackTrace();}
		finally {
			if (rs != null) try {rs.close();} catch (SQLException e) {throw e;}
			if (ps != null) try {ps.close();} catch (SQLException e) {throw e;}
			if (con != null) try {con.close();} catch (SQLException e) {throw e;}
		}
		return null;
	}
}
