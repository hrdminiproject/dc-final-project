package model.dao.experience;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.PreparedStatement;
import utilities.DBUtility;

public class DeleteExperience {
	Connection con;
	
	// get connection when create object
	public DeleteExperience() throws Exception {
		DBUtility utility = new DBUtility();
		con = utility.getConnection();
	}
	
	public boolean deleteExperiences(int id){
		PreparedStatement ps = null;
		try{
			ps = con.prepareStatement("DELETE FROM experiences WHERE id = ?");
			ps.setInt(1, id);
			if (ps.executeUpdate() > 0)
				return true;
		}catch (Exception e){
			e.printStackTrace();
		} finally {
			try { ps.close(); } catch (SQLException e) { e.printStackTrace(); }
		}
	return false;
	}
}
