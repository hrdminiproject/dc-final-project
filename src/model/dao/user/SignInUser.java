package model.dao.user;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import model.dto.Users;
import utilities.DBUtility;

public class SignInUser {
Connection con;
	
	// get connection when create object
	public SignInUser() throws Exception {
		DBUtility utility = new DBUtility();
		con = utility.getConnection();
		System.out.println("Connecting to database...");
	}
	
	public Users signIn(String name, String pwd) {
		Users u = new Users();
		PreparedStatement ps = null;
		try {
			ps = con.prepareStatement("SELECT id, username, avatar,type, lname, fname FROM v_info WHERE username=? AND password=?");
			ps.setString(1, name);
			ps.setString(2, pwd);
			ResultSet rs=ps.executeQuery();
			if (rs.next()){
				u.setId(rs.getInt("id"));
				u.setUsername(rs.getString("username"));
				u.setType(rs.getString("type"));
				u.setAvatar(rs.getString("avatar"));
				u.setLname(rs.getString("lname"));
				u.setFname(rs.getString("fname"));
				return u;
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				ps.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return null;
	}
}
