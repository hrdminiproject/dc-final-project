package model.dao.user;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import utilities.DBUtility;

public class ReactiveUser {
	Connection con;
	// get connection when create object
		public ReactiveUser() throws Exception {
			DBUtility utility = new DBUtility();
			con = utility.getConnection();
		}
		
		public boolean reactiveUser(int id){
			PreparedStatement ps = null;
			try {
				ps = con.prepareStatement("UPDATE users set status = 't' WHERE id=?");
				ps.setInt(1, id);
				if (ps.executeUpdate() > 0)
					return true;
			} catch (Exception e) {
				e.printStackTrace();
			} finally {
				try {
					ps.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
			return false;
		}
}
