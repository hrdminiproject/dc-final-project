package model.dao.user;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import model.dto.Users;
import utilities.DBUtility;

public class UpdateUserInfo {
	Connection con;
	// get connection when create object
		public UpdateUserInfo() throws Exception {
			DBUtility utility = new DBUtility();
			con = utility.getConnection();
			System.out.println("Connecting to database...");
		}
		
	public boolean updateUserInfo(Users user){
		PreparedStatement ps = null;
		System.out.println(user.getEmail() + user.getFname());
		
		try {
			ps = con.prepareStatement("UPDATE users SET lname = ?, fname = ?, gender = ?, email = ? WHERE id = ?");
			ps.setString(1, user.getLname());
			ps.setString(2, user.getFname());
			ps.setString(3, user.getGender());
			ps.setString(4, user.getEmail());
			ps.setInt(5, user.getId());
			
			if (ps.executeUpdate() > 0)
				return true;
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				ps.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return false;
	}
	
}
