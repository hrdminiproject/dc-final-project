package model.dao.user;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import model.dto.Users;
import utilities.DBUtility;

public class AddUser {
	Connection con;
	
	// get connection when create object
	public AddUser() throws Exception {
		DBUtility utility = new DBUtility();
		con = utility.getConnection();
		System.out.println("Connecting to database...");
	}
	
	public boolean addUser(Users user) {
		PreparedStatement ps = null;
		try {
			ps = con.prepareStatement("INSERT INTO users (username, password, lname, fname, email, gender, type, status) "
									+ "VALUES (? ,?, ?, ?, ?, ?, ?, ?)");
			ps.setString(1, user.getUsername());
			ps.setString(2, user.getPassword());
			ps.setString(4, user.getLname());
			ps.setString(3, user.getFname());
			ps.setString(5, user.getEmail());
			ps.setString(6, user.getGender());
			ps.setString(7, user.getType());
			ps.setBoolean(8, Boolean.parseBoolean(user.getStatus()));
			
			if (ps.executeUpdate() > 0)
				return true;
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				ps.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return false;
	}
}
