package model.dao.user;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import model.dto.Users;
import utilities.DBUtility;

public class UpdateType {
	Connection con;
	// get connection when create object
		public UpdateType() throws Exception {
			DBUtility utility = new DBUtility();
			con = utility.getConnection();
			System.out.println("Connecting to database...");
		}
		
	public boolean updatetype(Users user){

					PreparedStatement ps=null;
					System.out.println(user.getId());
					try {
						ps = con.prepareStatement("UPDATE users SET type =1  WHERE id = ?");
					
						ps.setInt(1, user.getId());
						
						if(ps.executeUpdate() > 0)
							return true;
					} catch (Exception e) {
						e.printStackTrace();
					}
					finally{
						try {
							ps.close();
						} catch (SQLException e) {
							e.printStackTrace();
						}
					}

		return false;
	}
	
}
