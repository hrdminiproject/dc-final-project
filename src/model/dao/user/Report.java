package model.dao.user;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;

import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import utilities.DBUtility;

public class Report {
	Connection con;

	// get connection when create object
	public Report() throws Exception {
		DBUtility utility = new DBUtility();
		con = utility.getConnection();
	}

	public void generateReport(HttpServletResponse response, Map<String, Object> param, String jrxmlPath) {
		try {
			InputStream input = new FileInputStream(new File(jrxmlPath));
			System.out.println("input: " + input);
			JasperReport jasper = JasperCompileManager.compileReport(input);
			System.out.println("jasper: " + jasper);
			JasperPrint print = JasperFillManager.fillReport(jasper, param, con);
			System.out.println("print: " + print);
			JasperExportManager.exportReportToPdfStream(print, response.getOutputStream());
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				con.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

	public static void main(String[] args) throws Exception {
		new Report().generateReport(null, null, "");
	}
}
