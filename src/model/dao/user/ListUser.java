package model.dao.user;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import utilities.DBUtility;
import model.dto.Users;

public class ListUser{
	Connection con;
	// get connection when create object
	public ListUser() throws Exception{
		DBUtility utility = new DBUtility();
		con = utility.getConnection();
	}
	
	public ArrayList<Users> listUsers() throws Exception{
		PreparedStatement ps = con.prepareStatement("SELECT * FROM users ORDER BY id");
		ResultSet rs = ps.executeQuery();
		ArrayList<Users> users = new ArrayList<Users>();
		try{
			while(rs.next()){
				Users user = new Users();
				user.setId(rs.getInt("id"));
				user.setUsername(rs.getString("username"));
				user.setPassword(rs.getString("password"));
				user.setFname(rs.getString("fname"));
				user.setLname(rs.getString("lname"));
				user.setEmail(rs.getString("email"));
				user.setGender(rs.getString("gender"));
				user.setType(rs.getString("type"));
				user.setStatus(rs.getString("status"));
				users.add(user);
			}		
			return users;
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			if (rs != null) try {rs.close();} catch (SQLException e) {throw e;}
			if (ps != null) try {ps.close();} catch (SQLException e) {throw e;}
			if (con != null) try {con.close();} catch (SQLException e) {throw e;}
		}
		return null;
	}	
	
	public  Users getRecord(int id) throws SQLException{
		PreparedStatement ps=con.prepareStatement("select * from users where id=?");
		ps.setInt(1,id);
		ResultSet rs=ps.executeQuery();
		Users user = new Users();
		try{
			while(rs.next()){
				user.setId(rs.getInt("id"));
				user.setUsername(rs.getString("username"));
				user.setPassword(rs.getString("password"));
				user.setFname(rs.getString("fname"));
				user.setLname(rs.getString("lname"));
				user.setEmail(rs.getString("email"));
				user.setGender(rs.getString("gender"));
				user.setType(rs.getString("type"));
				user.setStatus(rs.getString("status"));
			}		
			return user;
		}catch(Exception e){
			e.printStackTrace();
		}
		finally {
			if (rs != null) try {rs.close();} catch (SQLException e) {throw e;}
			if (ps != null) try {ps.close();} catch (SQLException e) {throw e;}
			if (con != null) try {con.close();} catch (SQLException e) {throw e;}
		}
		return null;
	}
}
