package model.dao.user;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import model.dto.Users;
import utilities.DBUtility;

public class UpdateChangeInfoAdmin {
	Connection con;
	// get connection when create object
		public UpdateChangeInfoAdmin() throws Exception {
			DBUtility utility = new DBUtility();
			con = utility.getConnection();
			System.out.println("Connecting to database...");
		}
		
	public boolean updatechangeinfo(Users user){

					PreparedStatement ps=null;
					
					try {
						ps = con.prepareStatement("UPDATE users SET lname = ?, fname = ?, email = ? WHERE id = ?");
						ps.setString(1, user.getLname());
						ps.setString(2, user.getFname());
						ps.setString(3, user.getEmail());
						ps.setInt(4, user.getId());
						
						if(ps.executeUpdate() > 0)
							return true;
					} catch (Exception e) {
						e.printStackTrace();
					}
					finally{
						try {
							ps.close();
						} catch (SQLException e) {
							e.printStackTrace();
						}
					}

		return false;
	}
	
}
