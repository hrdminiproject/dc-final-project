package model.dao.user;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import utilities.DBUtility;
import model.dto.Users;

public class UpdateUser {
	Connection con;
	// get connection when create object
		public UpdateUser() throws Exception {
			DBUtility utility = new DBUtility();
			con = utility.getConnection();
			System.out.println("Connecting to database...");
		}
		
	public boolean updateUser(Users user){
		PreparedStatement ps = null;
		
		try {
			ps = con.prepareStatement("UPDATE users SET username = ?, password = ?, fname = ?, lname = ?, email = ?, status = ? WHERE id = 2");
			ps.setString(1, user.getUsername());
			ps.setString(2, user.getPassword());
			ps.setString(3, user.getFname());
			ps.setString(4, user.getLname());
			ps.setString(5, user.getEmail());
			ps.setString(6, user.getStatus());
			
			if (ps.executeUpdate() > 0)
				return true;
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				ps.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return false;
	}

}
