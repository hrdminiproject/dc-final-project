package controller.achievement;

import java.io.File;
import java.io.IOException;
import java.util.Iterator;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;

import model.dto.Achievement;

/**
 * Servlet implementation class UpdateExp
 */

public class UpdateAch extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public UpdateAch() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		String filePath;
		String temp;
		String imageUrl = "";
		int maxFileSize = 1024 * 1024 * 5;
		int maxMemSize = 1024 * 1024 * 4;
		File file ;
		
		// TODO Auto-generated method stub
		filePath = getServletContext().getRealPath("/") + "admin/images/uploads/achievement/";
		temp = getServletContext().getRealPath("/") + "admin/images/uploads/temp/";
		response.setContentType("text/html");

		DiskFileItemFactory factory = new DiskFileItemFactory();
		// maximum size that will be stored in memory
		factory.setSizeThreshold(maxMemSize);
		// Location to save data that is larger than maxMemSize.
		factory.setRepository(new File(temp));

		// Create a new file upload handler
		ServletFileUpload upload = new ServletFileUpload(factory);
		// maximum file size to be uploaded.
		upload.setSizeMax(maxFileSize);

		try {
			Achievement ach = new Achievement();	
			// Parse the request to get file items.
			List fileItems = upload.parseRequest(request);

			// Process the uploaded file items
			Iterator i = fileItems.iterator();

			while (i.hasNext()) {
				FileItem fi = (FileItem) i.next();
				if (!fi.isFormField()) {
					if (fi.getName() == "" || fi.getName() == null) {
						continue;
					}
					// Get the uploaded file parameters
					String fileName = fi.getName();
					
					// Write the file	
					if (fileName.lastIndexOf("\\") >= 0) {
						file = new File(filePath + fileName.substring(fileName.lastIndexOf("\\")));
					} else {
						file = new File(filePath + fileName.substring(fileName.lastIndexOf("\\") + 1));
					}
					
					imageUrl = fileName;
					fi.write(file);
					
				} else {
					switch (fi.getFieldName()) {
						case "title":
							ach.setTitle(fi.getString());
							break;
						case "year":
							ach.setYear(fi.getString());
							break;
						case "location":
							ach.setLocation(fi.getString());
							break;
						case "description":
							ach.setDescription(fi.getString());
							break;
						case "image-url":
							imageUrl = fi.getString();
							break;
						default:
							break;
					}
				}
			}
			
			ach.setImage_url(imageUrl);
			ach.setId(Integer.parseInt(request.getParameter("id")));
			if(new model.dao.achievement.UpdateAch().udpateAch(ach)){
				response.sendRedirect("achievement.jsp");
			}
			
		} catch (Exception ex) {
			System.out.println(ex);
		}
	}

}
