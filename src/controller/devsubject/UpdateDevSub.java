package controller.devsubject;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import model.dao.devsubject.UpdateDevSubject;
import model.dto.DevSubjects;

public class UpdateDevSub extends HttpServlet{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public UpdateDevSub(){
		super();
	}
	
	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		DevSubjects devsub= new DevSubjects();
		devsub.setDevid((int)request.getSession().getAttribute("id"));
		devsub.setSid(Integer.parseInt(request.getParameter("sid")));
		devsub.setRate(request.getParameter("progress"));
		devsub.setDescription(request.getParameter("description"));
		
		response.setContentType("text/plain");
		try {
			if((new UpdateDevSubject().updateDevSubjects(devsub))){
				System.out.println("UPDATE SUCCESS");
				response.sendRedirect("myadddevsubject.jsp");
			
			}else{
				System.err.println("UPDATE FAIL");
				response.getWriter().write("fail");
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		
	}
	
	
}
