package controller.devsubject;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import utilities.Convertor;

import com.google.gson.Gson;

import model.dao.category.GetCategoryRecord;
import model.dao.subject.GetSubjectRecord;
import model.dto.Categories;
import model.dto.Subjects;

public class GetRecordDevSub extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public GetRecordDevSub(){
		super();
	}
	
	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		try {
			int sid=Integer.parseInt(request.getParameter("id"));
			response.setContentType("application/json");
			response.setCharacterEncoding("UTF-8");
			String jdata = Convertor.convertResultSetIntoJSON(new model.dao.devsubject.GetDevSubjectRecord().listDevSubjectEdit((int)request.getSession().getAttribute("id"), sid));
			response.getWriter().write(jdata);
			System.out.println(jdata);
		} catch (Exception e) {
			e.printStackTrace();
		}			
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		
	}	
}
