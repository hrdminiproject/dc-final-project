package controller.devsubject;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import model.dao.devsubject.AddDevSubject;
import model.dto.DevSubjects;


public class AddDevSub extends HttpServlet {
	
	private static final long serialVersionUID = 1L;
    
    /**
     * @see HttpServlet#HttpServlet()
     */
    public AddDevSub() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		DevSubjects devsub= new DevSubjects();
		
		devsub.setDevid((int)request.getSession().getAttribute("id"));
		devsub.setSid(Integer.parseInt(request.getParameter("sid")));
		devsub.setRate(request.getParameter("progress"));
		devsub.setDescription(request.getParameter("description"));
				
		response.setContentType("text/plain");
		try {
			if((new AddDevSubject()).addDevSubjects(devsub)){
				System.out.println("INSERT SUCCESS");
				response.sendRedirect("myadddevsubject.jsp");
			
			}else{
				System.err.println("INSERT FAIL");
				response.getWriter().write("fail");
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		
	}
	
}
