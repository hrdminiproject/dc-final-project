package controller.devsubject;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import model.dao.devsubject.DeleteDevSubject;

public class DeleteDevSub extends HttpServlet{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public DeleteDevSub(){
		super();
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		int sid= Integer.parseInt(request.getParameter("sid"));
		DeleteDevSubject dao = null;
		try {
			dao = new DeleteDevSubject();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		if(dao.deleteDevSubjects((int)request.getSession().getAttribute("id"),sid)){
			System.out.println("Delete Succesfull");
		}else{
			System.out.println("Delete UnSuccesfull");
		}
	}		
}
