package controller.search;

import java.io.IOException;
import java.sql.ResultSet;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import utilities.Convertor;

public class ListAdvancedSearchField extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public ListAdvancedSearchField() {
		// TODO Auto-generated constructor stub
	}
	
	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		
	}
	
	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		try {
			response.setContentType("application/json");
			response.setCharacterEncoding("UTF-8");
			ArrayList<ResultSet> list = new ArrayList<ResultSet>();
			list.add(new model.dao.search.ListAdvancedSearchField().listMajor());
			list.add(new model.dao.search.ListAdvancedSearchField().listLanguage());
			list.add(new model.dao.search.ListAdvancedSearchField().listCategorie());
			list.add(new model.dao.search.ListAdvancedSearchField().listSubject());
			list.add(new model.dao.search.ListAdvancedSearchField().listPosition());
			
			String jdata = Convertor.convertListResultSetIntoJSON(list);
			response.getWriter().write(jdata);
			System.err.println(jdata);
		} catch (Exception e) {
			e.printStackTrace();
		}	
	}

}
