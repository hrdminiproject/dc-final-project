package controller.search;

import java.io.IOException;
import java.sql.ResultSet;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import model.dao.search.AdvSearch;
import utilities.Convertor;

/**
 * Servlet implementation class AdvanceSearch
 */

public class AdvanceSearch extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public AdvanceSearch() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	
		String degree = request.getParameter("degree");
		String major = request.getParameter("major");
		String position = request.getParameter("position");
		String category = request.getParameter("category");
		String[] salary = request.getParameter("salary").split("-");
		String[] experience = request.getParameter("experience").split("-");
		String[] skill = request.getParameter("skill").split(",");
		String[] language = request.getParameter("language").split(",");		
		
		for (String x : language){
			System.out.println(x);
		}
		
		for (String x : skill){
			System.out.println(x);
		}
		
		for (String x : experience){
			System.out.println(x);
		}
		
		ResultSet rs = null;
		try {
			rs = new AdvSearch().search(degree, major, position, category, skill, language, experience, salary);
			response.setContentType("application/json");
			response.setCharacterEncoding("UTF-8");
			String data = Convertor.convertResultSetIntoJSON(rs);
			response.getWriter().write(data);
			System.out.println(data);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}

}
