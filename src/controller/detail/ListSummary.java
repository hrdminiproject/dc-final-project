package controller.detail;

import java.io.IOException;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import utilities.Convertor;

public class ListSummary extends HttpServlet{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public ListSummary() {
		// TODO Auto-generated constructor stub
	}
	
	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		
	}
	
	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		try {
			response.setContentType("application/json");
			response.setCharacterEncoding("UTF-8");
			ArrayList<ResultSet> list = new ArrayList<ResultSet>();
			list.add(new model.dao.developer.ListDev().countApprovedUser());
			list.add(new model.dao.developer.ListDev().countAllUser());
			list.add(new model.dao.developer.ListDev().countNormalUser());
			list.add(new model.dao.developer.ListDev().countRejectUser());
			
			String jdata = Convertor.convertListResultSetIntoJSON(list);
			response.getWriter().write(jdata);
			System.out.println(jdata);
		} catch (Exception e) {
			e.printStackTrace();
		}	
	}

}
