package controller.detail;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;

import model.dao.achievement.ListAch;
import model.dao.developer.ListDev;
import model.dao.developer.ListDevLang;
import model.dao.developer.ListDevSkill;
import model.dao.education.ListEducation;
import model.dao.experience.ListExperience;
import model.dto.Achievement;
import model.dto.Education;
import model.dto.Experiences;
import model.dto.UserDev;
import model.dto.UserLang;
import model.dto.UserSkill;

/**
 * Servlet implementation class ListDetailDevInfo
 */

public class ListDetailDevInfo extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ListDetailDevInfo() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		//response.getWriter().append("Served at: ").append(request.getContextPath());
			}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		int dev_id;
		if (request.getParameter("dev_id") == ""){
			dev_id = Integer.parseInt(request.getSession().getAttribute("id").toString());
		}else{
			dev_id = Integer.parseInt(request.getParameter("dev_id"));
		}
		
		UserDev user = null;
		ArrayList<Achievement> ach = null;
		ArrayList<Education> edu = null;
		ArrayList<Experiences> exp = null;
		ArrayList<UserSkill> skill = null;
		ArrayList<UserLang> lang = null;
		
		try {
			user = new ListDev().getUserDevInfo(dev_id);
			ach = new ListAch().listAch(dev_id);
			edu = new ListEducation().listEducation(dev_id);
			exp = new ListExperience().listExperiences(dev_id);
			skill = new ListDevSkill().listUserDevSkill(dev_id);
			lang = new ListDevLang().listUserDevLang(dev_id);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		Map<String, Object> data = new HashMap<String, Object>();
		data.put("info", user);
		data.put("educations", edu);
		data.put("achievements", ach);
		data.put("experiences", exp);
		data.put("skills", skill);
		data.put("languages", lang);
		
		response.setContentType("application/json");
		response.setCharacterEncoding("UTF-8");
		String detail = new Gson().toJson(data);
		response.getWriter().write(detail);
		System.out.println(detail);
	}

}
