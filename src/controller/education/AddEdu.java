package controller.education;

import java.io.File;
import java.io.IOException;
import java.util.Iterator;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;

import model.dto.Education;

/**
 * Servlet implementation class AddEdu
 */
public class AddEdu extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public AddEdu() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String filePath;
		String temp;
		int maxFileSize = 1024 * 1024 * 5;
		int maxMemSize = 1024 * 1024 * 4;
		File file ;
		
		// TODO Auto-generated method stub
		filePath = getServletContext().getRealPath("/") + "images/uploads/education/";
		temp = getServletContext().getRealPath("/") + "images/uploads/";
		response.setContentType("text/html");

		DiskFileItemFactory factory = new DiskFileItemFactory();
		// maximum size that will be stored in memory
		factory.setSizeThreshold(maxMemSize);
		// Location to save data that is larger than maxMemSize.
		factory.setRepository(new File(temp));

		// Create a new file upload handler
		ServletFileUpload upload = new ServletFileUpload(factory);
		// maximum file size to be uploaded.
		upload.setSizeMax(maxFileSize);

		try {
			Education edu = new Education();	
			// Parse the request to get file items.
			List fileItems = upload.parseRequest(request);

			// Process the uploaded file items
			Iterator i = fileItems.iterator();

			while (i.hasNext()) {
				FileItem fi = (FileItem) i.next();
				if (!fi.isFormField()) {
					if (fi.getName() == "" || fi.getName() == null) {
						continue;
					}
					// Get the uploaded file parameters
					String fileName = fi.getName();

					// Write the file	
					if (fileName.lastIndexOf("\\") >= 0) {
						file = new File(filePath + fileName.substring(fileName.lastIndexOf("\\")));
					} else {
						file = new File(filePath + fileName.substring(fileName.lastIndexOf("\\") + 1));
					}
					
					edu.setImage_url(fileName);
					System.out.println(fileName);
					fi.write(file);
					
				} else {
					switch (fi.getFieldName()) {
					case "school":
						edu.setSchool(fi.getString());
						break;
					case "degree":
						edu.setDegree(fi.getString());
						break;
					case "major":
					case "other":
						if (fi.getString() != "")	
							edu.setMajor(fi.getString());
						break;
					case "start_year":
						edu.setStart_year(fi.getString());
						break;
					case "end_year":
						edu.setEnd_year(fi.getString());
						break;
					case "current_year":
						edu.setCurrent_year(fi.getString());
						break;
					case "description":
						edu.setDescription(fi.getString());
						break;
					case "location":
						edu.setLocation(fi.getString());
						break;
					default:
						break;
					}
				}
			}
			
			edu.setDevid((int)request.getSession().getAttribute("id"));
			if(new model.dao.education.AddEducation().addEducation(edu)){
				response.sendRedirect("education.jsp");
			}
			
		} catch (Exception ex) {
			System.out.println(ex);
		}
	}
}
