package controller.education;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;

import model.dao.education.ListEducation;
import model.dto.Education;

/**
 * Servlet implementation class ListEdu
 */
public class ListEdu extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ListEdu() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		int dev_id = (int)request.getSession().getAttribute("id");
		ArrayList<Education> edu;
		try {
			edu = new ListEducation().listEducation(dev_id);
			response.setContentType("application/json");
			response.setCharacterEncoding("UTF-8");
			String jdata = new Gson().toJson(edu);
			response.getWriter().write(jdata);
			System.out.println(jdata);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}	
	}

}
