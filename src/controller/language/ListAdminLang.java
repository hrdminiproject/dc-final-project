package controller.language;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import utilities.Convertor;

import com.google.gson.Gson;

import model.dao.category.ListCategory;
import model.dto.Categories;

public class ListAdminLang extends HttpServlet{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public ListAdminLang(){
		super();
	}
	
	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		try {
			response.setContentType("application/json");
			response.setCharacterEncoding("UTF-8");
			String jdata = Convertor.convertResultSetIntoJSON(new model.dao.language.ListLanguage().listAdminLang());
			response.getWriter().write(jdata);
			System.out.println(jdata);
		} catch (Exception e) {
			e.printStackTrace();
		}	
	
	}
}
