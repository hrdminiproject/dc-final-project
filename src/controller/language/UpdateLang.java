package controller.language;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import model.dao.category.UpdateCategory;
import model.dao.language.UpdateLanguage;
import model.dto.Categories;
import model.dto.Languages;

public class UpdateLang extends HttpServlet{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public UpdateLang(){
		super();
	}
	
	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		Languages lang= new Languages();
		lang.setId(Integer.parseInt(request.getParameter("id")));
		lang.setLanguage(request.getParameter("language"));
		
		
		response.setContentType("text/plain");
		try {
			if((new UpdateLanguage().updateLanguages(lang))){
				System.out.println("UPDATE SUCCESS");
				response.sendRedirect("myaddlanguage.jsp");
			
			}else{
				System.err.println("UPDATE FAIL");
				response.getWriter().write("fail");
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		
	}
	
	
}
