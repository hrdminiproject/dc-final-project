package controller.user;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import model.dto.Users;

/**
 * Servlet implementation class SignInUser
 */
// @WebServlet("/SignInUser")
public class SignInSimpleUser extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public SignInSimpleUser() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		String name = request.getParameter("loginUsername");
		String pwd = request.getParameter("loginPassword");
		
		HttpSession session = request.getSession();
		Users user = null;
		response.setContentType("text/plain");
		try {
			if ((user = new model.dao.user.SignInUser().signIn(name, pwd)) != null) {
				System.out.println("SIGN-IN SUCCESS");
				session.setAttribute("id", user.getId());
				session.setAttribute("user", user.getUsername());
				session.setAttribute("avatar", user.getAvatar());
				session.setAttribute("role", user.getType());
				response.sendRedirect("index.jsp");
			} else {
				System.err.println("SIGN-IN FAIL");
				response.sendRedirect("index.jsp");
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return;
	}

}
