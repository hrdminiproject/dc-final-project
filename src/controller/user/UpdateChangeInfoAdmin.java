package controller.user;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import model.dto.Users;

public class UpdateChangeInfoAdmin extends HttpServlet{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public UpdateChangeInfoAdmin(){
		super();
	}
	
	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		Users user= new Users();
		user.setId((int)request.getSession().getAttribute("id"));
		user.setLname(request.getParameter("lname"));
		user.setFname(request.getParameter("fname"));
		user.setEmail(request.getParameter("email"));
		
		
		response.setContentType("text/plain");
		try {
			if((new model.dao.user.UpdateChangeInfoAdmin().updatechangeinfo(user))){
				System.out.println("UPDATE SUCCESS");
				//response.sendRedirect("index.jsp");
				response.getWriter().write("updated");
			
			}else{
				System.err.println("UPDATE FAIL");
				response.getWriter().write("fail");
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}	
	}
}
