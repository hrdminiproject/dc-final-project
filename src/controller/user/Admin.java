package controller.user;

import java.io.IOException;
import java.util.Arrays;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * Servlet Filter implementation class Admin
 */
@WebFilter("/Admin")
public class Admin implements Filter {

	/**
	 * Default constructor.
	 */
	public Admin() {
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see Filter#destroy()
	 */
	public void destroy() {
		// TODO Auto-generated method stub
	}

	/**
	 * @see Filter#doFilter(ServletRequest, ServletResponse, FilterChain)
	 */
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
			throws IOException, ServletException {

		HttpSession session = ((HttpServletRequest) request).getSession();
		if (session.getAttribute("user") == null) {

			String uri = ((HttpServletRequest) request).getRequestURI();

			System.out.println("URI: " + uri);
			session.setAttribute("requestURI", uri);
			((HttpServletResponse) response).sendRedirect("../login.jsp");

		} else {
			String[] admin = { "index.jsp", "view_emp.jsp", "myaddsubject.jsp", "myaddcategory.jsp",
					"myaddlanguage.jsp", "user.jsp" };
			String[] dev = { "profile.jsp", "form_personal.jsp", "education.jsp", "experience.jsp", "achievement.jsp",
					"myadddevlanguage.jsp", "myadddevsubject.jsp" };

			String[] str = ((HttpServletRequest) request).getRequestURI().toString().split("/");
			if (str.length > 3) {
				System.out.println(Integer.parseInt(session.getAttribute("role").toString()) + " " + str[3]);

				if (Integer.parseInt(session.getAttribute("role").toString()) == 0
						&& Arrays.asList(admin).contains(str[3]))
					chain.doFilter(request, response);
				else if (Integer.parseInt(session.getAttribute("role").toString()) == 1
						&& Arrays.asList(dev).contains(str[3]))
					chain.doFilter(request, response);
				else
					((HttpServletResponse) response).sendRedirect("../404.jsp");
			} else {
				if (Integer.parseInt(session.getAttribute("role").toString()) == 0)
					chain.doFilter(request, response);
				else if (Integer.parseInt(session.getAttribute("role").toString()) == 1)
					((HttpServletResponse) response).sendRedirect("profile.jsp");
				else
					((HttpServletResponse) response).sendRedirect("../404.jsp");

			}

		}
	}

	/**
	 * @see Filter#init(FilterConfig)
	 */
	public void init(FilterConfig fConfig) throws ServletException {
		// TODO Auto-generated method stub
	}

}
