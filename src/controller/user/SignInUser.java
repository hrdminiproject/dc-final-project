package controller.user;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.google.gson.Gson;
import model.dto.Users;

/**
 * Servlet implementation class SignInUser
 */
// @WebServlet("/SignInUser")
public class SignInUser extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public SignInUser() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		String name = request.getParameter("username");
		String pwd = request.getParameter("password");
		HttpSession session = request.getSession();
		Users user = null;
		response.setContentType("application/json");
		response.setCharacterEncoding("UTF-8");
		try {
			if ((user = new model.dao.user.SignInUser().signIn(name, pwd)) != null) {
				System.out.println("SIGN-IN SUCCESS");
				session.setAttribute("id", user.getId());
				session.setAttribute("user", user.getUsername());
				session.setAttribute("avatar", user.getAvatar());
				session.setAttribute("role", user.getType());
				session.setAttribute("lname", user.getLname());
				session.setAttribute("fname", user.getFname());

				// Create Map to append url
				Map<String, String> data = new HashMap<String, String>();
				data.put("id", String.valueOf(user.getId()));
				data.put("user", user.getUsername());
				data.put("avatar", user.getAvatar());
				data.put("role", user.getType());
				data.put("lname", user.getLname());
				data.put("fname", user.getFname());
				// get session from DeveloperDetailFilter.java
				data.put("uri", session.getAttribute("requestURI") == null ? ""
						: session.getAttribute("requestURI").toString());

				String jdata = new Gson().toJson(data);
				response.getWriter().write(jdata);
				System.out.println(jdata);
			} else {
				System.err.println("SIGN-IN FAIL");
				String jdata = new Gson().toJson("fail");
				response.getWriter().write(jdata);
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return;
	}

}
