package controller.user;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import model.dto.Users;

/**
 * Servlet implementation class AddUser
 */

public class AddUser extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public AddUser() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		Users user = new Users();
		if (request.getParameter("userType") != null || request.getParameter("userType") != ""){
			user.setType(request.getParameter("userType"));
		}else{
			user.setType("0");
		}
		user.setUsername(request.getParameter("username"));
		user.setLname(request.getParameter("lastname"));
		user.setFname(request.getParameter("firstname"));
		user.setPassword(request.getParameter("password"));
		user.setGender(request.getParameter("gender"));
		user.setEmail(request.getParameter("email"));
		user.setStatus("true");
		
		try {
			if(new model.dao.user.AddUser().addUser(user)){
				response.sendRedirect("../index.jsp");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
