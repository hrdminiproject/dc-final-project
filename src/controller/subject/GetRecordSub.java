package controller.subject;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;

import model.dao.category.GetCategoryRecord;
import model.dao.subject.GetSubjectRecord;
import model.dto.Categories;
import model.dto.Subjects;

public class GetRecordSub extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public GetRecordSub(){
		super();
	}
	
	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
				response.setCharacterEncoding("UTF-8");
				response.setContentType("application/json");
				try {
					Subjects sub = new GetSubjectRecord().listSubjectEdit(Integer.parseInt(request.getParameter("id")));
					
					String s = new Gson().toJson(sub);
					System.out.println(s);
					response.getWriter().write(s);
				} catch (Exception e) {
					e.printStackTrace();
				}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		
	}	
}
