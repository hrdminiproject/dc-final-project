package controller.changepwd;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import model.dto.Users;

public class UpdateChangePwd extends HttpServlet{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public UpdateChangePwd(){
		super();
	}
	
	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		Users user= new Users();
		user.setId((int)request.getSession().getAttribute("id"));
		user.setPassword(request.getParameter("oldpwd"));
		String newPwd = request.getParameter("newpwd");
		
		
		response.setContentType("text/plain");
		try {
			if((new model.dao.changepwd.UpdateChangePwd().updatechangepassword(user, newPwd))){
				System.out.println("UPDATE SUCCESS");
				response.sendRedirect("index.jsp");
			
			}else{
				System.err.println("UPDATE FAIL");
				response.getWriter().write("fail");
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}	
	}
}
