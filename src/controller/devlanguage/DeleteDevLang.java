package controller.devlanguage;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import model.dao.devlanguage.DeleteDevLanguage;

public class DeleteDevLang extends HttpServlet{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public DeleteDevLang(){
		super();
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		int lid= Integer.parseInt(request.getParameter("laid"));
		System.out.println(lid);
		DeleteDevLanguage dao = null;
		try {
			dao = new DeleteDevLanguage();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		if(dao.deleteDevLanguages((int) request.getSession().getAttribute("id"),lid)){
			System.out.println("Delete Succesfull");
		}else{
			System.out.println("Delete UnSuccesfull");
		}
	}		
}
