package controller.devlanguage;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import model.dao.devlanguage.AddDevLanguage;
import model.dao.devsubject.AddDevSubject;
import model.dto.DevLanguages;
import model.dto.DevSubjects;


public class AddDevLang extends HttpServlet {
	
	private static final long serialVersionUID = 1L;
    
    /**
     * @see HttpServlet#HttpServlet()
     */
    public AddDevLang() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		DevLanguages devsub= new DevLanguages();
		
		devsub.setDevid((int)request.getSession().getAttribute("id"));
		//devsub.setDevid(1);
		devsub.setLid(Integer.parseInt(request.getParameter("laid")));
		devsub.setUnderstandingLevel(Integer.parseInt(request.getParameter("understandingLevel")));
		devsub.setSpeakingLevel(Integer.parseInt(request.getParameter("speakingLevel")));
		devsub.setWritingLevel(Integer.parseInt(request.getParameter("writingLevel")));
		devsub.setDescription(request.getParameter("description"));
	
		response.setContentType("text/plain");
		try {
			if((new AddDevLanguage()).addDevLanguages(devsub)){
				System.out.println("INSERT SUCCESS");
				response.sendRedirect("myadddevlanguage.jsp");
			
			}else{
				System.err.println("INSERT FAIL");
				response.getWriter().write("fail");
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		
	}
	
}
