package controller.devlanguage;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import utilities.Convertor;

public class GetRecordDevLang extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public GetRecordDevLang(){
		super();
	}
	
	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		try {
			int lid=Integer.parseInt(request.getParameter("id"));
			
			response.setContentType("application/json");
			response.setCharacterEncoding("UTF-8");
			String jdata = Convertor.convertResultSetIntoJSON(new model.dao.devlanguage.GetDevLanguageRecord().listDevLanguageEdit((int)request.getSession().getAttribute("id"), lid));
			response.getWriter().write(jdata);
			System.out.println(jdata);
		} catch (Exception e) {
			e.printStackTrace();
		}			
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
	}	
}
