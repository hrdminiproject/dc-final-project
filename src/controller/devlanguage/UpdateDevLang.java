package controller.devlanguage;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import model.dao.devlanguage.UpdateDevLanguage;
import model.dto.DevLanguages;

public class UpdateDevLang extends HttpServlet{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public UpdateDevLang(){
		super();
	}
	
	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		DevLanguages devlan= new DevLanguages();
		devlan.setDevid((int)request.getSession().getAttribute("id"));
		devlan.setLid(Integer.parseInt(request.getParameter("lanid")));
		devlan.setUnderstandingLevel(Integer.parseInt(request.getParameter("understandingLevel")));
		devlan.setSpeakingLevel(Integer.parseInt(request.getParameter("speakingLevel")));
		devlan.setWritingLevel(Integer.parseInt(request.getParameter("writingLevel")));
		devlan.setDescription(request.getParameter("description"));
		
		response.setContentType("text/plain");
		try {
			if((new UpdateDevLanguage().udpateDevLanguages(devlan))){
				System.out.println("UPDATE SUCCESS");
				response.sendRedirect("myadddevlanguage.jsp");
			
			}else{
				System.err.println("UPDATE FAIL");
				response.getWriter().write("fail");
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	
}
