package controller.experience;

import java.io.File;
import java.io.IOException;
import java.util.Iterator;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;

import model.dto.Experiences;

/**
 * Servlet implementation class AddExperience
 */

public class AddExperience extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
	
    public AddExperience() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		String filePath;
		String temp;
		int maxFileSize = 1024 * 1024 * 5;
		int maxMemSize = 1024 * 1024 * 4;
		File file ;
		
		// TODO Auto-generated method stub
		filePath = getServletContext().getRealPath("/") + "images/uploads/experience/";
		temp = getServletContext().getRealPath("/") + "images/uploads/";
		response.setContentType("text/html");

		DiskFileItemFactory factory = new DiskFileItemFactory();
		// maximum size that will be stored in memory
		factory.setSizeThreshold(maxMemSize);
		// Location to save data that is larger than maxMemSize.
		factory.setRepository(new File(temp));

		// Create a new file upload handler
		ServletFileUpload upload = new ServletFileUpload(factory);
		// maximum file size to be uploaded.
		upload.setSizeMax(maxFileSize);

		try {
			Experiences exp = new Experiences();	
			// Parse the request to get file items.
			List fileItems = upload.parseRequest(request);

			// Process the uploaded file items
			Iterator i = fileItems.iterator();

			while (i.hasNext()) {
				FileItem fi = (FileItem) i.next();
				if (!fi.isFormField()) {
					if (fi.getName() == "" || fi.getName() == null) {
						continue;
					}
					// Get the uploaded file parameters
					String fileName = fi.getName();

					// Write the file	
					if (fileName.lastIndexOf("\\") >= 0) {
						file = new File(filePath + fileName.substring(fileName.lastIndexOf("\\")));
					} else {
						file = new File(filePath + fileName.substring(fileName.lastIndexOf("\\") + 1));
					}
					
					exp.setImage_url(fileName);
					fi.write(file);
					
				} else {
					switch (fi.getFieldName()) {
						case "place":
							exp.setPlace(fi.getString());
							break;
						case "position":
							exp.setPosition(fi.getString());
							break;
						case "duration":
							exp.setDuration(fi.getString());
							break;
						case "type":
							exp.setType(fi.getString());
							break;
						case "skill":
							exp.setSkill(fi.getString());
							break;
						case "description":
							exp.setDescription(fi.getString());
							break;
						default:
							break;
					}
				}
			}
			
			
			exp.setDevid((int)request.getSession().getAttribute("id"));
			if(new model.dao.experience.AddExperience().addExperiences(exp)){
				response.sendRedirect("experience.jsp");
			}
			
		} catch (Exception ex) {
			System.out.println(ex);
		}
		
	}

}
