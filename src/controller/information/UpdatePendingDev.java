package controller.information;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.codehaus.groovy.runtime.dgmimpl.arrays.IntegerArrayGetAtMetaMethod;

/**
 * Servlet implementation class UpdatePendingDev
 */

public class UpdatePendingDev extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public UpdatePendingDev() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		int dev_id;
		if (request.getParameter("dev_id") == ""){
			dev_id = (int) request.getSession().getAttribute("id");
		}else{
			dev_id = Integer.parseInt(request.getParameter("dev_id"));
		}
		
		String status = request.getParameter("status");
		try {
			if (new model.dao.developer.UpdatePendingDev().updateDevSubjects(status, dev_id)){
				response.getWriter().write("success");
			}else{
				response.getWriter().write("failed");
			}
		} catch (Exception e) {
			// TODO: handle exception
		}
	}

}
