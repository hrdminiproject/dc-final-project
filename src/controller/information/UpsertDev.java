package controller.information;

import java.io.File;
import java.io.IOException;
import java.util.Iterator;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;

import model.dto.Developers;
import model.dto.Users;

/**
 * Servlet implementation class UpsertDev
 */

public class UpsertDev extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public UpsertDev() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		String filePath;
		String temp;
		int maxFileSize = 1024 * 1024 * 5;
		int maxMemSize = 1024 * 1024 * 4;
		File file ;
		
		// TODO Auto-generated method stub
		filePath = getServletContext().getRealPath("/") + "images/uploads/developer/";
		temp = getServletContext().getRealPath("/") + "images/uploads/";
		response.setContentType("text/html");

		DiskFileItemFactory factory = new DiskFileItemFactory();
		// maximum size that will be stored in memory
		factory.setSizeThreshold(maxMemSize);
		// Location to save data that is larger than maxMemSize.
		factory.setRepository(new File(temp));

		// Create a new file upload handler
		ServletFileUpload upload = new ServletFileUpload(factory);
		// maximum file size to be uploaded.
		upload.setSizeMax(maxFileSize);

		try {
			Users user=new Users();
			Developers dev=new Developers();
					// Parse the request to get file items.
			List fileItems = upload.parseRequest(request);

			// Process the uploaded file items
			Iterator i = fileItems.iterator();

			while (i.hasNext()) {
				FileItem fi = (FileItem) i.next();
				if (!fi.isFormField()) {
					if (fi.getName() == "" || fi.getName() == null) {
						continue;
					}
					// Get the uploaded file parameters
					String fileName = fi.getName();

					// Write the file	
					if (fileName.lastIndexOf("\\") >= 0) {
						file = new File(filePath + fileName.substring(fileName.lastIndexOf("\\")));
					} else {
						file = new File(filePath + fileName.substring(fileName.lastIndexOf("\\") + 1));
					}			
					dev.setAvatar(fileName);
					fi.write(file);
					
				} else {
					switch (fi.getFieldName()) {
						case "fName":
							user.setFname(fi.getString());
							break;
						case "lName":
							user.setLname(fi.getString());
							break;
						case "cbo_gender":
							user.setGender(fi.getString());
							break;
						case "email":
							user.setEmail(fi.getString());
							break;
						case "phone":
							dev.setPhone(fi.getString());
							break;
						case "nationality":
							dev.setNationality(fi.getString());
							break;
						case "pob":
							dev.setPob(fi.getString());
							break;
						case "cbo_cur_citystate":
							dev.setCurrentCityState(fi.getString());
							break;
						case "address":
							dev.setAddress(fi.getString());
							break;
						case "cbo_marital":
							dev.setMarital(fi.getString());
							break;
						case "wanted_position":
							dev.setWantedPosition(fi.getString());
							break;
						case "motivation":
							dev.setMotivation(fi.getString());
							break;
						case "salary":
							dev.setSalary(Double.parseDouble(fi.getString()));
							break;
						case "specialty_skill":
							dev.setSpecialtySkill(fi.getString());
							break;
						case "dob":
							dev.setDob(fi.getString());
							break;
						case "avatar":
							dev.setAvatar(fi.getString());
							break;
						default:
							break;
					}
				}
			}
			user.setId((int)request.getSession().getAttribute("id"));
			if(new model.dao.developer.UpsertDev().upsertDeveloper(user, dev)){
				response.sendRedirect("form_personal.jsp");
			}			
		} catch (Exception ex) {
			System.out.println(ex);
		}
	}
}