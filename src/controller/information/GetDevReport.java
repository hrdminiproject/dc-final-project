package controller.information;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import model.dao.user.Report;

/**
 * Servlet implementation class GetDevReport
 */
@WebServlet("/GetDevReport")
public class GetDevReport extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public GetDevReport() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		try {
			Report report=new Report();
			int id = Integer.parseInt(request.getParameter("id"));
			Map<String, Object> param =new HashMap<String, Object>();
			param.put("dev_id", id);
			String imagePath = request.getServletContext().getRealPath("/images/uploads/developer") + "\\";
			System.out.println(imagePath);
			param.put("img_path", imagePath);
			
			String jrxmlPath = request.getServletContext().getRealPath("/report/reportCV.jrxml");
			report.generateReport(response, param, jrxmlPath);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
