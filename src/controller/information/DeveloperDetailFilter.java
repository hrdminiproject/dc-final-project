package controller.information;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.google.gson.Gson;

/**
 * Servlet Filter implementation class DeveloperDetailFilter
 */
@WebFilter("/DeveloperDetailFilter")
public class DeveloperDetailFilter implements Filter {

	/**
	 * Default constructor.
	 */
	public DeveloperDetailFilter() {
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see Filter#destroy()
	 */
	public void destroy() {
		// TODO Auto-generated method stub
	}

	/**
	 * @see Filter#doFilter(ServletRequest, ServletResponse, FilterChain)
	 */
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
			throws IOException, ServletException {
		System.out.println(((HttpServletRequest) request).getParameter("dev_id"));
		HttpSession session = ((HttpServletRequest) request).getSession();

		response.setContentType("application/json");
		response.setCharacterEncoding("UTF-8");

		if (session.getAttribute("user") == null) {
			String jdata = new Gson().toJson("fail");
			((HttpServletResponse) response).getWriter().write(jdata);
			System.out.println(jdata);
			String uri = ((HttpServletRequest) request).getRequestURI() + "?dev_id="
					+ ((HttpServletRequest) request).getParameter("dev_id");
			session.setAttribute("requestURI", uri);
			
		} else {
			// if ajax request then respone json
			if ("XMLHttpRequest".equals(((HttpServletRequest) request).getHeader("X-Requested-With"))) {
				String jdata = new Gson().toJson("success");
				((HttpServletResponse) response).getWriter().write(jdata);
				System.out.println(jdata);
			} else // if request by URL then direct go to that link
				chain.doFilter(request, response);
		}
	}

	/**
	 * @see Filter#init(FilterConfig)
	 */
	public void init(FilterConfig fConfig) throws ServletException {
		// TODO Auto-generated method stub
	}

}