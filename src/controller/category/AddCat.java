package controller.category;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import model.dao.category.AddCategory;
import model.dto.Categories;


public class AddCat extends HttpServlet {
	
	private static final long serialVersionUID = 1L;
    
    /**
     * @see HttpServlet#HttpServlet()
     */
    public AddCat() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		Categories 	cat= new Categories();
		cat.setName(request.getParameter("name"));
		cat.setDescription(request.getParameter("description"));
		System.out.println(request.getParameter("name")+request.getParameter("description"));
		
		response.setContentType("text/plain");
		try {
			if((new AddCategory()).addCategories(cat)){
				System.out.println("INSERT SUCCESS");
				response.sendRedirect("myaddcategory.jsp");
			
			}else{
				System.err.println("INSERT FAIL");
				response.getWriter().write("fail");
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
