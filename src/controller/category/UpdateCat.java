package controller.category;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import model.dao.category.UpdateCategory;
import model.dto.Categories;

public class UpdateCat extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public UpdateCat() {
		super();
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		Categories cat= new Categories();
		cat.setId(Integer.parseInt(request.getParameter("id")));
		cat.setName(request.getParameter("name"));
		cat.setDescription(request.getParameter("description"));
		
		
		response.setContentType("text/plain");
		try {
			if((new UpdateCategory().updateCategories(cat))){
				System.out.println("UPDATE SUCCESS");
				response.sendRedirect("myaddcategory.jsp");
			
			}else{
				System.err.println("UPDATE FAIL");
				response.getWriter().write("fail");
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
